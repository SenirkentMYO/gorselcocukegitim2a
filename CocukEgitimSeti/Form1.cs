﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CocukEgitimSeti
{
    public partial class form_index : Form
    {
        public form_index()
        {
            InitializeComponent();
        }

        private void form_index_Load(object sender, EventArgs e)
        {
           
        }

        private void btn_ders_Click(object sender, EventArgs e)
        {
            frm_dersler drs= new frm_dersler();
            drs.Show();
            this.Hide();
        }

        private void btn_msl_Click(object sender, EventArgs e)
        {
            MasalDiyari frm_msl = new MasalDiyari();
            frm_msl.Show();
            this.Hide();
        }

        private void btn_tlf_Click(object sender, EventArgs e)
        {
            Ogrenelim frm_tlffz = new Ogrenelim();
            frm_tlffz.Show();
            this.Hide();
        }

        private void btn_sar_Click(object sender, EventArgs e)
        {
            BirazadaSarkı frm_sar = new BirazadaSarkı();
            frm_sar.Show();
            this.Hide();
        }

        private void btn_not_Click(object sender, EventArgs e)
        {
  
            Notlarım frm_not = new Notlarım();
            frm_not.Show();
            this.Hide();
        }

        private void btn_oyn_Click(object sender, EventArgs e)
        {
            OyunZamani frm_oyn = new OyunZamani();
            frm_oyn.Show();
            this.Hide();
        }

        

       
    }
}

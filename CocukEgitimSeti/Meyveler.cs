﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CocukEgitimSeti
{
    public partial class Meyveler : Form
    {
        public Meyveler()
        {
            InitializeComponent();
        }

        private void btnAna_Click(object sender, EventArgs e)
        {
            form_index ind = new form_index();
            ind.Show();
            this.Hide();
        }

        private void btnOgr_Click(object sender, EventArgs e)
        {
            Ogrenelim ogr = new Ogrenelim();
            ogr.Show();
            this.Hide();
        }

        private void bttn1_Click(object sender, EventArgs e)
        {
            lbl1.Text = "PEAR";
        }

        private void bttn2_Click(object sender, EventArgs e)
        {
            lbl2.Text = "BLACKBERRY";
        }

        private void bttn3_Click(object sender, EventArgs e)
        {
            lbl3.Text = "STRAWBERY";
        }

        private void bttn4_Click(object sender, EventArgs e)
        {
            lbl4.Text = "APPLE";
        }

        private void bttn5_Click(object sender, EventArgs e)
        {
            lbl5.Text = "PLUM";
        }

        private void bttn6_Click(object sender, EventArgs e)
        {
            lbl6.Text = "WATERMELON";
        }

        private void bttn7_Click(object sender, EventArgs e)
        {
            lbl7.Text = "CHERRY";
        }

        private void bttn8_Click(object sender, EventArgs e)
        {
            lbl8.Text = "BANANA";
        }

        private void bttn9_Click(object sender, EventArgs e)
        {
            lbl9.Text = "ORANGE";
        }

        private void bttn10_Click(object sender, EventArgs e)
        {
            lbl10.Text = "GRAPE";
        }
    }
}

﻿namespace CocukEgitimSeti
{
    partial class Ulkeler
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAna = new System.Windows.Forms.Button();
            this.btnOgr = new System.Windows.Forms.Button();
            this.bttn1 = new System.Windows.Forms.Button();
            this.bttn2 = new System.Windows.Forms.Button();
            this.bttn3 = new System.Windows.Forms.Button();
            this.bttn4 = new System.Windows.Forms.Button();
            this.bttn5 = new System.Windows.Forms.Button();
            this.bttn6 = new System.Windows.Forms.Button();
            this.bttn7 = new System.Windows.Forms.Button();
            this.bttn8 = new System.Windows.Forms.Button();
            this.bttn9 = new System.Windows.Forms.Button();
            this.bttn10 = new System.Windows.Forms.Button();
            this.bttn11 = new System.Windows.Forms.Button();
            this.bttn12 = new System.Windows.Forms.Button();
            this.bttn13 = new System.Windows.Forms.Button();
            this.bttn14 = new System.Windows.Forms.Button();
            this.bttn15 = new System.Windows.Forms.Button();
            this.bttn16 = new System.Windows.Forms.Button();
            this.bttn17 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.lbl1 = new System.Windows.Forms.Label();
            this.lbl2 = new System.Windows.Forms.Label();
            this.lbl3 = new System.Windows.Forms.Label();
            this.lbl4 = new System.Windows.Forms.Label();
            this.lbl5 = new System.Windows.Forms.Label();
            this.lbl6 = new System.Windows.Forms.Label();
            this.lbl7 = new System.Windows.Forms.Label();
            this.lbl8 = new System.Windows.Forms.Label();
            this.lbl9 = new System.Windows.Forms.Label();
            this.lbl10 = new System.Windows.Forms.Label();
            this.lbl11 = new System.Windows.Forms.Label();
            this.lbl12 = new System.Windows.Forms.Label();
            this.lbl13 = new System.Windows.Forms.Label();
            this.lbl14 = new System.Windows.Forms.Label();
            this.lbl15 = new System.Windows.Forms.Label();
            this.lbl16 = new System.Windows.Forms.Label();
            this.lbl17 = new System.Windows.Forms.Label();
            this.lbl18 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnAna
            // 
            this.btnAna.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnAna.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnAna.Location = new System.Drawing.Point(12, 314);
            this.btnAna.Name = "btnAna";
            this.btnAna.Size = new System.Drawing.Size(148, 40);
            this.btnAna.TabIndex = 38;
            this.btnAna.Text = "ANASAYFA";
            this.btnAna.UseVisualStyleBackColor = false;
            this.btnAna.Click += new System.EventHandler(this.btnAna_Click);
            // 
            // btnOgr
            // 
            this.btnOgr.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnOgr.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnOgr.Location = new System.Drawing.Point(12, 371);
            this.btnOgr.Name = "btnOgr";
            this.btnOgr.Size = new System.Drawing.Size(148, 40);
            this.btnOgr.TabIndex = 39;
            this.btnOgr.Text = "ÖĞRENELİM";
            this.btnOgr.UseVisualStyleBackColor = false;
            this.btnOgr.Click += new System.EventHandler(this.btnOgr_Click);
            // 
            // bttn1
            // 
            this.bttn1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.bttn1.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.trky;
            this.bttn1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn1.ForeColor = System.Drawing.Color.Black;
            this.bttn1.Location = new System.Drawing.Point(12, 12);
            this.bttn1.Name = "bttn1";
            this.bttn1.Size = new System.Drawing.Size(108, 25);
            this.bttn1.TabIndex = 40;
            this.bttn1.UseVisualStyleBackColor = false;
            this.bttn1.Click += new System.EventHandler(this.bttn1_Click);
            // 
            // bttn2
            // 
            this.bttn2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.bttn2.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.amrk;
            this.bttn2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn2.ForeColor = System.Drawing.Color.Black;
            this.bttn2.Location = new System.Drawing.Point(12, 55);
            this.bttn2.Name = "bttn2";
            this.bttn2.Size = new System.Drawing.Size(108, 25);
            this.bttn2.TabIndex = 41;
            this.bttn2.UseVisualStyleBackColor = false;
            this.bttn2.Click += new System.EventHandler(this.bttn2_Click);
            // 
            // bttn3
            // 
            this.bttn3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.bttn3.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.arjn;
            this.bttn3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn3.ForeColor = System.Drawing.Color.Black;
            this.bttn3.Location = new System.Drawing.Point(12, 100);
            this.bttn3.Name = "bttn3";
            this.bttn3.Size = new System.Drawing.Size(108, 25);
            this.bttn3.TabIndex = 42;
            this.bttn3.UseVisualStyleBackColor = false;
            this.bttn3.Click += new System.EventHandler(this.bttn3_Click);
            // 
            // bttn4
            // 
            this.bttn4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.bttn4.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.blgrs;
            this.bttn4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn4.ForeColor = System.Drawing.Color.Black;
            this.bttn4.Location = new System.Drawing.Point(12, 145);
            this.bttn4.Name = "bttn4";
            this.bttn4.Size = new System.Drawing.Size(108, 25);
            this.bttn4.TabIndex = 43;
            this.bttn4.UseVisualStyleBackColor = false;
            this.bttn4.Click += new System.EventHandler(this.bttn4_Click);
            // 
            // bttn5
            // 
            this.bttn5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.bttn5.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.brzly;
            this.bttn5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn5.ForeColor = System.Drawing.Color.Black;
            this.bttn5.Location = new System.Drawing.Point(12, 190);
            this.bttn5.Name = "bttn5";
            this.bttn5.Size = new System.Drawing.Size(108, 25);
            this.bttn5.TabIndex = 44;
            this.bttn5.UseVisualStyleBackColor = false;
            this.bttn5.Click += new System.EventHandler(this.bttn5_Click);
            // 
            // bttn6
            // 
            this.bttn6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.bttn6.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.dnmrk;
            this.bttn6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn6.ForeColor = System.Drawing.Color.Black;
            this.bttn6.Location = new System.Drawing.Point(12, 239);
            this.bttn6.Name = "bttn6";
            this.bttn6.Size = new System.Drawing.Size(108, 25);
            this.bttn6.TabIndex = 45;
            this.bttn6.UseVisualStyleBackColor = false;
            this.bttn6.Click += new System.EventHandler(this.bttn6_Click);
            // 
            // bttn7
            // 
            this.bttn7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.bttn7.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.frns;
            this.bttn7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn7.ForeColor = System.Drawing.Color.Black;
            this.bttn7.Location = new System.Drawing.Point(280, 12);
            this.bttn7.Name = "bttn7";
            this.bttn7.Size = new System.Drawing.Size(108, 25);
            this.bttn7.TabIndex = 46;
            this.bttn7.UseVisualStyleBackColor = false;
            this.bttn7.Click += new System.EventHandler(this.bttn7_Click);
            // 
            // bttn8
            // 
            this.bttn8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.bttn8.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.gnyafr;
            this.bttn8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn8.ForeColor = System.Drawing.Color.Black;
            this.bttn8.Location = new System.Drawing.Point(280, 55);
            this.bttn8.Name = "bttn8";
            this.bttn8.Size = new System.Drawing.Size(108, 25);
            this.bttn8.TabIndex = 47;
            this.bttn8.UseVisualStyleBackColor = false;
            this.bttn8.Click += new System.EventHandler(this.bttn8_Click);
            // 
            // bttn9
            // 
            this.bttn9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.bttn9.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.hlnd;
            this.bttn9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn9.ForeColor = System.Drawing.Color.Black;
            this.bttn9.Location = new System.Drawing.Point(280, 100);
            this.bttn9.Name = "bttn9";
            this.bttn9.Size = new System.Drawing.Size(108, 25);
            this.bttn9.TabIndex = 48;
            this.bttn9.UseVisualStyleBackColor = false;
            this.bttn9.Click += new System.EventHandler(this.bttn9_Click);
            // 
            // bttn10
            // 
            this.bttn10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.bttn10.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.hndst;
            this.bttn10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn10.ForeColor = System.Drawing.Color.Black;
            this.bttn10.Location = new System.Drawing.Point(280, 145);
            this.bttn10.Name = "bttn10";
            this.bttn10.Size = new System.Drawing.Size(108, 25);
            this.bttn10.TabIndex = 49;
            this.bttn10.UseVisualStyleBackColor = false;
            this.bttn10.Click += new System.EventHandler(this.bttn10_Click);
            // 
            // bttn11
            // 
            this.bttn11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.bttn11.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.ingt;
            this.bttn11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn11.ForeColor = System.Drawing.Color.Black;
            this.bttn11.Location = new System.Drawing.Point(280, 190);
            this.bttn11.Name = "bttn11";
            this.bttn11.Size = new System.Drawing.Size(108, 25);
            this.bttn11.TabIndex = 50;
            this.bttn11.UseVisualStyleBackColor = false;
            this.bttn11.Click += new System.EventHandler(this.bttn11_Click);
            // 
            // bttn12
            // 
            this.bttn12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.bttn12.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.isvc;
            this.bttn12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn12.ForeColor = System.Drawing.Color.Black;
            this.bttn12.Location = new System.Drawing.Point(280, 239);
            this.bttn12.Name = "bttn12";
            this.bttn12.Size = new System.Drawing.Size(108, 25);
            this.bttn12.TabIndex = 51;
            this.bttn12.UseVisualStyleBackColor = false;
            this.bttn12.Click += new System.EventHandler(this.bttn12_Click);
            // 
            // bttn13
            // 
            this.bttn13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.bttn13.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.itly;
            this.bttn13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn13.ForeColor = System.Drawing.Color.Black;
            this.bttn13.Location = new System.Drawing.Point(546, 12);
            this.bttn13.Name = "bttn13";
            this.bttn13.Size = new System.Drawing.Size(108, 25);
            this.bttn13.TabIndex = 52;
            this.bttn13.UseVisualStyleBackColor = false;
            this.bttn13.Click += new System.EventHandler(this.bttn13_Click);
            // 
            // bttn14
            // 
            this.bttn14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.bttn14.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.jpny;
            this.bttn14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn14.ForeColor = System.Drawing.Color.Black;
            this.bttn14.Location = new System.Drawing.Point(546, 55);
            this.bttn14.Name = "bttn14";
            this.bttn14.Size = new System.Drawing.Size(108, 25);
            this.bttn14.TabIndex = 53;
            this.bttn14.UseVisualStyleBackColor = false;
            this.bttn14.Click += new System.EventHandler(this.bttn14_Click);
            // 
            // bttn15
            // 
            this.bttn15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.bttn15.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.mısr;
            this.bttn15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn15.ForeColor = System.Drawing.Color.Black;
            this.bttn15.Location = new System.Drawing.Point(546, 100);
            this.bttn15.Name = "bttn15";
            this.bttn15.Size = new System.Drawing.Size(108, 25);
            this.bttn15.TabIndex = 54;
            this.bttn15.UseVisualStyleBackColor = false;
            this.bttn15.Click += new System.EventHandler(this.bttn15_Click);
            // 
            // bttn16
            // 
            this.bttn16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.bttn16.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.rusy;
            this.bttn16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn16.ForeColor = System.Drawing.Color.Black;
            this.bttn16.Location = new System.Drawing.Point(546, 145);
            this.bttn16.Name = "bttn16";
            this.bttn16.Size = new System.Drawing.Size(108, 25);
            this.bttn16.TabIndex = 55;
            this.bttn16.UseVisualStyleBackColor = false;
            this.bttn16.Click += new System.EventHandler(this.bttn16_Click);
            // 
            // bttn17
            // 
            this.bttn17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.bttn17.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.almny;
            this.bttn17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn17.ForeColor = System.Drawing.Color.Black;
            this.bttn17.Location = new System.Drawing.Point(546, 190);
            this.bttn17.Name = "bttn17";
            this.bttn17.Size = new System.Drawing.Size(108, 25);
            this.bttn17.TabIndex = 56;
            this.bttn17.UseVisualStyleBackColor = false;
            this.bttn17.Click += new System.EventHandler(this.bttn17_Click);
            // 
            // button17
            // 
            this.button17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.button17.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.ynstn;
            this.button17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button17.ForeColor = System.Drawing.Color.Black;
            this.button17.Location = new System.Drawing.Point(546, 239);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(108, 25);
            this.button17.TabIndex = 57;
            this.button17.UseVisualStyleBackColor = false;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.BackColor = System.Drawing.Color.Transparent;
            this.lbl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl1.Location = new System.Drawing.Point(126, 15);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(0, 18);
            this.lbl1.TabIndex = 58;
            // 
            // lbl2
            // 
            this.lbl2.AutoSize = true;
            this.lbl2.BackColor = System.Drawing.Color.Transparent;
            this.lbl2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl2.Location = new System.Drawing.Point(126, 58);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(0, 18);
            this.lbl2.TabIndex = 59;
            // 
            // lbl3
            // 
            this.lbl3.AutoSize = true;
            this.lbl3.BackColor = System.Drawing.Color.Transparent;
            this.lbl3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl3.Location = new System.Drawing.Point(126, 103);
            this.lbl3.Name = "lbl3";
            this.lbl3.Size = new System.Drawing.Size(0, 18);
            this.lbl3.TabIndex = 60;
            // 
            // lbl4
            // 
            this.lbl4.AutoSize = true;
            this.lbl4.BackColor = System.Drawing.Color.Transparent;
            this.lbl4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl4.Location = new System.Drawing.Point(126, 148);
            this.lbl4.Name = "lbl4";
            this.lbl4.Size = new System.Drawing.Size(0, 18);
            this.lbl4.TabIndex = 61;
            // 
            // lbl5
            // 
            this.lbl5.AutoSize = true;
            this.lbl5.BackColor = System.Drawing.Color.Transparent;
            this.lbl5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl5.Location = new System.Drawing.Point(126, 193);
            this.lbl5.Name = "lbl5";
            this.lbl5.Size = new System.Drawing.Size(0, 18);
            this.lbl5.TabIndex = 62;
            // 
            // lbl6
            // 
            this.lbl6.AutoSize = true;
            this.lbl6.BackColor = System.Drawing.Color.Transparent;
            this.lbl6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl6.Location = new System.Drawing.Point(126, 242);
            this.lbl6.Name = "lbl6";
            this.lbl6.Size = new System.Drawing.Size(0, 18);
            this.lbl6.TabIndex = 63;
            // 
            // lbl7
            // 
            this.lbl7.AutoSize = true;
            this.lbl7.BackColor = System.Drawing.Color.Transparent;
            this.lbl7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl7.Location = new System.Drawing.Point(394, 15);
            this.lbl7.Name = "lbl7";
            this.lbl7.Size = new System.Drawing.Size(0, 18);
            this.lbl7.TabIndex = 64;
            // 
            // lbl8
            // 
            this.lbl8.AutoSize = true;
            this.lbl8.BackColor = System.Drawing.Color.Transparent;
            this.lbl8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl8.Location = new System.Drawing.Point(394, 58);
            this.lbl8.Name = "lbl8";
            this.lbl8.Size = new System.Drawing.Size(0, 18);
            this.lbl8.TabIndex = 65;
            // 
            // lbl9
            // 
            this.lbl9.AutoSize = true;
            this.lbl9.BackColor = System.Drawing.Color.Transparent;
            this.lbl9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl9.Location = new System.Drawing.Point(394, 103);
            this.lbl9.Name = "lbl9";
            this.lbl9.Size = new System.Drawing.Size(0, 18);
            this.lbl9.TabIndex = 66;
            // 
            // lbl10
            // 
            this.lbl10.AutoSize = true;
            this.lbl10.BackColor = System.Drawing.Color.Transparent;
            this.lbl10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl10.Location = new System.Drawing.Point(394, 148);
            this.lbl10.Name = "lbl10";
            this.lbl10.Size = new System.Drawing.Size(0, 18);
            this.lbl10.TabIndex = 67;
            // 
            // lbl11
            // 
            this.lbl11.AutoSize = true;
            this.lbl11.BackColor = System.Drawing.Color.Transparent;
            this.lbl11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl11.Location = new System.Drawing.Point(394, 193);
            this.lbl11.Name = "lbl11";
            this.lbl11.Size = new System.Drawing.Size(0, 18);
            this.lbl11.TabIndex = 68;
            // 
            // lbl12
            // 
            this.lbl12.AutoSize = true;
            this.lbl12.BackColor = System.Drawing.Color.Transparent;
            this.lbl12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl12.Location = new System.Drawing.Point(394, 242);
            this.lbl12.Name = "lbl12";
            this.lbl12.Size = new System.Drawing.Size(0, 18);
            this.lbl12.TabIndex = 69;
            // 
            // lbl13
            // 
            this.lbl13.AutoSize = true;
            this.lbl13.BackColor = System.Drawing.Color.Transparent;
            this.lbl13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl13.Location = new System.Drawing.Point(660, 15);
            this.lbl13.Name = "lbl13";
            this.lbl13.Size = new System.Drawing.Size(0, 18);
            this.lbl13.TabIndex = 70;
            // 
            // lbl14
            // 
            this.lbl14.AutoSize = true;
            this.lbl14.BackColor = System.Drawing.Color.Transparent;
            this.lbl14.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl14.Location = new System.Drawing.Point(660, 58);
            this.lbl14.Name = "lbl14";
            this.lbl14.Size = new System.Drawing.Size(0, 18);
            this.lbl14.TabIndex = 71;
            // 
            // lbl15
            // 
            this.lbl15.AutoSize = true;
            this.lbl15.BackColor = System.Drawing.Color.Transparent;
            this.lbl15.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl15.Location = new System.Drawing.Point(660, 103);
            this.lbl15.Name = "lbl15";
            this.lbl15.Size = new System.Drawing.Size(0, 18);
            this.lbl15.TabIndex = 72;
            // 
            // lbl16
            // 
            this.lbl16.AutoSize = true;
            this.lbl16.BackColor = System.Drawing.Color.Transparent;
            this.lbl16.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl16.Location = new System.Drawing.Point(660, 148);
            this.lbl16.Name = "lbl16";
            this.lbl16.Size = new System.Drawing.Size(0, 18);
            this.lbl16.TabIndex = 73;
            // 
            // lbl17
            // 
            this.lbl17.AutoSize = true;
            this.lbl17.BackColor = System.Drawing.Color.Transparent;
            this.lbl17.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl17.Location = new System.Drawing.Point(660, 193);
            this.lbl17.Name = "lbl17";
            this.lbl17.Size = new System.Drawing.Size(0, 18);
            this.lbl17.TabIndex = 74;
            // 
            // lbl18
            // 
            this.lbl18.AutoSize = true;
            this.lbl18.BackColor = System.Drawing.Color.Transparent;
            this.lbl18.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl18.Location = new System.Drawing.Point(660, 242);
            this.lbl18.Name = "lbl18";
            this.lbl18.Size = new System.Drawing.Size(0, 18);
            this.lbl18.TabIndex = 75;
            // 
            // Ulkeler
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.back4;
            this.ClientSize = new System.Drawing.Size(913, 460);
            this.Controls.Add(this.lbl18);
            this.Controls.Add(this.lbl17);
            this.Controls.Add(this.lbl16);
            this.Controls.Add(this.lbl15);
            this.Controls.Add(this.lbl14);
            this.Controls.Add(this.lbl13);
            this.Controls.Add(this.lbl12);
            this.Controls.Add(this.lbl11);
            this.Controls.Add(this.lbl10);
            this.Controls.Add(this.lbl9);
            this.Controls.Add(this.lbl8);
            this.Controls.Add(this.lbl7);
            this.Controls.Add(this.lbl6);
            this.Controls.Add(this.lbl5);
            this.Controls.Add(this.lbl4);
            this.Controls.Add(this.lbl3);
            this.Controls.Add(this.lbl2);
            this.Controls.Add(this.lbl1);
            this.Controls.Add(this.button17);
            this.Controls.Add(this.bttn17);
            this.Controls.Add(this.bttn16);
            this.Controls.Add(this.bttn15);
            this.Controls.Add(this.bttn14);
            this.Controls.Add(this.bttn13);
            this.Controls.Add(this.bttn12);
            this.Controls.Add(this.bttn11);
            this.Controls.Add(this.bttn10);
            this.Controls.Add(this.bttn9);
            this.Controls.Add(this.bttn8);
            this.Controls.Add(this.bttn7);
            this.Controls.Add(this.bttn6);
            this.Controls.Add(this.bttn5);
            this.Controls.Add(this.bttn4);
            this.Controls.Add(this.bttn3);
            this.Controls.Add(this.bttn2);
            this.Controls.Add(this.bttn1);
            this.Controls.Add(this.btnOgr);
            this.Controls.Add(this.btnAna);
            this.Name = "Ulkeler";
            this.Text = "Ulkeler";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAna;
        private System.Windows.Forms.Button btnOgr;
        private System.Windows.Forms.Button bttn1;
        private System.Windows.Forms.Button bttn2;
        private System.Windows.Forms.Button bttn3;
        private System.Windows.Forms.Button bttn4;
        private System.Windows.Forms.Button bttn5;
        private System.Windows.Forms.Button bttn6;
        private System.Windows.Forms.Button bttn7;
        private System.Windows.Forms.Button bttn8;
        private System.Windows.Forms.Button bttn9;
        private System.Windows.Forms.Button bttn10;
        private System.Windows.Forms.Button bttn11;
        private System.Windows.Forms.Button bttn12;
        private System.Windows.Forms.Button bttn13;
        private System.Windows.Forms.Button bttn14;
        private System.Windows.Forms.Button bttn15;
        private System.Windows.Forms.Button bttn16;
        private System.Windows.Forms.Button bttn17;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Label lbl2;
        private System.Windows.Forms.Label lbl3;
        private System.Windows.Forms.Label lbl4;
        private System.Windows.Forms.Label lbl5;
        private System.Windows.Forms.Label lbl6;
        private System.Windows.Forms.Label lbl7;
        private System.Windows.Forms.Label lbl8;
        private System.Windows.Forms.Label lbl9;
        private System.Windows.Forms.Label lbl10;
        private System.Windows.Forms.Label lbl11;
        private System.Windows.Forms.Label lbl12;
        private System.Windows.Forms.Label lbl13;
        private System.Windows.Forms.Label lbl14;
        private System.Windows.Forms.Label lbl15;
        private System.Windows.Forms.Label lbl16;
        private System.Windows.Forms.Label lbl17;
        private System.Windows.Forms.Label lbl18;
    }
}
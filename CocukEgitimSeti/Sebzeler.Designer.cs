﻿namespace CocukEgitimSeti
{
    partial class Sebzeler
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Sebzeler));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.btnOgr = new System.Windows.Forms.Button();
            this.btnAna = new System.Windows.Forms.Button();
            this.btnBiber = new System.Windows.Forms.Button();
            this.btnZeytin = new System.Windows.Forms.Button();
            this.btnSogan = new System.Windows.Forms.Button();
            this.btnSarimsak = new System.Windows.Forms.Button();
            this.btnSalatalik = new System.Windows.Forms.Button();
            this.btnPatlican = new System.Windows.Forms.Button();
            this.btnPatates = new System.Windows.Forms.Button();
            this.btnPancar = new System.Windows.Forms.Button();
            this.btnMarul = new System.Windows.Forms.Button();
            this.btnMantar = new System.Windows.Forms.Button();
            this.btnLahana = new System.Windows.Forms.Button();
            this.btnFasulye = new System.Windows.Forms.Button();
            this.btnHavuç = new System.Windows.Forms.Button();
            this.btnKarnıbahar = new System.Windows.Forms.Button();
            this.btnBrokoli = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(118, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.Location = new System.Drawing.Point(118, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label3.Location = new System.Drawing.Point(118, 139);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 17;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label4.Location = new System.Drawing.Point(115, 192);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 13);
            this.label4.TabIndex = 18;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label5.Location = new System.Drawing.Point(115, 242);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 13);
            this.label5.TabIndex = 19;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label6.Location = new System.Drawing.Point(334, 34);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 13);
            this.label6.TabIndex = 20;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label7.Location = new System.Drawing.Point(334, 87);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 13);
            this.label7.TabIndex = 21;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label8.Location = new System.Drawing.Point(334, 137);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(0, 13);
            this.label8.TabIndex = 22;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label9.Location = new System.Drawing.Point(334, 192);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(0, 13);
            this.label9.TabIndex = 23;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label10.Location = new System.Drawing.Point(334, 242);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(0, 13);
            this.label10.TabIndex = 24;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label11.Location = new System.Drawing.Point(579, 42);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(0, 13);
            this.label11.TabIndex = 25;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label12.Location = new System.Drawing.Point(579, 92);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(0, 13);
            this.label12.TabIndex = 26;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label13.Location = new System.Drawing.Point(579, 142);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(0, 13);
            this.label13.TabIndex = 27;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label14.Location = new System.Drawing.Point(579, 192);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(0, 13);
            this.label14.TabIndex = 28;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label15.Location = new System.Drawing.Point(579, 242);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(0, 13);
            this.label15.TabIndex = 29;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label16.Location = new System.Drawing.Point(190, 303);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(79, 13);
            this.label16.TabIndex = 30;
            this.label16.Text = "VEGETABLE";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.SteelBlue;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button1.Location = new System.Drawing.Point(90, 290);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(96, 38);
            this.button1.TabIndex = 31;
            this.button1.Text = "SEBZE";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnOgr
            // 
            this.btnOgr.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btnOgr.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnOgr.Location = new System.Drawing.Point(600, 365);
            this.btnOgr.Name = "btnOgr";
            this.btnOgr.Size = new System.Drawing.Size(148, 40);
            this.btnOgr.TabIndex = 60;
            this.btnOgr.Text = "ÖĞRENELİM";
            this.btnOgr.UseVisualStyleBackColor = false;
            this.btnOgr.Click += new System.EventHandler(this.btnOgr_Click);
            // 
            // btnAna
            // 
            this.btnAna.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnAna.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnAna.Location = new System.Drawing.Point(600, 319);
            this.btnAna.Name = "btnAna";
            this.btnAna.Size = new System.Drawing.Size(148, 40);
            this.btnAna.TabIndex = 61;
            this.btnAna.Text = "ANASAYFA";
            this.btnAna.UseVisualStyleBackColor = false;
            this.btnAna.Click += new System.EventHandler(this.btnAna_Click);
            // 
            // btnBiber
            // 
            this.btnBiber.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnBiber.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnBiber.BackgroundImage")));
            this.btnBiber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnBiber.ForeColor = System.Drawing.Color.Black;
            this.btnBiber.Location = new System.Drawing.Point(7, 30);
            this.btnBiber.Name = "btnBiber";
            this.btnBiber.Size = new System.Drawing.Size(108, 25);
            this.btnBiber.TabIndex = 62;
            this.btnBiber.UseVisualStyleBackColor = false;
            this.btnBiber.Click += new System.EventHandler(this.btnBiber_Click_1);
            // 
            // btnZeytin
            // 
            this.btnZeytin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnZeytin.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnZeytin.BackgroundImage")));
            this.btnZeytin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnZeytin.ForeColor = System.Drawing.Color.Black;
            this.btnZeytin.Location = new System.Drawing.Point(455, 235);
            this.btnZeytin.Name = "btnZeytin";
            this.btnZeytin.Size = new System.Drawing.Size(108, 25);
            this.btnZeytin.TabIndex = 63;
            this.btnZeytin.UseVisualStyleBackColor = false;
            this.btnZeytin.Click += new System.EventHandler(this.btnZeytin_Click_1);
            // 
            // btnSogan
            // 
            this.btnSogan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnSogan.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSogan.BackgroundImage")));
            this.btnSogan.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnSogan.ForeColor = System.Drawing.Color.Black;
            this.btnSogan.Location = new System.Drawing.Point(455, 185);
            this.btnSogan.Name = "btnSogan";
            this.btnSogan.Size = new System.Drawing.Size(108, 25);
            this.btnSogan.TabIndex = 64;
            this.btnSogan.UseVisualStyleBackColor = false;
            this.btnSogan.Click += new System.EventHandler(this.btnSogan_Click_1);
            // 
            // btnSarimsak
            // 
            this.btnSarimsak.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnSarimsak.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSarimsak.BackgroundImage")));
            this.btnSarimsak.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnSarimsak.ForeColor = System.Drawing.Color.Black;
            this.btnSarimsak.Location = new System.Drawing.Point(455, 130);
            this.btnSarimsak.Name = "btnSarimsak";
            this.btnSarimsak.Size = new System.Drawing.Size(108, 25);
            this.btnSarimsak.TabIndex = 65;
            this.btnSarimsak.UseVisualStyleBackColor = false;
            this.btnSarimsak.Click += new System.EventHandler(this.btnSarimsak_Click_1);
            // 
            // btnSalatalik
            // 
            this.btnSalatalik.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnSalatalik.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSalatalik.BackgroundImage")));
            this.btnSalatalik.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnSalatalik.ForeColor = System.Drawing.Color.Black;
            this.btnSalatalik.Location = new System.Drawing.Point(455, 80);
            this.btnSalatalik.Name = "btnSalatalik";
            this.btnSalatalik.Size = new System.Drawing.Size(108, 25);
            this.btnSalatalik.TabIndex = 66;
            this.btnSalatalik.UseVisualStyleBackColor = false;
            this.btnSalatalik.Click += new System.EventHandler(this.btnSalatalik_Click_1);
            // 
            // btnPatlican
            // 
            this.btnPatlican.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnPatlican.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPatlican.BackgroundImage")));
            this.btnPatlican.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnPatlican.ForeColor = System.Drawing.Color.Black;
            this.btnPatlican.Location = new System.Drawing.Point(455, 30);
            this.btnPatlican.Name = "btnPatlican";
            this.btnPatlican.Size = new System.Drawing.Size(108, 25);
            this.btnPatlican.TabIndex = 67;
            this.btnPatlican.UseVisualStyleBackColor = false;
            this.btnPatlican.Click += new System.EventHandler(this.btnPatlican_Click_1);
            // 
            // btnPatates
            // 
            this.btnPatates.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnPatates.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPatates.BackgroundImage")));
            this.btnPatates.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnPatates.ForeColor = System.Drawing.Color.Black;
            this.btnPatates.Location = new System.Drawing.Point(220, 235);
            this.btnPatates.Name = "btnPatates";
            this.btnPatates.Size = new System.Drawing.Size(108, 25);
            this.btnPatates.TabIndex = 68;
            this.btnPatates.UseVisualStyleBackColor = false;
            this.btnPatates.Click += new System.EventHandler(this.btnPatates_Click_1);
            // 
            // btnPancar
            // 
            this.btnPancar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnPancar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPancar.BackgroundImage")));
            this.btnPancar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnPancar.ForeColor = System.Drawing.Color.Black;
            this.btnPancar.Location = new System.Drawing.Point(220, 185);
            this.btnPancar.Name = "btnPancar";
            this.btnPancar.Size = new System.Drawing.Size(108, 25);
            this.btnPancar.TabIndex = 69;
            this.btnPancar.UseVisualStyleBackColor = false;
            this.btnPancar.Click += new System.EventHandler(this.btnPancar_Click_1);
            // 
            // btnMarul
            // 
            this.btnMarul.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnMarul.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMarul.BackgroundImage")));
            this.btnMarul.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnMarul.ForeColor = System.Drawing.Color.Black;
            this.btnMarul.Location = new System.Drawing.Point(220, 132);
            this.btnMarul.Name = "btnMarul";
            this.btnMarul.Size = new System.Drawing.Size(108, 25);
            this.btnMarul.TabIndex = 70;
            this.btnMarul.UseVisualStyleBackColor = false;
            this.btnMarul.Click += new System.EventHandler(this.btnMarul_Click_1);
            // 
            // btnMantar
            // 
            this.btnMantar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnMantar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMantar.BackgroundImage")));
            this.btnMantar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnMantar.ForeColor = System.Drawing.Color.Black;
            this.btnMantar.Location = new System.Drawing.Point(220, 80);
            this.btnMantar.Name = "btnMantar";
            this.btnMantar.Size = new System.Drawing.Size(108, 25);
            this.btnMantar.TabIndex = 71;
            this.btnMantar.UseVisualStyleBackColor = false;
            this.btnMantar.Click += new System.EventHandler(this.btnMantar_Click_1);
            // 
            // btnLahana
            // 
            this.btnLahana.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnLahana.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLahana.BackgroundImage")));
            this.btnLahana.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnLahana.ForeColor = System.Drawing.Color.Black;
            this.btnLahana.Location = new System.Drawing.Point(220, 30);
            this.btnLahana.Name = "btnLahana";
            this.btnLahana.Size = new System.Drawing.Size(108, 25);
            this.btnLahana.TabIndex = 72;
            this.btnLahana.UseVisualStyleBackColor = false;
            this.btnLahana.Click += new System.EventHandler(this.btnLahana_Click_1);
            // 
            // btnFasulye
            // 
            this.btnFasulye.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnFasulye.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFasulye.BackgroundImage")));
            this.btnFasulye.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnFasulye.ForeColor = System.Drawing.Color.Black;
            this.btnFasulye.Location = new System.Drawing.Point(7, 80);
            this.btnFasulye.Name = "btnFasulye";
            this.btnFasulye.Size = new System.Drawing.Size(108, 25);
            this.btnFasulye.TabIndex = 73;
            this.btnFasulye.UseVisualStyleBackColor = false;
            this.btnFasulye.Click += new System.EventHandler(this.btnFasulye_Click_1);
            // 
            // btnHavuç
            // 
            this.btnHavuç.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnHavuç.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnHavuç.BackgroundImage")));
            this.btnHavuç.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnHavuç.ForeColor = System.Drawing.Color.Black;
            this.btnHavuç.Location = new System.Drawing.Point(7, 132);
            this.btnHavuç.Name = "btnHavuç";
            this.btnHavuç.Size = new System.Drawing.Size(108, 25);
            this.btnHavuç.TabIndex = 74;
            this.btnHavuç.UseVisualStyleBackColor = false;
            this.btnHavuç.Click += new System.EventHandler(this.btnHavuç_Click);
            // 
            // btnKarnıbahar
            // 
            this.btnKarnıbahar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnKarnıbahar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnKarnıbahar.BackgroundImage")));
            this.btnKarnıbahar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnKarnıbahar.ForeColor = System.Drawing.Color.Black;
            this.btnKarnıbahar.Location = new System.Drawing.Point(7, 185);
            this.btnKarnıbahar.Name = "btnKarnıbahar";
            this.btnKarnıbahar.Size = new System.Drawing.Size(108, 25);
            this.btnKarnıbahar.TabIndex = 75;
            this.btnKarnıbahar.UseVisualStyleBackColor = false;
            this.btnKarnıbahar.Click += new System.EventHandler(this.btnKarnıbahar_Click);
            // 
            // btnBrokoli
            // 
            this.btnBrokoli.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.btnBrokoli.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnBrokoli.BackgroundImage")));
            this.btnBrokoli.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnBrokoli.ForeColor = System.Drawing.Color.Black;
            this.btnBrokoli.Location = new System.Drawing.Point(7, 235);
            this.btnBrokoli.Name = "btnBrokoli";
            this.btnBrokoli.Size = new System.Drawing.Size(108, 25);
            this.btnBrokoli.TabIndex = 76;
            this.btnBrokoli.UseVisualStyleBackColor = false;
            this.btnBrokoli.Click += new System.EventHandler(this.btnBrokoli_Click);
            // 
            // Sebzeler
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(845, 495);
            this.Controls.Add(this.btnBrokoli);
            this.Controls.Add(this.btnKarnıbahar);
            this.Controls.Add(this.btnHavuç);
            this.Controls.Add(this.btnFasulye);
            this.Controls.Add(this.btnLahana);
            this.Controls.Add(this.btnMantar);
            this.Controls.Add(this.btnMarul);
            this.Controls.Add(this.btnPancar);
            this.Controls.Add(this.btnPatates);
            this.Controls.Add(this.btnPatlican);
            this.Controls.Add(this.btnSalatalik);
            this.Controls.Add(this.btnSarimsak);
            this.Controls.Add(this.btnSogan);
            this.Controls.Add(this.btnZeytin);
            this.Controls.Add(this.btnBiber);
            this.Controls.Add(this.btnAna);
            this.Controls.Add(this.btnOgr);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Sebzeler";
            this.Text = "Sebzeler";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnOgr;
        private System.Windows.Forms.Button btnAna;
        private System.Windows.Forms.Button btnBiber;
        private System.Windows.Forms.Button btnZeytin;
        private System.Windows.Forms.Button btnSogan;
        private System.Windows.Forms.Button btnSarimsak;
        private System.Windows.Forms.Button btnSalatalik;
        private System.Windows.Forms.Button btnPatlican;
        private System.Windows.Forms.Button btnPatates;
        private System.Windows.Forms.Button btnPancar;
        private System.Windows.Forms.Button btnMarul;
        private System.Windows.Forms.Button btnMantar;
        private System.Windows.Forms.Button btnLahana;
        private System.Windows.Forms.Button btnFasulye;
        private System.Windows.Forms.Button btnHavuç;
        private System.Windows.Forms.Button btnKarnıbahar;
        private System.Windows.Forms.Button btnBrokoli;
    }
}
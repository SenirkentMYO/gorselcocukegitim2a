﻿namespace CocukEgitimSeti
{
    partial class Sekiller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Sekiller));
            this.pB1 = new System.Windows.Forms.PictureBox();
            this.pB2 = new System.Windows.Forms.PictureBox();
            this.pB3 = new System.Windows.Forms.PictureBox();
            this.pB4 = new System.Windows.Forms.PictureBox();
            this.pB11 = new System.Windows.Forms.PictureBox();
            this.pB7 = new System.Windows.Forms.PictureBox();
            this.pB6 = new System.Windows.Forms.PictureBox();
            this.pB10 = new System.Windows.Forms.PictureBox();
            this.pB12 = new System.Windows.Forms.PictureBox();
            this.pB13 = new System.Windows.Forms.PictureBox();
            this.pB14 = new System.Windows.Forms.PictureBox();
            this.pB5 = new System.Windows.Forms.PictureBox();
            this.pB9 = new System.Windows.Forms.PictureBox();
            this.pB8 = new System.Windows.Forms.PictureBox();
            this.lbl1 = new System.Windows.Forms.Label();
            this.lbl2 = new System.Windows.Forms.Label();
            this.lbl3 = new System.Windows.Forms.Label();
            this.lbl4 = new System.Windows.Forms.Label();
            this.lbl5 = new System.Windows.Forms.Label();
            this.lbl6 = new System.Windows.Forms.Label();
            this.lbl7 = new System.Windows.Forms.Label();
            this.lbl8 = new System.Windows.Forms.Label();
            this.lbl9 = new System.Windows.Forms.Label();
            this.lbl10 = new System.Windows.Forms.Label();
            this.lbl11 = new System.Windows.Forms.Label();
            this.lbl12 = new System.Windows.Forms.Label();
            this.lbl13 = new System.Windows.Forms.Label();
            this.lbl14 = new System.Windows.Forms.Label();
            this.btnAna = new System.Windows.Forms.Button();
            this.btnOgr = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pB1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB8)).BeginInit();
            this.SuspendLayout();
            // 
            // pB1
            // 
            this.pB1.BackColor = System.Drawing.Color.Transparent;
            this.pB1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pB1.BackgroundImage")));
            this.pB1.Location = new System.Drawing.Point(12, 12);
            this.pB1.Name = "pB1";
            this.pB1.Size = new System.Drawing.Size(113, 98);
            this.pB1.TabIndex = 0;
            this.pB1.TabStop = false;
            this.pB1.Click += new System.EventHandler(this.pB1_Click);
            // 
            // pB2
            // 
            this.pB2.BackColor = System.Drawing.Color.Transparent;
            this.pB2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pB2.BackgroundImage")));
            this.pB2.Location = new System.Drawing.Point(148, 12);
            this.pB2.Name = "pB2";
            this.pB2.Size = new System.Drawing.Size(112, 112);
            this.pB2.TabIndex = 1;
            this.pB2.TabStop = false;
            this.pB2.Click += new System.EventHandler(this.pB2_Click);
            // 
            // pB3
            // 
            this.pB3.BackColor = System.Drawing.Color.Transparent;
            this.pB3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pB3.BackgroundImage")));
            this.pB3.Location = new System.Drawing.Point(287, 12);
            this.pB3.Name = "pB3";
            this.pB3.Size = new System.Drawing.Size(116, 114);
            this.pB3.TabIndex = 2;
            this.pB3.TabStop = false;
            this.pB3.Click += new System.EventHandler(this.pB3_Click);
            // 
            // pB4
            // 
            this.pB4.BackColor = System.Drawing.Color.Transparent;
            this.pB4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pB4.BackgroundImage")));
            this.pB4.Location = new System.Drawing.Point(423, 12);
            this.pB4.Name = "pB4";
            this.pB4.Size = new System.Drawing.Size(59, 114);
            this.pB4.TabIndex = 3;
            this.pB4.TabStop = false;
            this.pB4.Click += new System.EventHandler(this.pB4_Click);
            // 
            // pB11
            // 
            this.pB11.BackColor = System.Drawing.Color.Transparent;
            this.pB11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pB11.BackgroundImage")));
            this.pB11.Location = new System.Drawing.Point(277, 241);
            this.pB11.Name = "pB11";
            this.pB11.Size = new System.Drawing.Size(215, 71);
            this.pB11.TabIndex = 4;
            this.pB11.TabStop = false;
            this.pB11.Click += new System.EventHandler(this.pB11_Click);
            // 
            // pB7
            // 
            this.pB7.BackColor = System.Drawing.Color.Transparent;
            this.pB7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pB7.BackgroundImage")));
            this.pB7.Location = new System.Drawing.Point(804, 12);
            this.pB7.Name = "pB7";
            this.pB7.Size = new System.Drawing.Size(80, 123);
            this.pB7.TabIndex = 5;
            this.pB7.TabStop = false;
            this.pB7.Click += new System.EventHandler(this.pB7_Click);
            // 
            // pB6
            // 
            this.pB6.BackColor = System.Drawing.Color.Transparent;
            this.pB6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pB6.BackgroundImage")));
            this.pB6.Location = new System.Drawing.Point(660, 11);
            this.pB6.Name = "pB6";
            this.pB6.Size = new System.Drawing.Size(112, 113);
            this.pB6.TabIndex = 6;
            this.pB6.TabStop = false;
            this.pB6.Click += new System.EventHandler(this.pB6_Click);
            // 
            // pB10
            // 
            this.pB10.BackColor = System.Drawing.Color.Transparent;
            this.pB10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pB10.BackgroundImage")));
            this.pB10.Location = new System.Drawing.Point(133, 241);
            this.pB10.Name = "pB10";
            this.pB10.Size = new System.Drawing.Size(111, 123);
            this.pB10.TabIndex = 7;
            this.pB10.TabStop = false;
            this.pB10.Click += new System.EventHandler(this.pB10_Click);
            // 
            // pB12
            // 
            this.pB12.BackColor = System.Drawing.Color.Transparent;
            this.pB12.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pB12.BackgroundImage")));
            this.pB12.Location = new System.Drawing.Point(533, 241);
            this.pB12.Name = "pB12";
            this.pB12.Size = new System.Drawing.Size(119, 96);
            this.pB12.TabIndex = 8;
            this.pB12.TabStop = false;
            this.pB12.Click += new System.EventHandler(this.pB12_Click);
            // 
            // pB13
            // 
            this.pB13.BackColor = System.Drawing.Color.Transparent;
            this.pB13.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pB13.BackgroundImage")));
            this.pB13.Location = new System.Drawing.Point(701, 241);
            this.pB13.Name = "pB13";
            this.pB13.Size = new System.Drawing.Size(117, 115);
            this.pB13.TabIndex = 9;
            this.pB13.TabStop = false;
            this.pB13.Click += new System.EventHandler(this.pB13_Click);
            // 
            // pB14
            // 
            this.pB14.BackColor = System.Drawing.Color.Transparent;
            this.pB14.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pB14.BackgroundImage")));
            this.pB14.Location = new System.Drawing.Point(855, 241);
            this.pB14.Name = "pB14";
            this.pB14.Size = new System.Drawing.Size(137, 115);
            this.pB14.TabIndex = 10;
            this.pB14.TabStop = false;
            this.pB14.Click += new System.EventHandler(this.pB14_Click);
            // 
            // pB5
            // 
            this.pB5.BackColor = System.Drawing.Color.Transparent;
            this.pB5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pB5.BackgroundImage")));
            this.pB5.Location = new System.Drawing.Point(524, 12);
            this.pB5.Name = "pB5";
            this.pB5.Size = new System.Drawing.Size(97, 99);
            this.pB5.TabIndex = 11;
            this.pB5.TabStop = false;
            this.pB5.Click += new System.EventHandler(this.pB5_Click);
            // 
            // pB9
            // 
            this.pB9.BackColor = System.Drawing.Color.Transparent;
            this.pB9.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pB9.BackgroundImage")));
            this.pB9.Location = new System.Drawing.Point(12, 241);
            this.pB9.Name = "pB9";
            this.pB9.Size = new System.Drawing.Size(88, 117);
            this.pB9.TabIndex = 12;
            this.pB9.TabStop = false;
            this.pB9.Click += new System.EventHandler(this.pB9_Click);
            // 
            // pB8
            // 
            this.pB8.BackColor = System.Drawing.Color.Transparent;
            this.pB8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pB8.BackgroundImage")));
            this.pB8.Location = new System.Drawing.Point(912, 12);
            this.pB8.Name = "pB8";
            this.pB8.Size = new System.Drawing.Size(119, 100);
            this.pB8.TabIndex = 13;
            this.pB8.TabStop = false;
            this.pB8.Click += new System.EventHandler(this.pB8_Click);
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.BackColor = System.Drawing.Color.Transparent;
            this.lbl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl1.Location = new System.Drawing.Point(24, 137);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(0, 20);
            this.lbl1.TabIndex = 14;
            // 
            // lbl2
            // 
            this.lbl2.AutoSize = true;
            this.lbl2.BackColor = System.Drawing.Color.Transparent;
            this.lbl2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl2.Location = new System.Drawing.Point(144, 168);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(0, 20);
            this.lbl2.TabIndex = 15;
            // 
            // lbl3
            // 
            this.lbl3.AutoSize = true;
            this.lbl3.BackColor = System.Drawing.Color.Transparent;
            this.lbl3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl3.Location = new System.Drawing.Point(292, 189);
            this.lbl3.Name = "lbl3";
            this.lbl3.Size = new System.Drawing.Size(0, 20);
            this.lbl3.TabIndex = 16;
            // 
            // lbl4
            // 
            this.lbl4.AutoSize = true;
            this.lbl4.BackColor = System.Drawing.Color.Transparent;
            this.lbl4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl4.Location = new System.Drawing.Point(419, 137);
            this.lbl4.Name = "lbl4";
            this.lbl4.Size = new System.Drawing.Size(0, 20);
            this.lbl4.TabIndex = 17;
            // 
            // lbl5
            // 
            this.lbl5.AutoSize = true;
            this.lbl5.BackColor = System.Drawing.Color.Transparent;
            this.lbl5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl5.Location = new System.Drawing.Point(529, 168);
            this.lbl5.Name = "lbl5";
            this.lbl5.Size = new System.Drawing.Size(0, 20);
            this.lbl5.TabIndex = 18;
            // 
            // lbl6
            // 
            this.lbl6.AutoSize = true;
            this.lbl6.BackColor = System.Drawing.Color.Transparent;
            this.lbl6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl6.Location = new System.Drawing.Point(669, 189);
            this.lbl6.Name = "lbl6";
            this.lbl6.Size = new System.Drawing.Size(0, 20);
            this.lbl6.TabIndex = 19;
            // 
            // lbl7
            // 
            this.lbl7.AutoSize = true;
            this.lbl7.BackColor = System.Drawing.Color.Transparent;
            this.lbl7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl7.Location = new System.Drawing.Point(784, 137);
            this.lbl7.Name = "lbl7";
            this.lbl7.Size = new System.Drawing.Size(0, 20);
            this.lbl7.TabIndex = 20;
            // 
            // lbl8
            // 
            this.lbl8.AutoSize = true;
            this.lbl8.BackColor = System.Drawing.Color.Transparent;
            this.lbl8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl8.Location = new System.Drawing.Point(881, 168);
            this.lbl8.Name = "lbl8";
            this.lbl8.Size = new System.Drawing.Size(0, 20);
            this.lbl8.TabIndex = 21;
            // 
            // lbl9
            // 
            this.lbl9.AutoSize = true;
            this.lbl9.BackColor = System.Drawing.Color.Transparent;
            this.lbl9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl9.Location = new System.Drawing.Point(24, 391);
            this.lbl9.Name = "lbl9";
            this.lbl9.Size = new System.Drawing.Size(0, 20);
            this.lbl9.TabIndex = 22;
            // 
            // lbl10
            // 
            this.lbl10.AutoSize = true;
            this.lbl10.BackColor = System.Drawing.Color.Transparent;
            this.lbl10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl10.Location = new System.Drawing.Point(129, 419);
            this.lbl10.Name = "lbl10";
            this.lbl10.Size = new System.Drawing.Size(0, 20);
            this.lbl10.TabIndex = 23;
            // 
            // lbl11
            // 
            this.lbl11.AutoSize = true;
            this.lbl11.BackColor = System.Drawing.Color.Transparent;
            this.lbl11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl11.Location = new System.Drawing.Point(273, 450);
            this.lbl11.Name = "lbl11";
            this.lbl11.Size = new System.Drawing.Size(0, 20);
            this.lbl11.TabIndex = 24;
            // 
            // lbl12
            // 
            this.lbl12.AutoSize = true;
            this.lbl12.BackColor = System.Drawing.Color.Transparent;
            this.lbl12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl12.Location = new System.Drawing.Point(529, 391);
            this.lbl12.Name = "lbl12";
            this.lbl12.Size = new System.Drawing.Size(0, 20);
            this.lbl12.TabIndex = 25;
            // 
            // lbl13
            // 
            this.lbl13.AutoSize = true;
            this.lbl13.BackColor = System.Drawing.Color.Transparent;
            this.lbl13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl13.Location = new System.Drawing.Point(697, 419);
            this.lbl13.Name = "lbl13";
            this.lbl13.Size = new System.Drawing.Size(0, 20);
            this.lbl13.TabIndex = 26;
            // 
            // lbl14
            // 
            this.lbl14.AutoSize = true;
            this.lbl14.BackColor = System.Drawing.Color.Transparent;
            this.lbl14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl14.Location = new System.Drawing.Point(851, 450);
            this.lbl14.Name = "lbl14";
            this.lbl14.Size = new System.Drawing.Size(0, 20);
            this.lbl14.TabIndex = 27;
            // 
            // btnAna
            // 
            this.btnAna.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnAna.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnAna.Location = new System.Drawing.Point(12, 527);
            this.btnAna.Name = "btnAna";
            this.btnAna.Size = new System.Drawing.Size(148, 40);
            this.btnAna.TabIndex = 37;
            this.btnAna.Text = "ANASAYFA";
            this.btnAna.UseVisualStyleBackColor = false;
            this.btnAna.Click += new System.EventHandler(this.btnAna_Click);
            // 
            // btnOgr
            // 
            this.btnOgr.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnOgr.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnOgr.Location = new System.Drawing.Point(194, 527);
            this.btnOgr.Name = "btnOgr";
            this.btnOgr.Size = new System.Drawing.Size(148, 40);
            this.btnOgr.TabIndex = 38;
            this.btnOgr.Text = "ÖĞRENELİM";
            this.btnOgr.UseVisualStyleBackColor = false;
            this.btnOgr.Click += new System.EventHandler(this.btnOgr_Click);
            // 
            // Sekiller
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1117, 570);
            this.Controls.Add(this.btnOgr);
            this.Controls.Add(this.btnAna);
            this.Controls.Add(this.lbl14);
            this.Controls.Add(this.lbl13);
            this.Controls.Add(this.lbl12);
            this.Controls.Add(this.lbl11);
            this.Controls.Add(this.lbl10);
            this.Controls.Add(this.lbl9);
            this.Controls.Add(this.lbl8);
            this.Controls.Add(this.lbl7);
            this.Controls.Add(this.lbl6);
            this.Controls.Add(this.lbl5);
            this.Controls.Add(this.lbl4);
            this.Controls.Add(this.lbl3);
            this.Controls.Add(this.lbl2);
            this.Controls.Add(this.lbl1);
            this.Controls.Add(this.pB8);
            this.Controls.Add(this.pB9);
            this.Controls.Add(this.pB5);
            this.Controls.Add(this.pB14);
            this.Controls.Add(this.pB13);
            this.Controls.Add(this.pB12);
            this.Controls.Add(this.pB10);
            this.Controls.Add(this.pB6);
            this.Controls.Add(this.pB7);
            this.Controls.Add(this.pB11);
            this.Controls.Add(this.pB4);
            this.Controls.Add(this.pB3);
            this.Controls.Add(this.pB2);
            this.Controls.Add(this.pB1);
            this.Name = "Sekiller";
            this.Text = "Sekiller";
            ((System.ComponentModel.ISupportInitialize)(this.pB1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB8)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pB1;
        private System.Windows.Forms.PictureBox pB2;
        private System.Windows.Forms.PictureBox pB3;
        private System.Windows.Forms.PictureBox pB4;
        private System.Windows.Forms.PictureBox pB11;
        private System.Windows.Forms.PictureBox pB7;
        private System.Windows.Forms.PictureBox pB6;
        private System.Windows.Forms.PictureBox pB10;
        private System.Windows.Forms.PictureBox pB12;
        private System.Windows.Forms.PictureBox pB13;
        private System.Windows.Forms.PictureBox pB14;
        private System.Windows.Forms.PictureBox pB5;
        private System.Windows.Forms.PictureBox pB9;
        private System.Windows.Forms.PictureBox pB8;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Label lbl2;
        private System.Windows.Forms.Label lbl3;
        private System.Windows.Forms.Label lbl4;
        private System.Windows.Forms.Label lbl5;
        private System.Windows.Forms.Label lbl6;
        private System.Windows.Forms.Label lbl7;
        private System.Windows.Forms.Label lbl8;
        private System.Windows.Forms.Label lbl9;
        private System.Windows.Forms.Label lbl10;
        private System.Windows.Forms.Label lbl11;
        private System.Windows.Forms.Label lbl12;
        private System.Windows.Forms.Label lbl13;
        private System.Windows.Forms.Label lbl14;
        private System.Windows.Forms.Button btnAna;
        private System.Windows.Forms.Button btnOgr;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CocukEgitimSeti
{
    public partial class Meslekler : Form
    {
        public Meslekler()
        {
            InitializeComponent();
        }

        private void bttn1_Click(object sender, EventArgs e)
        {
            lbl1.Text = "COOK";
        }

        private void bttn2_Click(object sender, EventArgs e)
        {
            lbl2.Text = "LAWYER";
        }

        private void bttn3_Click(object sender, EventArgs e)
        {
            lbl3.Text = "BARBER";
        }

        private void bttn4_Click(object sender, EventArgs e)
        {
            lbl4.Text = "FARMER";
        }

        private void bttn5_Click(object sender, EventArgs e)
        {
            lbl5.Text = "DOCTOR";
        }

        private void bttn6_Click(object sender, EventArgs e)
        {
            lbl6.Text = "JUDGE";
        }

        private void bttn7_Click(object sender, EventArgs e)
        {
            lbl7.Text = "WORKER";
        }

        private void bttn8_Click(object sender, EventArgs e)
        {
            lbl8.Text = "MİNER";
        }

        private void btnAna_Click(object sender, EventArgs e)
        {
            form_index ind = new form_index();
            ind.Show();
            this.Hide();
        }

        private void btnOgr_Click(object sender, EventArgs e)
        {
            Ogrenelim ogr = new Ogrenelim();
            ogr.Show();
            this.Hide();
        }

        
    }
}

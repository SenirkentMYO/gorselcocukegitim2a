﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CocukEgitimSeti
{
    public partial class Renkler : Form
    {
        public Renkler()
        {
            InitializeComponent();
        }

        private void Renkler_Load(object sender, EventArgs e)
        {
           
        }

        private void btnAna_Click(object sender, EventArgs e)
        {
            form_index ind = new form_index();
            ind.Show();
            this.Hide();
        }

        private void btnOgr_Click(object sender, EventArgs e)
        {
            Ogrenelim ogr = new Ogrenelim();
            ogr.Show();
            this.Hide();
        }

        private void bttn1_Click(object sender, EventArgs e)
        {
            lbl1.Text = "Siyah / BLACK";
        }

        private void bttn2_Click(object sender, EventArgs e)
        {
            lbl2.Text = "Beyaz / WHITE";
        }

        private void bttn3_Click(object sender, EventArgs e)
        {
            lbl3.Text = "Kırmızı / RED";
        }

        private void bttn4_Click(object sender, EventArgs e)
        {
            lbl4.Text = "Turuncu / ORANGE";
        }

        private void bttn5_Click(object sender, EventArgs e)
        {
            lbl5.Text = "Sarı / YELLOW";
        }

        private void bttn6_Click(object sender, EventArgs e)
        {
            lbl6.Text = "Yeşil / GREEN";
        }

        private void bttn7_Click(object sender, EventArgs e)
        {
            lbl7.Text = "Mavi / BLUE";
        }

        private void bttn8_Click(object sender, EventArgs e)
        {
            lbl8.Text = "Mor / PURPLE";
        }

        private void bttn9_Click(object sender, EventArgs e)
        {
            lbl9.Text = "Pembe / PINK";
        }

        private void bttn10_Click(object sender, EventArgs e)
        {
            lbl10.Text = "Bordo / CLARET RED";
        }

        private void pb3_Click(object sender, EventArgs e)
        {
            lbl11.Text = "ANARENK / Sarı-Yellow";
        }

        private void pb2_Click(object sender, EventArgs e)
        {
            lbl12.Text = "ARARENK / Turuncu-Orange";
        }

        private void pb1_Click(object sender, EventArgs e)
        {
            lbl13.Text = "ANARENK / Kırmızı-Red";
        }

        private void pb6_Click(object sender, EventArgs e)
        {
            lbl14.Text = "ARARENK / Mor-Violet";
        }

        private void pb5_Click(object sender, EventArgs e)
        {
            lbl15.Text = "ANARENK / Mavi-Blue";
        }

        private void pb4_Click(object sender, EventArgs e)
        {
            lbl16.Text = "ARARENK / Yeşil-Green";
        }

       
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CocukEgitimSeti
{
    public partial class frm_dersler : Form
    {
        public frm_dersler()
        {
            InitializeComponent();
        }

        private void btn_index_Click(object sender, EventArgs e)
        {
            form_index index = new form_index();
            index.Show();
            this.Hide();
        }

        private void btZamir_Click(object sender, EventArgs e)
        {
            
            List<Zamirler> zamirListesi = new List<Zamirler>();
            CocukEgitimSetiEntities dersZamirler = new CocukEgitimSetiEntities();
            zamirListesi = dersZamirler.Zamirler.ToList();
            gvDersler.DataSource = zamirListesi;
            gvDersler.Show();
        }

        private void btIsim_Click(object sender, EventArgs e)
        {
            List<Isimler> isimListesi = new List<Isimler>();
            CocukEgitimSetiEntities dersIsimler = new CocukEgitimSetiEntities();
            isimListesi = dersIsimler.Isimler.ToList();
            gvDersler.DataSource = isimListesi;
            gvDersler.Show();
        }

        private void btDil_Click(object sender, EventArgs e)
        {
            List<Diller> dilListesi = new List<Diller>();
            CocukEgitimSetiEntities dersDiller = new CocukEgitimSetiEntities();
            dilListesi = dersDiller.Diller.ToList();
            gvDersler.DataSource = dilListesi;
            gvDersler.Show();
        }

        private void btSifat_Click(object sender, EventArgs e)
        {
            List<Sifatlar> sifatListesi = new List<Sifatlar>();
            CocukEgitimSetiEntities dersSifatlar = new CocukEgitimSetiEntities();
            sifatListesi = dersSifatlar.Sifatlar.ToList();
            gvDersler.DataSource = sifatListesi;
            gvDersler.Show();
        }

        private void btIyelik_Click(object sender, EventArgs e)
        {
            List<Iyelik> iyelikListesi = new List<Iyelik>();
            CocukEgitimSetiEntities dersIyelik = new CocukEgitimSetiEntities();
            iyelikListesi = dersIyelik.Iyelik.ToList();
            gvDersler.DataSource = iyelikListesi;
            gvDersler.Show();
        }

        private void btSoru_Click(object sender, EventArgs e)
        {
            List<Sorular> soruListesi = new List<Sorular>();
            CocukEgitimSetiEntities dersSorular = new CocukEgitimSetiEntities();
            soruListesi = dersSorular.Sorular.ToList();
            gvDersler.DataSource = soruListesi;
            gvDersler.Show();
        }

        private void btYer_Click(object sender, EventArgs e)
        {
            List<Yer> yerListesi = new List<Yer>();
            CocukEgitimSetiEntities dersYer = new CocukEgitimSetiEntities();
            yerListesi = dersYer.Yer.ToList();
            gvDersler.DataSource = yerListesi;
            gvDersler.Show();
        }

        private void btDzensizFl_Click(object sender, EventArgs e)
        {
            List<DuzensizFiiller> duzensizfiilListesi = new List<DuzensizFiiller>();
            CocukEgitimSetiEntities dersDzensizfl = new CocukEgitimSetiEntities();
            duzensizfiilListesi = dersDzensizfl.DuzensizFiiller.ToList();
            gvDersler.DataSource = duzensizfiilListesi;
            gvDersler.Show();
        }

        private void frm_dersler_Activated(object sender, EventArgs e)
        {
            gvDersler.Hide();
        }

       

       
    }
}

﻿namespace CocukEgitimSeti
{
    partial class Harfler
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Harfler));
            this.btn1 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.btn4 = new System.Windows.Forms.Button();
            this.btn5 = new System.Windows.Forms.Button();
            this.btn6 = new System.Windows.Forms.Button();
            this.btn7 = new System.Windows.Forms.Button();
            this.btn8 = new System.Windows.Forms.Button();
            this.btn9 = new System.Windows.Forms.Button();
            this.btn10 = new System.Windows.Forms.Button();
            this.btn11 = new System.Windows.Forms.Button();
            this.btn12 = new System.Windows.Forms.Button();
            this.btn13 = new System.Windows.Forms.Button();
            this.btn14 = new System.Windows.Forms.Button();
            this.btn15 = new System.Windows.Forms.Button();
            this.btn16 = new System.Windows.Forms.Button();
            this.btn17 = new System.Windows.Forms.Button();
            this.btn18 = new System.Windows.Forms.Button();
            this.btn19 = new System.Windows.Forms.Button();
            this.btn20 = new System.Windows.Forms.Button();
            this.btn21 = new System.Windows.Forms.Button();
            this.btn22 = new System.Windows.Forms.Button();
            this.btn23 = new System.Windows.Forms.Button();
            this.btn24 = new System.Windows.Forms.Button();
            this.btn25 = new System.Windows.Forms.Button();
            this.btn26 = new System.Windows.Forms.Button();
            this.btnAna = new System.Windows.Forms.Button();
            this.btnOgr = new System.Windows.Forms.Button();
            this.lbl1 = new System.Windows.Forms.Label();
            this.lbl2 = new System.Windows.Forms.Label();
            this.lbl3 = new System.Windows.Forms.Label();
            this.lbl4 = new System.Windows.Forms.Label();
            this.lbl5 = new System.Windows.Forms.Label();
            this.lbl6 = new System.Windows.Forms.Label();
            this.lbl7 = new System.Windows.Forms.Label();
            this.lbl8 = new System.Windows.Forms.Label();
            this.lbl9 = new System.Windows.Forms.Label();
            this.lbl10 = new System.Windows.Forms.Label();
            this.lbl11 = new System.Windows.Forms.Label();
            this.lbl12 = new System.Windows.Forms.Label();
            this.lbl13 = new System.Windows.Forms.Label();
            this.lbl14 = new System.Windows.Forms.Label();
            this.lbl15 = new System.Windows.Forms.Label();
            this.lbl16 = new System.Windows.Forms.Label();
            this.lbl17 = new System.Windows.Forms.Label();
            this.lbl18 = new System.Windows.Forms.Label();
            this.lbl19 = new System.Windows.Forms.Label();
            this.lbl20 = new System.Windows.Forms.Label();
            this.lbl21 = new System.Windows.Forms.Label();
            this.lbl22 = new System.Windows.Forms.Label();
            this.lbl23 = new System.Windows.Forms.Label();
            this.lbl24 = new System.Windows.Forms.Label();
            this.lbl25 = new System.Windows.Forms.Label();
            this.lbl26 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn1
            // 
            this.btn1.BackColor = System.Drawing.Color.Transparent;
            this.btn1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn1.BackgroundImage")));
            this.btn1.Location = new System.Drawing.Point(22, 12);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(40, 40);
            this.btn1.TabIndex = 2;
            this.btn1.UseVisualStyleBackColor = false;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn2
            // 
            this.btn2.BackColor = System.Drawing.Color.Transparent;
            this.btn2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn2.BackgroundImage")));
            this.btn2.Location = new System.Drawing.Point(22, 58);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(40, 40);
            this.btn2.TabIndex = 3;
            this.btn2.UseVisualStyleBackColor = false;
            this.btn2.Click += new System.EventHandler(this.btn2_Click);
            // 
            // btn3
            // 
            this.btn3.BackColor = System.Drawing.Color.Transparent;
            this.btn3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn3.BackgroundImage")));
            this.btn3.Location = new System.Drawing.Point(22, 104);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(40, 40);
            this.btn3.TabIndex = 4;
            this.btn3.UseVisualStyleBackColor = false;
            this.btn3.Click += new System.EventHandler(this.btn3_Click);
            // 
            // btn4
            // 
            this.btn4.BackColor = System.Drawing.Color.Transparent;
            this.btn4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn4.BackgroundImage")));
            this.btn4.Location = new System.Drawing.Point(22, 150);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(40, 40);
            this.btn4.TabIndex = 5;
            this.btn4.UseVisualStyleBackColor = false;
            this.btn4.Click += new System.EventHandler(this.btn4_Click);
            // 
            // btn5
            // 
            this.btn5.BackColor = System.Drawing.Color.Transparent;
            this.btn5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn5.BackgroundImage")));
            this.btn5.Location = new System.Drawing.Point(22, 196);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(40, 40);
            this.btn5.TabIndex = 6;
            this.btn5.UseVisualStyleBackColor = false;
            this.btn5.Click += new System.EventHandler(this.btn5_Click);
            // 
            // btn6
            // 
            this.btn6.BackColor = System.Drawing.Color.Transparent;
            this.btn6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn6.BackgroundImage")));
            this.btn6.Location = new System.Drawing.Point(22, 242);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(40, 40);
            this.btn6.TabIndex = 7;
            this.btn6.UseVisualStyleBackColor = false;
            this.btn6.Click += new System.EventHandler(this.btn6_Click);
            // 
            // btn7
            // 
            this.btn7.BackColor = System.Drawing.Color.Transparent;
            this.btn7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn7.BackgroundImage")));
            this.btn7.Location = new System.Drawing.Point(22, 289);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(40, 40);
            this.btn7.TabIndex = 8;
            this.btn7.UseVisualStyleBackColor = false;
            this.btn7.Click += new System.EventHandler(this.btn7_Click);
            // 
            // btn8
            // 
            this.btn8.BackColor = System.Drawing.Color.Transparent;
            this.btn8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn8.BackgroundImage")));
            this.btn8.Location = new System.Drawing.Point(22, 336);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(40, 40);
            this.btn8.TabIndex = 9;
            this.btn8.UseVisualStyleBackColor = false;
            this.btn8.Click += new System.EventHandler(this.btn8_Click);
            // 
            // btn9
            // 
            this.btn9.BackColor = System.Drawing.Color.Transparent;
            this.btn9.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn9.BackgroundImage")));
            this.btn9.Location = new System.Drawing.Point(193, 12);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(40, 40);
            this.btn9.TabIndex = 10;
            this.btn9.UseVisualStyleBackColor = false;
            this.btn9.Click += new System.EventHandler(this.btn9_Click);
            // 
            // btn10
            // 
            this.btn10.BackColor = System.Drawing.Color.Transparent;
            this.btn10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn10.BackgroundImage")));
            this.btn10.Location = new System.Drawing.Point(193, 58);
            this.btn10.Name = "btn10";
            this.btn10.Size = new System.Drawing.Size(40, 40);
            this.btn10.TabIndex = 11;
            this.btn10.UseVisualStyleBackColor = false;
            this.btn10.Click += new System.EventHandler(this.btn10_Click);
            // 
            // btn11
            // 
            this.btn11.BackColor = System.Drawing.Color.Transparent;
            this.btn11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn11.BackgroundImage")));
            this.btn11.Location = new System.Drawing.Point(193, 104);
            this.btn11.Name = "btn11";
            this.btn11.Size = new System.Drawing.Size(40, 40);
            this.btn11.TabIndex = 12;
            this.btn11.UseVisualStyleBackColor = false;
            this.btn11.Click += new System.EventHandler(this.btn11_Click);
            // 
            // btn12
            // 
            this.btn12.BackColor = System.Drawing.Color.Transparent;
            this.btn12.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn12.BackgroundImage")));
            this.btn12.Location = new System.Drawing.Point(193, 150);
            this.btn12.Name = "btn12";
            this.btn12.Size = new System.Drawing.Size(40, 40);
            this.btn12.TabIndex = 13;
            this.btn12.UseVisualStyleBackColor = false;
            this.btn12.Click += new System.EventHandler(this.btn12_Click);
            // 
            // btn13
            // 
            this.btn13.BackColor = System.Drawing.Color.Transparent;
            this.btn13.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn13.BackgroundImage")));
            this.btn13.Location = new System.Drawing.Point(193, 196);
            this.btn13.Name = "btn13";
            this.btn13.Size = new System.Drawing.Size(40, 40);
            this.btn13.TabIndex = 14;
            this.btn13.UseVisualStyleBackColor = false;
            this.btn13.Click += new System.EventHandler(this.btn13_Click);
            // 
            // btn14
            // 
            this.btn14.BackColor = System.Drawing.Color.Transparent;
            this.btn14.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn14.BackgroundImage")));
            this.btn14.Location = new System.Drawing.Point(193, 242);
            this.btn14.Name = "btn14";
            this.btn14.Size = new System.Drawing.Size(40, 40);
            this.btn14.TabIndex = 15;
            this.btn14.UseVisualStyleBackColor = false;
            this.btn14.Click += new System.EventHandler(this.btn14_Click);
            // 
            // btn15
            // 
            this.btn15.BackColor = System.Drawing.Color.Transparent;
            this.btn15.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn15.BackgroundImage")));
            this.btn15.Location = new System.Drawing.Point(193, 289);
            this.btn15.Name = "btn15";
            this.btn15.Size = new System.Drawing.Size(40, 40);
            this.btn15.TabIndex = 16;
            this.btn15.UseVisualStyleBackColor = false;
            this.btn15.Click += new System.EventHandler(this.btn15_Click);
            // 
            // btn16
            // 
            this.btn16.BackColor = System.Drawing.Color.Transparent;
            this.btn16.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn16.BackgroundImage")));
            this.btn16.Location = new System.Drawing.Point(193, 336);
            this.btn16.Name = "btn16";
            this.btn16.Size = new System.Drawing.Size(40, 40);
            this.btn16.TabIndex = 17;
            this.btn16.UseVisualStyleBackColor = false;
            this.btn16.Click += new System.EventHandler(this.btn16_Click);
            // 
            // btn17
            // 
            this.btn17.BackColor = System.Drawing.Color.Transparent;
            this.btn17.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn17.BackgroundImage")));
            this.btn17.Location = new System.Drawing.Point(378, 12);
            this.btn17.Name = "btn17";
            this.btn17.Size = new System.Drawing.Size(40, 40);
            this.btn17.TabIndex = 18;
            this.btn17.UseVisualStyleBackColor = false;
            this.btn17.Click += new System.EventHandler(this.btn17_Click);
            // 
            // btn18
            // 
            this.btn18.BackColor = System.Drawing.Color.Transparent;
            this.btn18.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn18.BackgroundImage")));
            this.btn18.Location = new System.Drawing.Point(378, 58);
            this.btn18.Name = "btn18";
            this.btn18.Size = new System.Drawing.Size(40, 40);
            this.btn18.TabIndex = 19;
            this.btn18.UseVisualStyleBackColor = false;
            this.btn18.Click += new System.EventHandler(this.btn18_Click);
            // 
            // btn19
            // 
            this.btn19.BackColor = System.Drawing.Color.Transparent;
            this.btn19.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn19.BackgroundImage")));
            this.btn19.Location = new System.Drawing.Point(378, 104);
            this.btn19.Name = "btn19";
            this.btn19.Size = new System.Drawing.Size(40, 40);
            this.btn19.TabIndex = 20;
            this.btn19.UseVisualStyleBackColor = false;
            this.btn19.Click += new System.EventHandler(this.btn19_Click);
            // 
            // btn20
            // 
            this.btn20.BackColor = System.Drawing.Color.Transparent;
            this.btn20.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn20.BackgroundImage")));
            this.btn20.Location = new System.Drawing.Point(378, 150);
            this.btn20.Name = "btn20";
            this.btn20.Size = new System.Drawing.Size(40, 40);
            this.btn20.TabIndex = 21;
            this.btn20.UseVisualStyleBackColor = false;
            this.btn20.Click += new System.EventHandler(this.btn20_Click);
            // 
            // btn21
            // 
            this.btn21.BackColor = System.Drawing.Color.Transparent;
            this.btn21.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn21.BackgroundImage")));
            this.btn21.Location = new System.Drawing.Point(378, 196);
            this.btn21.Name = "btn21";
            this.btn21.Size = new System.Drawing.Size(40, 40);
            this.btn21.TabIndex = 22;
            this.btn21.UseVisualStyleBackColor = false;
            this.btn21.Click += new System.EventHandler(this.btn21_Click);
            // 
            // btn22
            // 
            this.btn22.BackColor = System.Drawing.Color.Transparent;
            this.btn22.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn22.BackgroundImage")));
            this.btn22.Location = new System.Drawing.Point(378, 242);
            this.btn22.Name = "btn22";
            this.btn22.Size = new System.Drawing.Size(40, 40);
            this.btn22.TabIndex = 23;
            this.btn22.UseVisualStyleBackColor = false;
            this.btn22.Click += new System.EventHandler(this.btn22_Click);
            // 
            // btn23
            // 
            this.btn23.BackColor = System.Drawing.Color.Transparent;
            this.btn23.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn23.BackgroundImage")));
            this.btn23.Location = new System.Drawing.Point(378, 289);
            this.btn23.Name = "btn23";
            this.btn23.Size = new System.Drawing.Size(40, 40);
            this.btn23.TabIndex = 24;
            this.btn23.UseVisualStyleBackColor = false;
            this.btn23.Click += new System.EventHandler(this.btn23_Click);
            // 
            // btn24
            // 
            this.btn24.BackColor = System.Drawing.Color.Transparent;
            this.btn24.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn24.BackgroundImage")));
            this.btn24.Location = new System.Drawing.Point(378, 336);
            this.btn24.Name = "btn24";
            this.btn24.Size = new System.Drawing.Size(40, 40);
            this.btn24.TabIndex = 25;
            this.btn24.UseVisualStyleBackColor = false;
            this.btn24.Click += new System.EventHandler(this.btn24_Click);
            // 
            // btn25
            // 
            this.btn25.BackColor = System.Drawing.Color.Transparent;
            this.btn25.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn25.BackgroundImage")));
            this.btn25.Location = new System.Drawing.Point(553, 12);
            this.btn25.Name = "btn25";
            this.btn25.Size = new System.Drawing.Size(40, 40);
            this.btn25.TabIndex = 26;
            this.btn25.UseVisualStyleBackColor = false;
            this.btn25.Click += new System.EventHandler(this.btn25_Click);
            // 
            // btn26
            // 
            this.btn26.BackColor = System.Drawing.Color.Transparent;
            this.btn26.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn26.BackgroundImage")));
            this.btn26.Location = new System.Drawing.Point(553, 58);
            this.btn26.Name = "btn26";
            this.btn26.Size = new System.Drawing.Size(40, 40);
            this.btn26.TabIndex = 27;
            this.btn26.UseVisualStyleBackColor = false;
            this.btn26.Click += new System.EventHandler(this.btn26_Click);
            // 
            // btnAna
            // 
            this.btnAna.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnAna.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnAna.Location = new System.Drawing.Point(566, 242);
            this.btnAna.Name = "btnAna";
            this.btnAna.Size = new System.Drawing.Size(148, 40);
            this.btnAna.TabIndex = 58;
            this.btnAna.Text = "ANASAYFA";
            this.btnAna.UseVisualStyleBackColor = false;
            this.btnAna.Click += new System.EventHandler(this.btnAna_Click);
            // 
            // btnOgr
            // 
            this.btnOgr.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.btnOgr.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnOgr.Location = new System.Drawing.Point(566, 288);
            this.btnOgr.Name = "btnOgr";
            this.btnOgr.Size = new System.Drawing.Size(148, 40);
            this.btnOgr.TabIndex = 59;
            this.btnOgr.Text = "ÖĞRENELİM";
            this.btnOgr.UseVisualStyleBackColor = false;
            this.btnOgr.Click += new System.EventHandler(this.btnOgr_Click);
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.BackColor = System.Drawing.Color.Transparent;
            this.lbl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl1.ForeColor = System.Drawing.Color.Black;
            this.lbl1.Location = new System.Drawing.Point(70, 32);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(0, 20);
            this.lbl1.TabIndex = 60;
            // 
            // lbl2
            // 
            this.lbl2.AutoSize = true;
            this.lbl2.BackColor = System.Drawing.Color.Transparent;
            this.lbl2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl2.ForeColor = System.Drawing.Color.Black;
            this.lbl2.Location = new System.Drawing.Point(70, 78);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(0, 20);
            this.lbl2.TabIndex = 61;
            // 
            // lbl3
            // 
            this.lbl3.AutoSize = true;
            this.lbl3.BackColor = System.Drawing.Color.Transparent;
            this.lbl3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl3.ForeColor = System.Drawing.Color.Black;
            this.lbl3.Location = new System.Drawing.Point(70, 124);
            this.lbl3.Name = "lbl3";
            this.lbl3.Size = new System.Drawing.Size(0, 20);
            this.lbl3.TabIndex = 62;
            // 
            // lbl4
            // 
            this.lbl4.AutoSize = true;
            this.lbl4.BackColor = System.Drawing.Color.Transparent;
            this.lbl4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl4.ForeColor = System.Drawing.Color.Black;
            this.lbl4.Location = new System.Drawing.Point(70, 170);
            this.lbl4.Name = "lbl4";
            this.lbl4.Size = new System.Drawing.Size(0, 20);
            this.lbl4.TabIndex = 63;
            // 
            // lbl5
            // 
            this.lbl5.AutoSize = true;
            this.lbl5.BackColor = System.Drawing.Color.Transparent;
            this.lbl5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl5.ForeColor = System.Drawing.Color.Black;
            this.lbl5.Location = new System.Drawing.Point(70, 216);
            this.lbl5.Name = "lbl5";
            this.lbl5.Size = new System.Drawing.Size(0, 20);
            this.lbl5.TabIndex = 64;
            // 
            // lbl6
            // 
            this.lbl6.AutoSize = true;
            this.lbl6.BackColor = System.Drawing.Color.Transparent;
            this.lbl6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl6.ForeColor = System.Drawing.Color.Black;
            this.lbl6.Location = new System.Drawing.Point(70, 262);
            this.lbl6.Name = "lbl6";
            this.lbl6.Size = new System.Drawing.Size(0, 20);
            this.lbl6.TabIndex = 65;
            // 
            // lbl7
            // 
            this.lbl7.AutoSize = true;
            this.lbl7.BackColor = System.Drawing.Color.Transparent;
            this.lbl7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl7.ForeColor = System.Drawing.Color.Black;
            this.lbl7.Location = new System.Drawing.Point(70, 309);
            this.lbl7.Name = "lbl7";
            this.lbl7.Size = new System.Drawing.Size(0, 20);
            this.lbl7.TabIndex = 66;
            // 
            // lbl8
            // 
            this.lbl8.AutoSize = true;
            this.lbl8.BackColor = System.Drawing.Color.Transparent;
            this.lbl8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl8.ForeColor = System.Drawing.Color.Black;
            this.lbl8.Location = new System.Drawing.Point(70, 356);
            this.lbl8.Name = "lbl8";
            this.lbl8.Size = new System.Drawing.Size(0, 20);
            this.lbl8.TabIndex = 67;
            // 
            // lbl9
            // 
            this.lbl9.AutoSize = true;
            this.lbl9.BackColor = System.Drawing.Color.Transparent;
            this.lbl9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl9.ForeColor = System.Drawing.Color.Black;
            this.lbl9.Location = new System.Drawing.Point(252, 32);
            this.lbl9.Name = "lbl9";
            this.lbl9.Size = new System.Drawing.Size(0, 20);
            this.lbl9.TabIndex = 68;
            // 
            // lbl10
            // 
            this.lbl10.AutoSize = true;
            this.lbl10.BackColor = System.Drawing.Color.Transparent;
            this.lbl10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl10.ForeColor = System.Drawing.Color.Black;
            this.lbl10.Location = new System.Drawing.Point(252, 78);
            this.lbl10.Name = "lbl10";
            this.lbl10.Size = new System.Drawing.Size(0, 20);
            this.lbl10.TabIndex = 69;
            // 
            // lbl11
            // 
            this.lbl11.AutoSize = true;
            this.lbl11.BackColor = System.Drawing.Color.Transparent;
            this.lbl11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl11.ForeColor = System.Drawing.Color.Black;
            this.lbl11.Location = new System.Drawing.Point(252, 124);
            this.lbl11.Name = "lbl11";
            this.lbl11.Size = new System.Drawing.Size(0, 20);
            this.lbl11.TabIndex = 70;
            // 
            // lbl12
            // 
            this.lbl12.AutoSize = true;
            this.lbl12.BackColor = System.Drawing.Color.Transparent;
            this.lbl12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl12.ForeColor = System.Drawing.Color.Black;
            this.lbl12.Location = new System.Drawing.Point(252, 170);
            this.lbl12.Name = "lbl12";
            this.lbl12.Size = new System.Drawing.Size(0, 20);
            this.lbl12.TabIndex = 71;
            // 
            // lbl13
            // 
            this.lbl13.AutoSize = true;
            this.lbl13.BackColor = System.Drawing.Color.Transparent;
            this.lbl13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl13.ForeColor = System.Drawing.Color.Black;
            this.lbl13.Location = new System.Drawing.Point(252, 216);
            this.lbl13.Name = "lbl13";
            this.lbl13.Size = new System.Drawing.Size(0, 20);
            this.lbl13.TabIndex = 72;
            // 
            // lbl14
            // 
            this.lbl14.AutoSize = true;
            this.lbl14.BackColor = System.Drawing.Color.Transparent;
            this.lbl14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl14.ForeColor = System.Drawing.Color.Black;
            this.lbl14.Location = new System.Drawing.Point(252, 262);
            this.lbl14.Name = "lbl14";
            this.lbl14.Size = new System.Drawing.Size(0, 20);
            this.lbl14.TabIndex = 73;
            // 
            // lbl15
            // 
            this.lbl15.AutoSize = true;
            this.lbl15.BackColor = System.Drawing.Color.Transparent;
            this.lbl15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl15.ForeColor = System.Drawing.Color.Black;
            this.lbl15.Location = new System.Drawing.Point(252, 309);
            this.lbl15.Name = "lbl15";
            this.lbl15.Size = new System.Drawing.Size(0, 20);
            this.lbl15.TabIndex = 74;
            // 
            // lbl16
            // 
            this.lbl16.AutoSize = true;
            this.lbl16.BackColor = System.Drawing.Color.Transparent;
            this.lbl16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl16.ForeColor = System.Drawing.Color.Black;
            this.lbl16.Location = new System.Drawing.Point(252, 356);
            this.lbl16.Name = "lbl16";
            this.lbl16.Size = new System.Drawing.Size(0, 20);
            this.lbl16.TabIndex = 75;
            // 
            // lbl17
            // 
            this.lbl17.AutoSize = true;
            this.lbl17.BackColor = System.Drawing.Color.Transparent;
            this.lbl17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl17.ForeColor = System.Drawing.Color.Black;
            this.lbl17.Location = new System.Drawing.Point(437, 32);
            this.lbl17.Name = "lbl17";
            this.lbl17.Size = new System.Drawing.Size(0, 20);
            this.lbl17.TabIndex = 76;
            // 
            // lbl18
            // 
            this.lbl18.AutoSize = true;
            this.lbl18.BackColor = System.Drawing.Color.Transparent;
            this.lbl18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl18.ForeColor = System.Drawing.Color.Black;
            this.lbl18.Location = new System.Drawing.Point(437, 78);
            this.lbl18.Name = "lbl18";
            this.lbl18.Size = new System.Drawing.Size(0, 20);
            this.lbl18.TabIndex = 77;
            // 
            // lbl19
            // 
            this.lbl19.AutoSize = true;
            this.lbl19.BackColor = System.Drawing.Color.Transparent;
            this.lbl19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl19.ForeColor = System.Drawing.Color.Black;
            this.lbl19.Location = new System.Drawing.Point(437, 124);
            this.lbl19.Name = "lbl19";
            this.lbl19.Size = new System.Drawing.Size(0, 20);
            this.lbl19.TabIndex = 78;
            // 
            // lbl20
            // 
            this.lbl20.AutoSize = true;
            this.lbl20.BackColor = System.Drawing.Color.Transparent;
            this.lbl20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl20.ForeColor = System.Drawing.Color.Black;
            this.lbl20.Location = new System.Drawing.Point(437, 170);
            this.lbl20.Name = "lbl20";
            this.lbl20.Size = new System.Drawing.Size(0, 20);
            this.lbl20.TabIndex = 79;
            // 
            // lbl21
            // 
            this.lbl21.AutoSize = true;
            this.lbl21.BackColor = System.Drawing.Color.Transparent;
            this.lbl21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl21.ForeColor = System.Drawing.Color.Black;
            this.lbl21.Location = new System.Drawing.Point(437, 216);
            this.lbl21.Name = "lbl21";
            this.lbl21.Size = new System.Drawing.Size(0, 20);
            this.lbl21.TabIndex = 80;
            // 
            // lbl22
            // 
            this.lbl22.AutoSize = true;
            this.lbl22.BackColor = System.Drawing.Color.Transparent;
            this.lbl22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl22.ForeColor = System.Drawing.Color.Black;
            this.lbl22.Location = new System.Drawing.Point(437, 262);
            this.lbl22.Name = "lbl22";
            this.lbl22.Size = new System.Drawing.Size(0, 20);
            this.lbl22.TabIndex = 81;
            // 
            // lbl23
            // 
            this.lbl23.AutoSize = true;
            this.lbl23.BackColor = System.Drawing.Color.Transparent;
            this.lbl23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl23.ForeColor = System.Drawing.Color.Black;
            this.lbl23.Location = new System.Drawing.Point(437, 309);
            this.lbl23.Name = "lbl23";
            this.lbl23.Size = new System.Drawing.Size(0, 20);
            this.lbl23.TabIndex = 82;
            // 
            // lbl24
            // 
            this.lbl24.AutoSize = true;
            this.lbl24.BackColor = System.Drawing.Color.Transparent;
            this.lbl24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl24.ForeColor = System.Drawing.Color.Black;
            this.lbl24.Location = new System.Drawing.Point(437, 356);
            this.lbl24.Name = "lbl24";
            this.lbl24.Size = new System.Drawing.Size(0, 20);
            this.lbl24.TabIndex = 83;
            // 
            // lbl25
            // 
            this.lbl25.AutoSize = true;
            this.lbl25.BackColor = System.Drawing.Color.Transparent;
            this.lbl25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl25.ForeColor = System.Drawing.Color.Black;
            this.lbl25.Location = new System.Drawing.Point(608, 32);
            this.lbl25.Name = "lbl25";
            this.lbl25.Size = new System.Drawing.Size(0, 20);
            this.lbl25.TabIndex = 84;
            // 
            // lbl26
            // 
            this.lbl26.AutoSize = true;
            this.lbl26.BackColor = System.Drawing.Color.Transparent;
            this.lbl26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl26.ForeColor = System.Drawing.Color.Black;
            this.lbl26.Location = new System.Drawing.Point(608, 78);
            this.lbl26.Name = "lbl26";
            this.lbl26.Size = new System.Drawing.Size(0, 20);
            this.lbl26.TabIndex = 85;
            // 
            // Harfler
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(808, 478);
            this.Controls.Add(this.lbl26);
            this.Controls.Add(this.lbl25);
            this.Controls.Add(this.lbl24);
            this.Controls.Add(this.lbl23);
            this.Controls.Add(this.lbl22);
            this.Controls.Add(this.lbl21);
            this.Controls.Add(this.lbl20);
            this.Controls.Add(this.lbl19);
            this.Controls.Add(this.lbl18);
            this.Controls.Add(this.lbl17);
            this.Controls.Add(this.lbl16);
            this.Controls.Add(this.lbl15);
            this.Controls.Add(this.lbl14);
            this.Controls.Add(this.lbl13);
            this.Controls.Add(this.lbl12);
            this.Controls.Add(this.lbl11);
            this.Controls.Add(this.lbl10);
            this.Controls.Add(this.lbl9);
            this.Controls.Add(this.lbl8);
            this.Controls.Add(this.lbl7);
            this.Controls.Add(this.lbl6);
            this.Controls.Add(this.lbl5);
            this.Controls.Add(this.lbl4);
            this.Controls.Add(this.lbl3);
            this.Controls.Add(this.lbl2);
            this.Controls.Add(this.lbl1);
            this.Controls.Add(this.btnOgr);
            this.Controls.Add(this.btnAna);
            this.Controls.Add(this.btn26);
            this.Controls.Add(this.btn25);
            this.Controls.Add(this.btn24);
            this.Controls.Add(this.btn23);
            this.Controls.Add(this.btn22);
            this.Controls.Add(this.btn21);
            this.Controls.Add(this.btn20);
            this.Controls.Add(this.btn19);
            this.Controls.Add(this.btn18);
            this.Controls.Add(this.btn17);
            this.Controls.Add(this.btn16);
            this.Controls.Add(this.btn15);
            this.Controls.Add(this.btn14);
            this.Controls.Add(this.btn13);
            this.Controls.Add(this.btn12);
            this.Controls.Add(this.btn11);
            this.Controls.Add(this.btn10);
            this.Controls.Add(this.btn9);
            this.Controls.Add(this.btn8);
            this.Controls.Add(this.btn7);
            this.Controls.Add(this.btn6);
            this.Controls.Add(this.btn5);
            this.Controls.Add(this.btn4);
            this.Controls.Add(this.btn3);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btn1);
            this.Name = "Harfler";
            this.Text = "Harfler";
            this.Activated += new System.EventHandler(this.Harfler_Activated);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.Button btn4;
        private System.Windows.Forms.Button btn5;
        private System.Windows.Forms.Button btn6;
        private System.Windows.Forms.Button btn7;
        private System.Windows.Forms.Button btn8;
        private System.Windows.Forms.Button btn9;
        private System.Windows.Forms.Button btn10;
        private System.Windows.Forms.Button btn11;
        private System.Windows.Forms.Button btn12;
        private System.Windows.Forms.Button btn13;
        private System.Windows.Forms.Button btn14;
        private System.Windows.Forms.Button btn15;
        private System.Windows.Forms.Button btn16;
        private System.Windows.Forms.Button btn17;
        private System.Windows.Forms.Button btn18;
        private System.Windows.Forms.Button btn19;
        private System.Windows.Forms.Button btn20;
        private System.Windows.Forms.Button btn21;
        private System.Windows.Forms.Button btn22;
        private System.Windows.Forms.Button btn23;
        private System.Windows.Forms.Button btn24;
        private System.Windows.Forms.Button btn25;
        private System.Windows.Forms.Button btn26;
        private System.Windows.Forms.Button btnAna;
        private System.Windows.Forms.Button btnOgr;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Label lbl2;
        private System.Windows.Forms.Label lbl3;
        private System.Windows.Forms.Label lbl4;
        private System.Windows.Forms.Label lbl5;
        private System.Windows.Forms.Label lbl6;
        private System.Windows.Forms.Label lbl7;
        private System.Windows.Forms.Label lbl8;
        private System.Windows.Forms.Label lbl9;
        private System.Windows.Forms.Label lbl10;
        private System.Windows.Forms.Label lbl11;
        private System.Windows.Forms.Label lbl12;
        private System.Windows.Forms.Label lbl13;
        private System.Windows.Forms.Label lbl14;
        private System.Windows.Forms.Label lbl15;
        private System.Windows.Forms.Label lbl16;
        private System.Windows.Forms.Label lbl17;
        private System.Windows.Forms.Label lbl18;
        private System.Windows.Forms.Label lbl19;
        private System.Windows.Forms.Label lbl20;
        private System.Windows.Forms.Label lbl21;
        private System.Windows.Forms.Label lbl22;
        private System.Windows.Forms.Label lbl23;
        private System.Windows.Forms.Label lbl24;
        private System.Windows.Forms.Label lbl25;
        private System.Windows.Forms.Label lbl26;
    }
}
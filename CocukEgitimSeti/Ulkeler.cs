﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CocukEgitimSeti
{
    public partial class Ulkeler : Form
    {
        public Ulkeler()
        {
            InitializeComponent();
        }

        private void btnAna_Click(object sender, EventArgs e)
        {
            form_index ind = new form_index();
            ind.Show();
            this.Hide();
        }

        private void btnOgr_Click(object sender, EventArgs e)
        {
            Ogrenelim ogr = new Ogrenelim();
            ogr.Show();
            this.Hide();
        }

        private void bttn1_Click(object sender, EventArgs e)
        {
            lbl1.Text = "TURKEY";
        }

        private void bttn2_Click(object sender, EventArgs e)
        {
            lbl2.Text = "USA";
        }

        private void bttn3_Click(object sender, EventArgs e)
        {
            lbl3.Text = "ARGENTİNA";
        }

        private void bttn4_Click(object sender, EventArgs e)
        {
            lbl4.Text = "BULGARİA";
        }

        private void bttn5_Click(object sender, EventArgs e)
        {
            lbl5.Text = "BRAZİL";
        }

        private void bttn6_Click(object sender, EventArgs e)
        {
            lbl6.Text = "DENMARK";
        }

        private void bttn7_Click(object sender, EventArgs e)
        {
            lbl7.Text = "FRANCE";
        }

        private void bttn8_Click(object sender, EventArgs e)
        {
            lbl8.Text = "SOUTH AFRİCA";
        }

        private void bttn9_Click(object sender, EventArgs e)
        {
            lbl9.Text = "NETHERLAND";
        }

        private void bttn10_Click(object sender, EventArgs e)
        {
            lbl10.Text = "İNDİA";
        }

        private void bttn11_Click(object sender, EventArgs e)
        {
            lbl11.Text = "ENGLAND";
        }

        private void bttn12_Click(object sender, EventArgs e)
        {
            lbl12.Text = "SWEDEN";
        }

        private void bttn13_Click(object sender, EventArgs e)
        {
            lbl13.Text = "ITALY";
        }

        private void bttn14_Click(object sender, EventArgs e)
        {
            lbl14.Text = "JAPAN";
        }

        private void bttn15_Click(object sender, EventArgs e)
        {
            lbl15.Text = "EGYPT";
        }

        private void bttn16_Click(object sender, EventArgs e)
        {
            lbl16.Text = "RUSSİA";
        }

        private void bttn17_Click(object sender, EventArgs e)
        {
            lbl17.Text = "GERMANY";
        }

        private void button17_Click(object sender, EventArgs e)
        {
            lbl18.Text = "GREECE";
        }
    }
}

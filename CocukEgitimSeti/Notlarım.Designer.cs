﻿namespace CocukEgitimSeti
{
    partial class Notlarım
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bttn1 = new System.Windows.Forms.Button();
            this.bttn2 = new System.Windows.Forms.Button();
            this.lbl1 = new System.Windows.Forms.Label();
            this.bttn3 = new System.Windows.Forms.Button();
            this.lbl2 = new System.Windows.Forms.Label();
            this.bttn4 = new System.Windows.Forms.Button();
            this.bttn5 = new System.Windows.Forms.Button();
            this.bttn6 = new System.Windows.Forms.Button();
            this.bttn7 = new System.Windows.Forms.Button();
            this.bttn8 = new System.Windows.Forms.Button();
            this.lbl3 = new System.Windows.Forms.Label();
            this.lbl4 = new System.Windows.Forms.Label();
            this.lbl5 = new System.Windows.Forms.Label();
            this.lbl6 = new System.Windows.Forms.Label();
            this.lbl7 = new System.Windows.Forms.Label();
            this.bttn9 = new System.Windows.Forms.Button();
            this.bttn10 = new System.Windows.Forms.Button();
            this.bttn11 = new System.Windows.Forms.Button();
            this.bttn12 = new System.Windows.Forms.Button();
            this.bttn13 = new System.Windows.Forms.Button();
            this.bttn14 = new System.Windows.Forms.Button();
            this.lbl8 = new System.Windows.Forms.Label();
            this.lbl9 = new System.Windows.Forms.Label();
            this.lbl10 = new System.Windows.Forms.Label();
            this.lbl11 = new System.Windows.Forms.Label();
            this.lbl12 = new System.Windows.Forms.Label();
            this.lbl13 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // bttn1
            // 
            this.bttn1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.bttn1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn1.Location = new System.Drawing.Point(365, 12);
            this.bttn1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.bttn1.Name = "bttn1";
            this.bttn1.Size = new System.Drawing.Size(148, 40);
            this.bttn1.TabIndex = 0;
            this.bttn1.Text = "ANASAYFA";
            this.bttn1.UseVisualStyleBackColor = false;
            this.bttn1.Click += new System.EventHandler(this.bttn1_Click);
            // 
            // bttn2
            // 
            this.bttn2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.bttn2.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.Evet;
            this.bttn2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn2.ForeColor = System.Drawing.Color.Black;
            this.bttn2.Location = new System.Drawing.Point(12, 12);
            this.bttn2.Name = "bttn2";
            this.bttn2.Size = new System.Drawing.Size(108, 25);
            this.bttn2.TabIndex = 2;
            this.bttn2.UseVisualStyleBackColor = false;
            this.bttn2.Click += new System.EventHandler(this.bttn2_Click);
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.BackColor = System.Drawing.Color.Transparent;
            this.lbl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl1.ForeColor = System.Drawing.Color.Black;
            this.lbl1.Location = new System.Drawing.Point(137, 14);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(0, 20);
            this.lbl1.TabIndex = 3;
            // 
            // bttn3
            // 
            this.bttn3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.bttn3.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.Hayır;
            this.bttn3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn3.ForeColor = System.Drawing.Color.Black;
            this.bttn3.Location = new System.Drawing.Point(12, 43);
            this.bttn3.Name = "bttn3";
            this.bttn3.Size = new System.Drawing.Size(108, 25);
            this.bttn3.TabIndex = 5;
            this.bttn3.UseVisualStyleBackColor = false;
            this.bttn3.Click += new System.EventHandler(this.bttn3_Click);
            // 
            // lbl2
            // 
            this.lbl2.AutoSize = true;
            this.lbl2.BackColor = System.Drawing.Color.Transparent;
            this.lbl2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl2.ForeColor = System.Drawing.Color.Black;
            this.lbl2.Location = new System.Drawing.Point(137, 45);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(0, 20);
            this.lbl2.TabIndex = 6;
            // 
            // bttn4
            // 
            this.bttn4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.bttn4.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.Afedersiniz;
            this.bttn4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn4.ForeColor = System.Drawing.Color.Black;
            this.bttn4.Location = new System.Drawing.Point(12, 74);
            this.bttn4.Name = "bttn4";
            this.bttn4.Size = new System.Drawing.Size(108, 25);
            this.bttn4.TabIndex = 14;
            this.bttn4.UseVisualStyleBackColor = false;
            this.bttn4.Click += new System.EventHandler(this.bttn4_Click);
            // 
            // bttn5
            // 
            this.bttn5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.bttn5.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.Ozur;
            this.bttn5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn5.ForeColor = System.Drawing.Color.Black;
            this.bttn5.Location = new System.Drawing.Point(12, 105);
            this.bttn5.Name = "bttn5";
            this.bttn5.Size = new System.Drawing.Size(108, 25);
            this.bttn5.TabIndex = 16;
            this.bttn5.UseVisualStyleBackColor = false;
            this.bttn5.Click += new System.EventHandler(this.bttn5_Click);
            // 
            // bttn6
            // 
            this.bttn6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.bttn6.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.Lutfen;
            this.bttn6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn6.ForeColor = System.Drawing.Color.Black;
            this.bttn6.Location = new System.Drawing.Point(12, 134);
            this.bttn6.Name = "bttn6";
            this.bttn6.Size = new System.Drawing.Size(108, 25);
            this.bttn6.TabIndex = 18;
            this.bttn6.UseVisualStyleBackColor = false;
            this.bttn6.Click += new System.EventHandler(this.bttn6_Click);
            // 
            // bttn7
            // 
            this.bttn7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.bttn7.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.Pekala;
            this.bttn7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn7.ForeColor = System.Drawing.Color.Black;
            this.bttn7.Location = new System.Drawing.Point(12, 163);
            this.bttn7.Name = "bttn7";
            this.bttn7.Size = new System.Drawing.Size(108, 25);
            this.bttn7.TabIndex = 20;
            this.bttn7.UseVisualStyleBackColor = false;
            this.bttn7.Click += new System.EventHandler(this.bttn7_Click);
            // 
            // bttn8
            // 
            this.bttn8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.bttn8.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.Tamam;
            this.bttn8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn8.ForeColor = System.Drawing.Color.Black;
            this.bttn8.Location = new System.Drawing.Point(12, 192);
            this.bttn8.Name = "bttn8";
            this.bttn8.Size = new System.Drawing.Size(108, 25);
            this.bttn8.TabIndex = 22;
            this.bttn8.UseVisualStyleBackColor = false;
            this.bttn8.Click += new System.EventHandler(this.bttn8_Click);
            // 
            // lbl3
            // 
            this.lbl3.AutoSize = true;
            this.lbl3.BackColor = System.Drawing.Color.Transparent;
            this.lbl3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl3.ForeColor = System.Drawing.Color.Black;
            this.lbl3.Location = new System.Drawing.Point(137, 76);
            this.lbl3.Name = "lbl3";
            this.lbl3.Size = new System.Drawing.Size(0, 20);
            this.lbl3.TabIndex = 24;
            // 
            // lbl4
            // 
            this.lbl4.AutoSize = true;
            this.lbl4.BackColor = System.Drawing.Color.Transparent;
            this.lbl4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl4.ForeColor = System.Drawing.Color.Black;
            this.lbl4.Location = new System.Drawing.Point(137, 107);
            this.lbl4.Name = "lbl4";
            this.lbl4.Size = new System.Drawing.Size(0, 20);
            this.lbl4.TabIndex = 25;
            // 
            // lbl5
            // 
            this.lbl5.AutoSize = true;
            this.lbl5.BackColor = System.Drawing.Color.Transparent;
            this.lbl5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl5.ForeColor = System.Drawing.Color.Black;
            this.lbl5.Location = new System.Drawing.Point(137, 136);
            this.lbl5.Name = "lbl5";
            this.lbl5.Size = new System.Drawing.Size(0, 20);
            this.lbl5.TabIndex = 26;
            // 
            // lbl6
            // 
            this.lbl6.AutoSize = true;
            this.lbl6.BackColor = System.Drawing.Color.Transparent;
            this.lbl6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl6.ForeColor = System.Drawing.Color.Black;
            this.lbl6.Location = new System.Drawing.Point(137, 165);
            this.lbl6.Name = "lbl6";
            this.lbl6.Size = new System.Drawing.Size(0, 20);
            this.lbl6.TabIndex = 27;
            // 
            // lbl7
            // 
            this.lbl7.AutoSize = true;
            this.lbl7.BackColor = System.Drawing.Color.Transparent;
            this.lbl7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl7.ForeColor = System.Drawing.Color.Black;
            this.lbl7.Location = new System.Drawing.Point(137, 194);
            this.lbl7.Name = "lbl7";
            this.lbl7.Size = new System.Drawing.Size(0, 20);
            this.lbl7.TabIndex = 28;
            // 
            // bttn9
            // 
            this.bttn9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.bttn9.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.Gule_Gule;
            this.bttn9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn9.ForeColor = System.Drawing.Color.Black;
            this.bttn9.Location = new System.Drawing.Point(12, 223);
            this.bttn9.Name = "bttn9";
            this.bttn9.Size = new System.Drawing.Size(108, 25);
            this.bttn9.TabIndex = 29;
            this.bttn9.UseVisualStyleBackColor = false;
            this.bttn9.Click += new System.EventHandler(this.bttn9_Click);
            // 
            // bttn10
            // 
            this.bttn10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.bttn10.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.imdat;
            this.bttn10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn10.ForeColor = System.Drawing.Color.Black;
            this.bttn10.Location = new System.Drawing.Point(12, 254);
            this.bttn10.Name = "bttn10";
            this.bttn10.Size = new System.Drawing.Size(108, 25);
            this.bttn10.TabIndex = 31;
            this.bttn10.UseVisualStyleBackColor = false;
            this.bttn10.Click += new System.EventHandler(this.bttn10_Click);
            // 
            // bttn11
            // 
            this.bttn11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.bttn11.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.Bay;
            this.bttn11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn11.ForeColor = System.Drawing.Color.Black;
            this.bttn11.Location = new System.Drawing.Point(12, 285);
            this.bttn11.Name = "bttn11";
            this.bttn11.Size = new System.Drawing.Size(108, 25);
            this.bttn11.TabIndex = 33;
            this.bttn11.UseVisualStyleBackColor = false;
            this.bttn11.Click += new System.EventHandler(this.bttn11_Click);
            // 
            // bttn12
            // 
            this.bttn12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.bttn12.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.Beyfendi;
            this.bttn12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn12.ForeColor = System.Drawing.Color.Black;
            this.bttn12.Location = new System.Drawing.Point(12, 316);
            this.bttn12.Name = "bttn12";
            this.bttn12.Size = new System.Drawing.Size(108, 25);
            this.bttn12.TabIndex = 35;
            this.bttn12.UseVisualStyleBackColor = false;
            this.bttn12.Click += new System.EventHandler(this.bttn12_Click);
            // 
            // bttn13
            // 
            this.bttn13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.bttn13.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.Bayan;
            this.bttn13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn13.ForeColor = System.Drawing.Color.Black;
            this.bttn13.Location = new System.Drawing.Point(12, 347);
            this.bttn13.Name = "bttn13";
            this.bttn13.Size = new System.Drawing.Size(108, 25);
            this.bttn13.TabIndex = 37;
            this.bttn13.UseVisualStyleBackColor = false;
            this.bttn13.Click += new System.EventHandler(this.bttn13_Click);
            // 
            // bttn14
            // 
            this.bttn14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.bttn14.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.Bayann;
            this.bttn14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn14.ForeColor = System.Drawing.Color.Black;
            this.bttn14.Location = new System.Drawing.Point(12, 378);
            this.bttn14.Name = "bttn14";
            this.bttn14.Size = new System.Drawing.Size(108, 25);
            this.bttn14.TabIndex = 39;
            this.bttn14.UseVisualStyleBackColor = false;
            this.bttn14.Click += new System.EventHandler(this.bttn14_Click);
            // 
            // lbl8
            // 
            this.lbl8.AutoSize = true;
            this.lbl8.BackColor = System.Drawing.Color.Transparent;
            this.lbl8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl8.ForeColor = System.Drawing.Color.Black;
            this.lbl8.Location = new System.Drawing.Point(137, 225);
            this.lbl8.Name = "lbl8";
            this.lbl8.Size = new System.Drawing.Size(0, 20);
            this.lbl8.TabIndex = 41;
            // 
            // lbl9
            // 
            this.lbl9.AutoSize = true;
            this.lbl9.BackColor = System.Drawing.Color.Transparent;
            this.lbl9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl9.ForeColor = System.Drawing.Color.Black;
            this.lbl9.Location = new System.Drawing.Point(137, 256);
            this.lbl9.Name = "lbl9";
            this.lbl9.Size = new System.Drawing.Size(0, 20);
            this.lbl9.TabIndex = 42;
            // 
            // lbl10
            // 
            this.lbl10.AutoSize = true;
            this.lbl10.BackColor = System.Drawing.Color.Transparent;
            this.lbl10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl10.ForeColor = System.Drawing.Color.Black;
            this.lbl10.Location = new System.Drawing.Point(137, 287);
            this.lbl10.Name = "lbl10";
            this.lbl10.Size = new System.Drawing.Size(0, 20);
            this.lbl10.TabIndex = 43;
            // 
            // lbl11
            // 
            this.lbl11.AutoSize = true;
            this.lbl11.BackColor = System.Drawing.Color.Transparent;
            this.lbl11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl11.ForeColor = System.Drawing.Color.Black;
            this.lbl11.Location = new System.Drawing.Point(137, 318);
            this.lbl11.Name = "lbl11";
            this.lbl11.Size = new System.Drawing.Size(0, 20);
            this.lbl11.TabIndex = 44;
            // 
            // lbl12
            // 
            this.lbl12.AutoSize = true;
            this.lbl12.BackColor = System.Drawing.Color.Transparent;
            this.lbl12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl12.ForeColor = System.Drawing.Color.Black;
            this.lbl12.Location = new System.Drawing.Point(137, 349);
            this.lbl12.Name = "lbl12";
            this.lbl12.Size = new System.Drawing.Size(0, 20);
            this.lbl12.TabIndex = 45;
            // 
            // lbl13
            // 
            this.lbl13.AutoSize = true;
            this.lbl13.BackColor = System.Drawing.Color.Transparent;
            this.lbl13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl13.ForeColor = System.Drawing.Color.Black;
            this.lbl13.Location = new System.Drawing.Point(137, 380);
            this.lbl13.Name = "lbl13";
            this.lbl13.Size = new System.Drawing.Size(0, 20);
            this.lbl13.TabIndex = 46;
            // 
            // Notlarım
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(5F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.back;
            this.ClientSize = new System.Drawing.Size(760, 457);
            this.Controls.Add(this.lbl13);
            this.Controls.Add(this.lbl12);
            this.Controls.Add(this.lbl11);
            this.Controls.Add(this.lbl10);
            this.Controls.Add(this.lbl9);
            this.Controls.Add(this.lbl8);
            this.Controls.Add(this.bttn14);
            this.Controls.Add(this.bttn13);
            this.Controls.Add(this.bttn12);
            this.Controls.Add(this.bttn11);
            this.Controls.Add(this.bttn10);
            this.Controls.Add(this.bttn9);
            this.Controls.Add(this.lbl7);
            this.Controls.Add(this.lbl6);
            this.Controls.Add(this.lbl5);
            this.Controls.Add(this.lbl4);
            this.Controls.Add(this.lbl3);
            this.Controls.Add(this.bttn8);
            this.Controls.Add(this.bttn7);
            this.Controls.Add(this.bttn6);
            this.Controls.Add(this.bttn5);
            this.Controls.Add(this.bttn4);
            this.Controls.Add(this.lbl2);
            this.Controls.Add(this.bttn3);
            this.Controls.Add(this.lbl1);
            this.Controls.Add(this.bttn2);
            this.Controls.Add(this.bttn1);
            this.Font = new System.Drawing.Font("Mistral", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "Notlarım";
            this.Text = "Notlarım";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void btn3_Click(object sender, System.EventArgs e)
        {
            throw new System.NotImplementedException();
        }

        private void btn2_Click(object sender, System.EventArgs e)
        {
            throw new System.NotImplementedException();
        }

        #endregion

        private System.Windows.Forms.Button bttn1;
        private System.Windows.Forms.Button bttn2;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Button bttn3;
        private System.Windows.Forms.Label lbl2;
        private System.Windows.Forms.Button bttn4;
        private System.Windows.Forms.Button bttn5;
        private System.Windows.Forms.Button bttn6;
        private System.Windows.Forms.Button bttn7;
        private System.Windows.Forms.Button bttn8;
        private System.Windows.Forms.Label lbl3;
        private System.Windows.Forms.Label lbl4;
        private System.Windows.Forms.Label lbl5;
        private System.Windows.Forms.Label lbl6;
        private System.Windows.Forms.Label lbl7;
        private System.Windows.Forms.Button bttn9;
        private System.Windows.Forms.Button bttn10;
        private System.Windows.Forms.Button bttn11;
        private System.Windows.Forms.Button bttn12;
        private System.Windows.Forms.Button bttn13;
        private System.Windows.Forms.Button bttn14;
        private System.Windows.Forms.Label lbl8;
        private System.Windows.Forms.Label lbl9;
        private System.Windows.Forms.Label lbl10;
        private System.Windows.Forms.Label lbl11;
        private System.Windows.Forms.Label lbl12;
        private System.Windows.Forms.Label lbl13;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CocukEgitimSeti
{
    public partial class Akrabalar : Form
    {
        public Akrabalar()
        {
            InitializeComponent();
        }

       
        private void button2_Click(object sender, EventArgs e)
        {
            lbl1.Text = "FATHER";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            lbl2.Text = "MOTHER";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            lbl3.Text = "BROTHER";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            lbl4.Text = "SİSTER";
        }

        private void button11_Click(object sender, EventArgs e)
        {
            lbl5.Text = "SON";
        }

        private void button12_Click(object sender, EventArgs e)
        {
            lbl6.Text = "DAUGHTER";
        }

        private void button9_Click(object sender, EventArgs e)
        {
            lbl7.Text = "GRANDDAD";
        }

        private void button10_Click(object sender, EventArgs e)
        {
            lbl8.Text = "GRANNY";
        }

        private void button5_Click(object sender, EventArgs e)
        {
            lbl9.Text = "NEPHEW";
        }

        private void button6_Click(object sender, EventArgs e)
        {
            lbl10.Text = "NİECE";
        }

        private void button7_Click(object sender, EventArgs e)
        {
            lbl11.Text = "AUNT";
        }

        private void button8_Click(object sender, EventArgs e)
        {
            form_index index = new form_index();
            index.Show();
            this.Hide();
        }

        private void button13_Click(object sender, EventArgs e)
        {
            Ogrenelim ogr = new Ogrenelim();
            ogr.Show();
            this.Hide();
        }
    }
}

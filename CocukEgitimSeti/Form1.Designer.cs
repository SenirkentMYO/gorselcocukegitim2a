﻿namespace CocukEgitimSeti
{
    partial class form_index
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(form_index));
            this.btn_ders = new System.Windows.Forms.Button();
            this.btn_msl = new System.Windows.Forms.Button();
            this.btn_tlf = new System.Windows.Forms.Button();
            this.btn_sar = new System.Windows.Forms.Button();
            this.btn_not = new System.Windows.Forms.Button();
            this.btn_oyn = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_ders
            // 
            this.btn_ders.BackColor = System.Drawing.Color.Transparent;
            this.btn_ders.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.ders;
            this.btn_ders.Location = new System.Drawing.Point(47, 450);
            this.btn_ders.Name = "btn_ders";
            this.btn_ders.Size = new System.Drawing.Size(152, 31);
            this.btn_ders.TabIndex = 0;
            this.btn_ders.UseVisualStyleBackColor = false;
            this.btn_ders.Click += new System.EventHandler(this.btn_ders_Click);
            // 
            // btn_msl
            // 
            this.btn_msl.BackColor = System.Drawing.Color.Transparent;
            this.btn_msl.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.mas;
            this.btn_msl.Location = new System.Drawing.Point(47, 296);
            this.btn_msl.Name = "btn_msl";
            this.btn_msl.Size = new System.Drawing.Size(152, 31);
            this.btn_msl.TabIndex = 1;
            this.btn_msl.UseVisualStyleBackColor = false;
            this.btn_msl.Click += new System.EventHandler(this.btn_msl_Click);
            // 
            // btn_tlf
            // 
            this.btn_tlf.BackColor = System.Drawing.Color.Transparent;
            this.btn_tlf.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.ogr;
            this.btn_tlf.Location = new System.Drawing.Point(76, 398);
            this.btn_tlf.Name = "btn_tlf";
            this.btn_tlf.Size = new System.Drawing.Size(152, 31);
            this.btn_tlf.TabIndex = 2;
            this.btn_tlf.UseVisualStyleBackColor = false;
            this.btn_tlf.Click += new System.EventHandler(this.btn_tlf_Click);
            // 
            // btn_sar
            // 
            this.btn_sar.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.sar2;
            this.btn_sar.Location = new System.Drawing.Point(76, 346);
            this.btn_sar.Name = "btn_sar";
            this.btn_sar.Size = new System.Drawing.Size(152, 31);
            this.btn_sar.TabIndex = 3;
            this.btn_sar.UseVisualStyleBackColor = true;
            this.btn_sar.Click += new System.EventHandler(this.btn_sar_Click);
            // 
            // btn_not
            // 
            this.btn_not.BackColor = System.Drawing.Color.Transparent;
            this.btn_not.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.not;
            this.btn_not.Location = new System.Drawing.Point(12, 246);
            this.btn_not.Name = "btn_not";
            this.btn_not.Size = new System.Drawing.Size(152, 31);
            this.btn_not.TabIndex = 5;
            this.btn_not.UseVisualStyleBackColor = false;
            this.btn_not.Click += new System.EventHandler(this.btn_not_Click);
            // 
            // btn_oyn
            // 
            this.btn_oyn.BackColor = System.Drawing.Color.Transparent;
            this.btn_oyn.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.oyun;
            this.btn_oyn.Location = new System.Drawing.Point(12, 498);
            this.btn_oyn.Name = "btn_oyn";
            this.btn_oyn.Size = new System.Drawing.Size(152, 31);
            this.btn_oyn.TabIndex = 6;
            this.btn_oyn.UseVisualStyleBackColor = false;
            this.btn_oyn.Click += new System.EventHandler(this.btn_oyn_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.banner3;
            this.pictureBox1.Location = new System.Drawing.Point(34, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1053, 161);
            this.pictureBox1.TabIndex = 37;
            this.pictureBox1.TabStop = false;
            // 
            // form_index
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1127, 673);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btn_oyn);
            this.Controls.Add(this.btn_not);
            this.Controls.Add(this.btn_sar);
            this.Controls.Add(this.btn_tlf);
            this.Controls.Add(this.btn_msl);
            this.Controls.Add(this.btn_ders);
            this.Name = "form_index";
            this.Text = "Dil Eğitimi";
            this.Load += new System.EventHandler(this.form_index_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_ders;
        private System.Windows.Forms.Button btn_msl;
        private System.Windows.Forms.Button btn_tlf;
        private System.Windows.Forms.Button btn_sar;
        private System.Windows.Forms.Button btn_not;
        private System.Windows.Forms.Button btn_oyn;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}


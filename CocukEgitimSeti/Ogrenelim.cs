﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CocukEgitimSeti
{
    public partial class Ogrenelim : Form
    {
        public Ogrenelim()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            form_index index = new form_index();
            index.Show();
            this.Hide();
        }

        private void btn_akrb_Click(object sender, EventArgs e)
        {
            Akrabalar frm_akrb = new Akrabalar();
            frm_akrb.Show();
            this.Hide();
        }

        private void btn_mslk_Click(object sender, EventArgs e)
        {
            Meslekler frm_mslk = new Meslekler();
            frm_mslk.Show();
            this.Hide();
        }

        private void btn_ulke_Click(object sender, EventArgs e)
        {
            Ulkeler frm_ulke = new Ulkeler();
            frm_ulke.Show();
            this.Hide();
        }

        private void btn_harf_Click(object sender, EventArgs e)
        {
            Harfler frm_harf = new Harfler();
            frm_harf.Show();
            this.Hide();
        }

        private void btn_sayi_Click(object sender, EventArgs e)
        {
            Sayılar frm_sayi = new Sayılar();
            frm_sayi.Show();
            this.Hide();
        }

        private void btn_esya_Click(object sender, EventArgs e)
        {
            Esyalar frm_esya = new Esyalar();
            frm_esya.Show();
            this.Hide();
        }

        private void btn_vct_Click(object sender, EventArgs e)
        {
            Vucut frm_vct = new Vucut();
            frm_vct.Show();
            this.Hide();
        }

        private void btn_renk_Click(object sender, EventArgs e)
        {
            Renkler frm_renk = new Renkler();
            frm_renk.Show();
            this.Hide();
        }

        private void btn_skil_Click(object sender, EventArgs e)
        {
            Sekiller frm_skil = new Sekiller();
            frm_skil.Show();
            this.Hide();
        }

        private void btn_hyvn_Click(object sender, EventArgs e)
        {
            Hayvanlar frm_hyvn = new Hayvanlar();
            frm_hyvn.Show();
            this.Hide();
        }

        private void btn_myv_Click(object sender, EventArgs e)
        {
            Meyveler frm_myv = new Meyveler();
            frm_myv.Show();
            this.Hide();
        }

        private void btn_sbz_Click(object sender, EventArgs e)
        {
            Sebzeler frm_sbz = new Sebzeler();
            frm_sbz.Show();
            this.Hide();
        }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CocukEgitimSeti
{
    public partial class Notlarım : Form
    {
        public Notlarım()
        {
            InitializeComponent();
        }

        private void bttn1_Click(object sender, EventArgs e)
        {
            form_index index = new form_index();
            index.Show();
            this.Hide();
        }


        private void bttn2_Click(object sender, EventArgs e)
        {
            lbl1.Text = "YES/yes";
        }

        private void bttn3_Click(object sender, EventArgs e)
        {
            lbl2.Text = "NO/nou";
        }

        private void bttn4_Click(object sender, EventArgs e)
        {
            lbl3.Text = "Excuse me/ik'skyu:z mi";
        }

        private void bttn5_Click(object sender, EventArgs e)
        {
            lbl4.Text = "I am sorry/ay em'sori";
        }

        private void bttn6_Click(object sender, EventArgs e)
        {
            lbl5.Text = "Please/pli:z";
        }

        private void bttn7_Click(object sender, EventArgs e)
        {
            lbl6.Text = "All right/o:l rayt";
        }

        private void bttn8_Click(object sender, EventArgs e)
        {
            lbl7.Text = "Okay!/ou'key";
        }

        private void bttn9_Click(object sender, EventArgs e)
        {
            lbl8.Text = "Good-bye/'gud'bay";
        }

        private void bttn10_Click(object sender, EventArgs e)
        {
            lbl9.Text = "Help!/help";
        }

        private void bttn11_Click(object sender, EventArgs e)
        {
            lbl10.Text = "Mr. .../'mistı";
        }

        private void bttn12_Click(object sender, EventArgs e)
        {
            lbl11.Text = "Sir/sö:";
        }

        private void bttn13_Click(object sender, EventArgs e)
        {
            lbl12.Text = "Mrs. .../'misiz";
        }

        private void bttn14_Click(object sender, EventArgs e)
        {
            lbl13.Text = "Miss .../mis";
        }

       

        
    }
}



﻿namespace CocukEgitimSeti
{
    partial class Hayvanlar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAri = new System.Windows.Forms.Button();
            this.btnAslan = new System.Windows.Forms.Button();
            this.btnAt = new System.Windows.Forms.Button();
            this.btnAyi = new System.Windows.Forms.Button();
            this.btnAkrep = new System.Windows.Forms.Button();
            this.btnBalik = new System.Windows.Forms.Button();
            this.btnBalina = new System.Windows.Forms.Button();
            this.btnDeve = new System.Windows.Forms.Button();
            this.btnEsek = new System.Windows.Forms.Button();
            this.btnFare = new System.Windows.Forms.Button();
            this.btnFil = new System.Windows.Forms.Button();
            this.btnGeyik = new System.Windows.Forms.Button();
            this.btnGoril = new System.Windows.Forms.Button();
            this.btnHoroz = new System.Windows.Forms.Button();
            this.btnKaplumbaga = new System.Windows.Forms.Button();
            this.btnKarinca = new System.Windows.Forms.Button();
            this.btnKedi = new System.Windows.Forms.Button();
            this.btnKoyun = new System.Windows.Forms.Button();
            this.btnKopek = new System.Windows.Forms.Button();
            this.btnKurbaga = new System.Windows.Forms.Button();
            this.btnKutup = new System.Windows.Forms.Button();
            this.btnKelebek = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.btnLeylek = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.btnMaymun = new System.Windows.Forms.Button();
            this.btnSalyangoz = new System.Windows.Forms.Button();
            this.btnSerce = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.btnSincap = new System.Windows.Forms.Button();
            this.btnTavsan = new System.Windows.Forms.Button();
            this.btnTavuk = new System.Windows.Forms.Button();
            this.btnTavusKus = new System.Windows.Forms.Button();
            this.btnTilki = new System.Windows.Forms.Button();
            this.btnTimsah = new System.Windows.Forms.Button();
            this.btnUskumru = new System.Windows.Forms.Button();
            this.btnYarasa = new System.Windows.Forms.Button();
            this.btnYilan = new System.Windows.Forms.Button();
            this.btnYilanBalik = new System.Windows.Forms.Button();
            this.btnYengec = new System.Windows.Forms.Button();
            this.btnZebra = new System.Windows.Forms.Button();
            this.btnZurafa = new System.Windows.Forms.Button();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.btnAnasayfa = new System.Windows.Forms.Button();
            this.btnOgren = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnAri
            // 
            this.btnAri.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnAri.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnAri.Location = new System.Drawing.Point(0, 0);
            this.btnAri.Name = "btnAri";
            this.btnAri.Size = new System.Drawing.Size(75, 23);
            this.btnAri.TabIndex = 0;
            this.btnAri.Text = "ARI";
            this.btnAri.UseVisualStyleBackColor = false;
            this.btnAri.Click += new System.EventHandler(this.btnAri_Click);
            // 
            // btnAslan
            // 
            this.btnAslan.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnAslan.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnAslan.Location = new System.Drawing.Point(0, 60);
            this.btnAslan.Name = "btnAslan";
            this.btnAslan.Size = new System.Drawing.Size(75, 23);
            this.btnAslan.TabIndex = 1;
            this.btnAslan.Text = "ASLAN";
            this.btnAslan.UseVisualStyleBackColor = false;
            this.btnAslan.Click += new System.EventHandler(this.btnAslan_Click);
            // 
            // btnAt
            // 
            this.btnAt.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnAt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnAt.Location = new System.Drawing.Point(0, 90);
            this.btnAt.Name = "btnAt";
            this.btnAt.Size = new System.Drawing.Size(75, 23);
            this.btnAt.TabIndex = 2;
            this.btnAt.Text = "AT";
            this.btnAt.UseVisualStyleBackColor = false;
            this.btnAt.Click += new System.EventHandler(this.btnAt_Click);
            // 
            // btnAyi
            // 
            this.btnAyi.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnAyi.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnAyi.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnAyi.Location = new System.Drawing.Point(0, 120);
            this.btnAyi.Name = "btnAyi";
            this.btnAyi.Size = new System.Drawing.Size(75, 23);
            this.btnAyi.TabIndex = 3;
            this.btnAyi.Text = "AYI";
            this.btnAyi.UseVisualStyleBackColor = false;
            this.btnAyi.Click += new System.EventHandler(this.btnAyi_Click);
            // 
            // btnAkrep
            // 
            this.btnAkrep.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnAkrep.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnAkrep.Location = new System.Drawing.Point(0, 30);
            this.btnAkrep.Name = "btnAkrep";
            this.btnAkrep.Size = new System.Drawing.Size(75, 23);
            this.btnAkrep.TabIndex = 4;
            this.btnAkrep.Text = "AKREP";
            this.btnAkrep.UseVisualStyleBackColor = false;
            this.btnAkrep.Click += new System.EventHandler(this.btnAkrep_Click);
            // 
            // btnBalik
            // 
            this.btnBalik.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnBalik.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnBalik.Location = new System.Drawing.Point(0, 150);
            this.btnBalik.Name = "btnBalik";
            this.btnBalik.Size = new System.Drawing.Size(75, 23);
            this.btnBalik.TabIndex = 5;
            this.btnBalik.Text = "BALIK";
            this.btnBalik.UseVisualStyleBackColor = false;
            this.btnBalik.Click += new System.EventHandler(this.btnBalik_Click);
            // 
            // btnBalina
            // 
            this.btnBalina.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnBalina.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnBalina.Location = new System.Drawing.Point(0, 180);
            this.btnBalina.Name = "btnBalina";
            this.btnBalina.Size = new System.Drawing.Size(75, 23);
            this.btnBalina.TabIndex = 6;
            this.btnBalina.Text = "BALİNA";
            this.btnBalina.UseVisualStyleBackColor = false;
            this.btnBalina.Click += new System.EventHandler(this.btnBalina_Click);
            // 
            // btnDeve
            // 
            this.btnDeve.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnDeve.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnDeve.Location = new System.Drawing.Point(0, 210);
            this.btnDeve.Name = "btnDeve";
            this.btnDeve.Size = new System.Drawing.Size(75, 23);
            this.btnDeve.TabIndex = 7;
            this.btnDeve.Text = "DEVE";
            this.btnDeve.UseVisualStyleBackColor = false;
            this.btnDeve.Click += new System.EventHandler(this.btnDeve_Click);
            // 
            // btnEsek
            // 
            this.btnEsek.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnEsek.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnEsek.Location = new System.Drawing.Point(0, 240);
            this.btnEsek.Name = "btnEsek";
            this.btnEsek.Size = new System.Drawing.Size(75, 23);
            this.btnEsek.TabIndex = 8;
            this.btnEsek.Text = "EŞEK";
            this.btnEsek.UseVisualStyleBackColor = false;
            this.btnEsek.Click += new System.EventHandler(this.btnEsek_Click);
            // 
            // btnFare
            // 
            this.btnFare.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnFare.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnFare.Location = new System.Drawing.Point(0, 270);
            this.btnFare.Name = "btnFare";
            this.btnFare.Size = new System.Drawing.Size(75, 23);
            this.btnFare.TabIndex = 9;
            this.btnFare.Text = "FARE";
            this.btnFare.UseVisualStyleBackColor = false;
            this.btnFare.Click += new System.EventHandler(this.btnFare_Click);
            // 
            // btnFil
            // 
            this.btnFil.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnFil.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnFil.Location = new System.Drawing.Point(0, 300);
            this.btnFil.Name = "btnFil";
            this.btnFil.Size = new System.Drawing.Size(75, 23);
            this.btnFil.TabIndex = 10;
            this.btnFil.Text = "FİL";
            this.btnFil.UseVisualStyleBackColor = false;
            this.btnFil.Click += new System.EventHandler(this.btnFil_Click);
            // 
            // btnGeyik
            // 
            this.btnGeyik.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnGeyik.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnGeyik.Location = new System.Drawing.Point(0, 330);
            this.btnGeyik.Name = "btnGeyik";
            this.btnGeyik.Size = new System.Drawing.Size(75, 23);
            this.btnGeyik.TabIndex = 11;
            this.btnGeyik.Text = "GEYİK";
            this.btnGeyik.UseVisualStyleBackColor = false;
            this.btnGeyik.Click += new System.EventHandler(this.btnGeyik_Click);
            // 
            // btnGoril
            // 
            this.btnGoril.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnGoril.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnGoril.Location = new System.Drawing.Point(0, 360);
            this.btnGoril.Name = "btnGoril";
            this.btnGoril.Size = new System.Drawing.Size(75, 23);
            this.btnGoril.TabIndex = 12;
            this.btnGoril.Text = "GORİL";
            this.btnGoril.UseVisualStyleBackColor = false;
            this.btnGoril.Click += new System.EventHandler(this.btnGoril_Click);
            // 
            // btnHoroz
            // 
            this.btnHoroz.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnHoroz.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnHoroz.Location = new System.Drawing.Point(200, 0);
            this.btnHoroz.Name = "btnHoroz";
            this.btnHoroz.Size = new System.Drawing.Size(75, 23);
            this.btnHoroz.TabIndex = 13;
            this.btnHoroz.Text = "HOROZ";
            this.btnHoroz.UseVisualStyleBackColor = false;
            this.btnHoroz.Click += new System.EventHandler(this.btnHoroz_Click);
            // 
            // btnKaplumbaga
            // 
            this.btnKaplumbaga.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnKaplumbaga.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnKaplumbaga.Location = new System.Drawing.Point(200, 30);
            this.btnKaplumbaga.Name = "btnKaplumbaga";
            this.btnKaplumbaga.Size = new System.Drawing.Size(104, 23);
            this.btnKaplumbaga.TabIndex = 14;
            this.btnKaplumbaga.Text = "KAPLUMBAĞA";
            this.btnKaplumbaga.UseVisualStyleBackColor = false;
            this.btnKaplumbaga.Click += new System.EventHandler(this.btnKaplumbaga_Click);
            // 
            // btnKarinca
            // 
            this.btnKarinca.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnKarinca.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnKarinca.Location = new System.Drawing.Point(200, 60);
            this.btnKarinca.Name = "btnKarinca";
            this.btnKarinca.Size = new System.Drawing.Size(75, 23);
            this.btnKarinca.TabIndex = 15;
            this.btnKarinca.Text = "KARINCA";
            this.btnKarinca.UseVisualStyleBackColor = false;
            this.btnKarinca.Click += new System.EventHandler(this.btnKarinca_Click);
            // 
            // btnKedi
            // 
            this.btnKedi.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnKedi.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnKedi.Location = new System.Drawing.Point(200, 90);
            this.btnKedi.Name = "btnKedi";
            this.btnKedi.Size = new System.Drawing.Size(75, 23);
            this.btnKedi.TabIndex = 16;
            this.btnKedi.Text = "KEDİ";
            this.btnKedi.UseVisualStyleBackColor = false;
            this.btnKedi.Click += new System.EventHandler(this.btnKedi_Click);
            // 
            // btnKoyun
            // 
            this.btnKoyun.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnKoyun.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnKoyun.Location = new System.Drawing.Point(200, 150);
            this.btnKoyun.Name = "btnKoyun";
            this.btnKoyun.Size = new System.Drawing.Size(75, 23);
            this.btnKoyun.TabIndex = 17;
            this.btnKoyun.Text = "KOYUN";
            this.btnKoyun.UseVisualStyleBackColor = false;
            this.btnKoyun.Click += new System.EventHandler(this.btnKoyun_Click);
            // 
            // btnKopek
            // 
            this.btnKopek.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnKopek.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnKopek.Location = new System.Drawing.Point(200, 180);
            this.btnKopek.Name = "btnKopek";
            this.btnKopek.Size = new System.Drawing.Size(75, 23);
            this.btnKopek.TabIndex = 18;
            this.btnKopek.Text = "KÖPEK";
            this.btnKopek.UseVisualStyleBackColor = false;
            this.btnKopek.Click += new System.EventHandler(this.btnKopek_Click);
            // 
            // btnKurbaga
            // 
            this.btnKurbaga.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnKurbaga.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnKurbaga.Location = new System.Drawing.Point(200, 210);
            this.btnKurbaga.Name = "btnKurbaga";
            this.btnKurbaga.Size = new System.Drawing.Size(75, 23);
            this.btnKurbaga.TabIndex = 19;
            this.btnKurbaga.Text = "KURBAĞA";
            this.btnKurbaga.UseVisualStyleBackColor = false;
            this.btnKurbaga.Click += new System.EventHandler(this.btnKurbaga_Click);
            // 
            // btnKutup
            // 
            this.btnKutup.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnKutup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnKutup.Location = new System.Drawing.Point(200, 240);
            this.btnKutup.Name = "btnKutup";
            this.btnKutup.Size = new System.Drawing.Size(104, 23);
            this.btnKutup.TabIndex = 20;
            this.btnKutup.Text = "KUTUP AYISI";
            this.btnKutup.UseVisualStyleBackColor = false;
            this.btnKutup.Click += new System.EventHandler(this.btnKutup_Click);
            // 
            // btnKelebek
            // 
            this.btnKelebek.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnKelebek.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnKelebek.Location = new System.Drawing.Point(200, 120);
            this.btnKelebek.Name = "btnKelebek";
            this.btnKelebek.Size = new System.Drawing.Size(75, 23);
            this.btnKelebek.TabIndex = 21;
            this.btnKelebek.Text = "KELEBEK";
            this.btnKelebek.UseVisualStyleBackColor = false;
            this.btnKelebek.Click += new System.EventHandler(this.btnKelebek_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Pink;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(110, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 22;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Pink;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.Location = new System.Drawing.Point(110, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 23;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Pink;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label3.Location = new System.Drawing.Point(110, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 24;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Pink;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label4.Location = new System.Drawing.Point(110, 95);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 13);
            this.label4.TabIndex = 25;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Pink;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label5.Location = new System.Drawing.Point(110, 125);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 13);
            this.label5.TabIndex = 26;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Pink;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label6.Location = new System.Drawing.Point(110, 155);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 13);
            this.label6.TabIndex = 27;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Pink;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label7.Location = new System.Drawing.Point(110, 185);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 13);
            this.label7.TabIndex = 28;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Pink;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label8.Location = new System.Drawing.Point(110, 215);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(0, 13);
            this.label8.TabIndex = 29;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Pink;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label9.Location = new System.Drawing.Point(110, 245);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(0, 13);
            this.label9.TabIndex = 30;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Pink;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label10.Location = new System.Drawing.Point(110, 275);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(0, 13);
            this.label10.TabIndex = 31;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Pink;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label11.Location = new System.Drawing.Point(110, 305);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(0, 13);
            this.label11.TabIndex = 32;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Pink;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label12.Location = new System.Drawing.Point(110, 335);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(0, 13);
            this.label12.TabIndex = 33;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Pink;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label13.Location = new System.Drawing.Point(110, 365);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(0, 13);
            this.label13.TabIndex = 34;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Pink;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label14.Location = new System.Drawing.Point(310, 5);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(0, 13);
            this.label14.TabIndex = 35;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Pink;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label15.Location = new System.Drawing.Point(310, 35);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(0, 13);
            this.label15.TabIndex = 36;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Pink;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label16.Location = new System.Drawing.Point(310, 65);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(0, 13);
            this.label16.TabIndex = 37;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Pink;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label17.Location = new System.Drawing.Point(310, 95);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(0, 13);
            this.label17.TabIndex = 38;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Pink;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label18.Location = new System.Drawing.Point(310, 125);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(0, 13);
            this.label18.TabIndex = 39;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Pink;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label19.Location = new System.Drawing.Point(310, 155);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(0, 13);
            this.label19.TabIndex = 40;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Pink;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label20.Location = new System.Drawing.Point(310, 185);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(0, 13);
            this.label20.TabIndex = 41;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Pink;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label21.Location = new System.Drawing.Point(310, 215);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(0, 13);
            this.label21.TabIndex = 42;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Pink;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label22.Location = new System.Drawing.Point(310, 245);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(0, 13);
            this.label22.TabIndex = 43;
            // 
            // btnLeylek
            // 
            this.btnLeylek.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnLeylek.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnLeylek.Location = new System.Drawing.Point(200, 270);
            this.btnLeylek.Name = "btnLeylek";
            this.btnLeylek.Size = new System.Drawing.Size(75, 23);
            this.btnLeylek.TabIndex = 44;
            this.btnLeylek.Text = "LEYLEK";
            this.btnLeylek.UseVisualStyleBackColor = false;
            this.btnLeylek.Click += new System.EventHandler(this.btnLeylek_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Pink;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label23.Location = new System.Drawing.Point(310, 275);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(0, 13);
            this.label23.TabIndex = 45;
            // 
            // btnMaymun
            // 
            this.btnMaymun.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnMaymun.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnMaymun.Location = new System.Drawing.Point(200, 300);
            this.btnMaymun.Name = "btnMaymun";
            this.btnMaymun.Size = new System.Drawing.Size(75, 23);
            this.btnMaymun.TabIndex = 46;
            this.btnMaymun.Text = "MAYMUN";
            this.btnMaymun.UseVisualStyleBackColor = false;
            this.btnMaymun.Click += new System.EventHandler(this.btnMaymun_Click);
            // 
            // btnSalyangoz
            // 
            this.btnSalyangoz.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnSalyangoz.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnSalyangoz.Location = new System.Drawing.Point(200, 330);
            this.btnSalyangoz.Name = "btnSalyangoz";
            this.btnSalyangoz.Size = new System.Drawing.Size(92, 23);
            this.btnSalyangoz.TabIndex = 47;
            this.btnSalyangoz.Text = "SALYANGOZ";
            this.btnSalyangoz.UseVisualStyleBackColor = false;
            this.btnSalyangoz.Click += new System.EventHandler(this.btnSalyangoz_Click);
            // 
            // btnSerce
            // 
            this.btnSerce.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnSerce.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnSerce.Location = new System.Drawing.Point(200, 360);
            this.btnSerce.Name = "btnSerce";
            this.btnSerce.Size = new System.Drawing.Size(75, 23);
            this.btnSerce.TabIndex = 48;
            this.btnSerce.Text = "SERÇE";
            this.btnSerce.UseVisualStyleBackColor = false;
            this.btnSerce.Click += new System.EventHandler(this.btnSerce_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Pink;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label24.Location = new System.Drawing.Point(310, 305);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(0, 13);
            this.label24.TabIndex = 49;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Pink;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label25.Location = new System.Drawing.Point(310, 335);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(0, 13);
            this.label25.TabIndex = 50;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Pink;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label26.Location = new System.Drawing.Point(310, 365);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(0, 13);
            this.label26.TabIndex = 51;
            // 
            // btnSincap
            // 
            this.btnSincap.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnSincap.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnSincap.Location = new System.Drawing.Point(400, 0);
            this.btnSincap.Name = "btnSincap";
            this.btnSincap.Size = new System.Drawing.Size(75, 23);
            this.btnSincap.TabIndex = 52;
            this.btnSincap.Text = "SİNCAP";
            this.btnSincap.UseVisualStyleBackColor = false;
            this.btnSincap.Click += new System.EventHandler(this.btnSincap_Click);
            // 
            // btnTavsan
            // 
            this.btnTavsan.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnTavsan.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnTavsan.Location = new System.Drawing.Point(400, 30);
            this.btnTavsan.Name = "btnTavsan";
            this.btnTavsan.Size = new System.Drawing.Size(75, 23);
            this.btnTavsan.TabIndex = 53;
            this.btnTavsan.Text = "TAVŞAN";
            this.btnTavsan.UseVisualStyleBackColor = false;
            this.btnTavsan.Click += new System.EventHandler(this.btnTavsan_Click);
            // 
            // btnTavuk
            // 
            this.btnTavuk.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnTavuk.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnTavuk.Location = new System.Drawing.Point(400, 60);
            this.btnTavuk.Name = "btnTavuk";
            this.btnTavuk.Size = new System.Drawing.Size(75, 23);
            this.btnTavuk.TabIndex = 54;
            this.btnTavuk.Text = "TAVUK";
            this.btnTavuk.UseVisualStyleBackColor = false;
            this.btnTavuk.Click += new System.EventHandler(this.btnTavuk_Click);
            // 
            // btnTavusKus
            // 
            this.btnTavusKus.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnTavusKus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnTavusKus.Location = new System.Drawing.Point(400, 90);
            this.btnTavusKus.Name = "btnTavusKus";
            this.btnTavusKus.Size = new System.Drawing.Size(96, 23);
            this.btnTavusKus.TabIndex = 55;
            this.btnTavusKus.Text = "TAVUS KUŞU";
            this.btnTavusKus.UseVisualStyleBackColor = false;
            this.btnTavusKus.Click += new System.EventHandler(this.btnTavusKus_Click);
            // 
            // btnTilki
            // 
            this.btnTilki.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnTilki.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnTilki.Location = new System.Drawing.Point(400, 120);
            this.btnTilki.Name = "btnTilki";
            this.btnTilki.Size = new System.Drawing.Size(75, 23);
            this.btnTilki.TabIndex = 56;
            this.btnTilki.Text = "TİLKİ";
            this.btnTilki.UseVisualStyleBackColor = false;
            this.btnTilki.Click += new System.EventHandler(this.btnTilki_Click);
            // 
            // btnTimsah
            // 
            this.btnTimsah.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnTimsah.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnTimsah.Location = new System.Drawing.Point(400, 150);
            this.btnTimsah.Name = "btnTimsah";
            this.btnTimsah.Size = new System.Drawing.Size(75, 23);
            this.btnTimsah.TabIndex = 57;
            this.btnTimsah.Text = "TİMSAH";
            this.btnTimsah.UseVisualStyleBackColor = false;
            this.btnTimsah.Click += new System.EventHandler(this.btnTimsah_Click);
            // 
            // btnUskumru
            // 
            this.btnUskumru.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnUskumru.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnUskumru.Location = new System.Drawing.Point(400, 180);
            this.btnUskumru.Name = "btnUskumru";
            this.btnUskumru.Size = new System.Drawing.Size(87, 23);
            this.btnUskumru.TabIndex = 58;
            this.btnUskumru.Text = "USKUMRU";
            this.btnUskumru.UseVisualStyleBackColor = false;
            this.btnUskumru.Click += new System.EventHandler(this.btnUskumru_Click);
            // 
            // btnYarasa
            // 
            this.btnYarasa.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnYarasa.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnYarasa.Location = new System.Drawing.Point(400, 210);
            this.btnYarasa.Name = "btnYarasa";
            this.btnYarasa.Size = new System.Drawing.Size(75, 23);
            this.btnYarasa.TabIndex = 59;
            this.btnYarasa.Text = "YARASA";
            this.btnYarasa.UseVisualStyleBackColor = false;
            this.btnYarasa.Click += new System.EventHandler(this.btnYarasa_Click);
            // 
            // btnYilan
            // 
            this.btnYilan.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnYilan.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnYilan.Location = new System.Drawing.Point(400, 270);
            this.btnYilan.Name = "btnYilan";
            this.btnYilan.Size = new System.Drawing.Size(75, 23);
            this.btnYilan.TabIndex = 60;
            this.btnYilan.Text = "YILAN";
            this.btnYilan.UseVisualStyleBackColor = false;
            this.btnYilan.Click += new System.EventHandler(this.btnYilan_Click);
            // 
            // btnYilanBalik
            // 
            this.btnYilanBalik.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnYilanBalik.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnYilanBalik.Location = new System.Drawing.Point(400, 300);
            this.btnYilanBalik.Name = "btnYilanBalik";
            this.btnYilanBalik.Size = new System.Drawing.Size(96, 23);
            this.btnYilanBalik.TabIndex = 61;
            this.btnYilanBalik.Text = "YILAN BALIĞI";
            this.btnYilanBalik.UseVisualStyleBackColor = false;
            this.btnYilanBalik.Click += new System.EventHandler(this.btnYilanBalik_Click);
            // 
            // btnYengec
            // 
            this.btnYengec.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnYengec.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnYengec.Location = new System.Drawing.Point(400, 240);
            this.btnYengec.Name = "btnYengec";
            this.btnYengec.Size = new System.Drawing.Size(75, 23);
            this.btnYengec.TabIndex = 62;
            this.btnYengec.Text = "YENGEÇ";
            this.btnYengec.UseVisualStyleBackColor = false;
            this.btnYengec.Click += new System.EventHandler(this.btnYengec_Click);
            // 
            // btnZebra
            // 
            this.btnZebra.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnZebra.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnZebra.Location = new System.Drawing.Point(400, 330);
            this.btnZebra.Name = "btnZebra";
            this.btnZebra.Size = new System.Drawing.Size(75, 23);
            this.btnZebra.TabIndex = 63;
            this.btnZebra.Text = "ZEBRA";
            this.btnZebra.UseVisualStyleBackColor = false;
            this.btnZebra.Click += new System.EventHandler(this.btnZebra_Click);
            // 
            // btnZurafa
            // 
            this.btnZurafa.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnZurafa.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnZurafa.Location = new System.Drawing.Point(400, 360);
            this.btnZurafa.Name = "btnZurafa";
            this.btnZurafa.Size = new System.Drawing.Size(75, 23);
            this.btnZurafa.TabIndex = 64;
            this.btnZurafa.Text = "ZÜRAFA";
            this.btnZurafa.UseVisualStyleBackColor = false;
            this.btnZurafa.Click += new System.EventHandler(this.btnZurafa_Click);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Pink;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label27.Location = new System.Drawing.Point(510, 5);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(0, 13);
            this.label27.TabIndex = 65;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Pink;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label28.Location = new System.Drawing.Point(510, 35);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(0, 13);
            this.label28.TabIndex = 66;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Pink;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label29.Location = new System.Drawing.Point(510, 65);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(0, 13);
            this.label29.TabIndex = 67;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Pink;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label30.Location = new System.Drawing.Point(510, 95);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(0, 13);
            this.label30.TabIndex = 68;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Pink;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label31.Location = new System.Drawing.Point(510, 125);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(0, 13);
            this.label31.TabIndex = 69;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Pink;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label32.Location = new System.Drawing.Point(510, 155);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(0, 13);
            this.label32.TabIndex = 70;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Pink;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label33.Location = new System.Drawing.Point(510, 185);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(0, 13);
            this.label33.TabIndex = 71;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Pink;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label34.Location = new System.Drawing.Point(510, 215);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(0, 13);
            this.label34.TabIndex = 72;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Pink;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label35.Location = new System.Drawing.Point(510, 245);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(0, 13);
            this.label35.TabIndex = 73;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.Color.Pink;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label36.Location = new System.Drawing.Point(510, 275);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(0, 13);
            this.label36.TabIndex = 74;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.BackColor = System.Drawing.Color.Pink;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label37.Location = new System.Drawing.Point(510, 305);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(0, 13);
            this.label37.TabIndex = 75;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.BackColor = System.Drawing.Color.Pink;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label38.Location = new System.Drawing.Point(510, 335);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(0, 13);
            this.label38.TabIndex = 76;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.BackColor = System.Drawing.Color.Pink;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label39.Location = new System.Drawing.Point(510, 365);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(0, 13);
            this.label39.TabIndex = 77;
            // 
            // btnAnasayfa
            // 
            this.btnAnasayfa.BackColor = System.Drawing.Color.Crimson;
            this.btnAnasayfa.Font = new System.Drawing.Font("Mia\'s Scribblings ~", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnAnasayfa.Location = new System.Drawing.Point(650, 365);
            this.btnAnasayfa.Name = "btnAnasayfa";
            this.btnAnasayfa.Size = new System.Drawing.Size(100, 40);
            this.btnAnasayfa.TabIndex = 78;
            this.btnAnasayfa.Text = "ANASAYFA";
            this.btnAnasayfa.UseVisualStyleBackColor = false;
            this.btnAnasayfa.Click += new System.EventHandler(this.btnAnasayfa_Click);
            // 
            // btnOgren
            // 
            this.btnOgren.BackColor = System.Drawing.Color.PaleVioletRed;
            this.btnOgren.Font = new System.Drawing.Font("Mia\'s Scribblings ~", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnOgren.Location = new System.Drawing.Point(650, 325);
            this.btnOgren.Name = "btnOgren";
            this.btnOgren.Size = new System.Drawing.Size(100, 40);
            this.btnOgren.TabIndex = 79;
            this.btnOgren.Text = "ÖĞRENELİM";
            this.btnOgren.UseVisualStyleBackColor = false;
            this.btnOgren.Click += new System.EventHandler(this.btnOgren_Click);
            // 
            // Hayvanlar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(752, 407);
            this.Controls.Add(this.btnOgren);
            this.Controls.Add(this.btnAnasayfa);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.btnZurafa);
            this.Controls.Add(this.btnZebra);
            this.Controls.Add(this.btnYengec);
            this.Controls.Add(this.btnYilanBalik);
            this.Controls.Add(this.btnYilan);
            this.Controls.Add(this.btnYarasa);
            this.Controls.Add(this.btnUskumru);
            this.Controls.Add(this.btnTimsah);
            this.Controls.Add(this.btnTilki);
            this.Controls.Add(this.btnTavusKus);
            this.Controls.Add(this.btnTavuk);
            this.Controls.Add(this.btnTavsan);
            this.Controls.Add(this.btnSincap);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.btnSerce);
            this.Controls.Add(this.btnSalyangoz);
            this.Controls.Add(this.btnMaymun);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.btnLeylek);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnKelebek);
            this.Controls.Add(this.btnKutup);
            this.Controls.Add(this.btnKurbaga);
            this.Controls.Add(this.btnKopek);
            this.Controls.Add(this.btnKoyun);
            this.Controls.Add(this.btnKedi);
            this.Controls.Add(this.btnKarinca);
            this.Controls.Add(this.btnKaplumbaga);
            this.Controls.Add(this.btnHoroz);
            this.Controls.Add(this.btnGoril);
            this.Controls.Add(this.btnGeyik);
            this.Controls.Add(this.btnFil);
            this.Controls.Add(this.btnFare);
            this.Controls.Add(this.btnEsek);
            this.Controls.Add(this.btnDeve);
            this.Controls.Add(this.btnBalina);
            this.Controls.Add(this.btnBalik);
            this.Controls.Add(this.btnAkrep);
            this.Controls.Add(this.btnAyi);
            this.Controls.Add(this.btnAt);
            this.Controls.Add(this.btnAslan);
            this.Controls.Add(this.btnAri);
            this.Name = "Hayvanlar";
            this.Text = "Hayvanlar";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAri;
        private System.Windows.Forms.Button btnAslan;
        private System.Windows.Forms.Button btnAt;
        private System.Windows.Forms.Button btnAyi;
        private System.Windows.Forms.Button btnAkrep;
        private System.Windows.Forms.Button btnBalik;
        private System.Windows.Forms.Button btnBalina;
        private System.Windows.Forms.Button btnDeve;
        private System.Windows.Forms.Button btnEsek;
        private System.Windows.Forms.Button btnFare;
        private System.Windows.Forms.Button btnFil;
        private System.Windows.Forms.Button btnGeyik;
        private System.Windows.Forms.Button btnGoril;
        private System.Windows.Forms.Button btnHoroz;
        private System.Windows.Forms.Button btnKaplumbaga;
        private System.Windows.Forms.Button btnKarinca;
        private System.Windows.Forms.Button btnKedi;
        private System.Windows.Forms.Button btnKoyun;
        private System.Windows.Forms.Button btnKopek;
        private System.Windows.Forms.Button btnKurbaga;
        private System.Windows.Forms.Button btnKutup;
        private System.Windows.Forms.Button btnKelebek;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button btnLeylek;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button btnMaymun;
        private System.Windows.Forms.Button btnSalyangoz;
        private System.Windows.Forms.Button btnSerce;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button btnSincap;
        private System.Windows.Forms.Button btnTavsan;
        private System.Windows.Forms.Button btnTavuk;
        private System.Windows.Forms.Button btnTavusKus;
        private System.Windows.Forms.Button btnTilki;
        private System.Windows.Forms.Button btnTimsah;
        private System.Windows.Forms.Button btnUskumru;
        private System.Windows.Forms.Button btnYarasa;
        private System.Windows.Forms.Button btnYilan;
        private System.Windows.Forms.Button btnYilanBalik;
        private System.Windows.Forms.Button btnYengec;
        private System.Windows.Forms.Button btnZebra;
        private System.Windows.Forms.Button btnZurafa;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Button btnAnasayfa;
        private System.Windows.Forms.Button btnOgren;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CocukEgitimSeti
{
    public partial class Sayılar : Form
    {
        public Sayılar()
        {
            InitializeComponent();
        }


        private void btnOgr_Click_1(object sender, EventArgs e)
        {
            Ogrenelim ogr = new Ogrenelim();
            ogr.Show();
            this.Hide();
        }

        private void btnAna_Click(object sender, EventArgs e)
        {
            form_index index = new form_index();
            index.Show();
            this.Hide();
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            lbl1.Text = "One";
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            lbl2.Text = "Two";
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            lbl3.Text = "Three";
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            lbl4.Text = "Four";
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            lbl5.Text = "Five";
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            lbl6.Text = "Six";
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            lbl7.Text = "Seven";
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            lbl8.Text = "Eight";
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            lbl9.Text = "Nine";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            lbl11.Text = "Eleven";    
        }

        private void button2_Click(object sender, EventArgs e)
        {
            lbl12.Text = "Twelve";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            lbl13.Text = "Thirteen";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            lbl14.Text = "Fourteen";
        }

        private void button5_Click(object sender, EventArgs e)
        {
            lbl15.Text = "Fifteen";
        }

        private void button6_Click(object sender, EventArgs e)
        {
            lbl16.Text = "Sixteen ";
        }

        private void button7_Click(object sender, EventArgs e)
        {
            lbl17.Text = "Seventeen ";
        }

        private void button8_Click(object sender, EventArgs e)
        {
            lbl18.Text = "Eighteen";
        }

        private void button9_Click(object sender, EventArgs e)
        {
            lbl19.Text = "Nineteen ";
        }

        private void btn10_Click(object sender, EventArgs e)
        {
            lbl10.Text = "Ten";
        }

        private void btn20_Click(object sender, EventArgs e)
        {
            lbl20.Text = "Twenty"; 
        }

        private void btn30_Click(object sender, EventArgs e)
        {
            lbl30.Text = "Thirty";
        }

        private void btn40_Click(object sender, EventArgs e)
        {
            lbl40.Text = "Forty";
        }

        private void btn50_Click(object sender, EventArgs e)
        {
            lbl50.Text = "Fifty";
        }

        private void btn60_Click(object sender, EventArgs e)
        {
            lbl60.Text = "Tossed";
        }

        private void btn70_Click(object sender, EventArgs e)
        {
            lbl70.Text = "Seventy";
        }

        private void btn80_Click(object sender, EventArgs e)
        {
            lbl80.Text = "Eighty";
        }

        private void btn90_Click(object sender, EventArgs e)
        {
            lbl90.Text = "Ninety";
        }

        
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CocukEgitimSeti
{
    public partial class Sebzeler : Form
    {
        public Sebzeler()
        {
            InitializeComponent();
        }

       

        
       
        private void button1_Click(object sender, EventArgs e)
        {
            label16.Text = "VEGETABLE";
        }

        private void btnOgr_Click(object sender, EventArgs e)
        {
            Ogrenelim ogren = new Ogrenelim();
            ogren.Show();
            this.Hide();
        }

        private void btnAna_Click(object sender, EventArgs e)
        {
            form_index index = new form_index();
            index.Show();
            this.Hide();
        }

       

        private void btnBiber_Click_1(object sender, EventArgs e)
        {
            label1.Text = "PEPPER";
        }

        private void btnFasulye_Click_1(object sender, EventArgs e)
        {
            label2.Text = "BEAN";
        }

        private void btnHavuç_Click(object sender, EventArgs e)
        {
            label3.Text = "CARROT";
        }

        private void btnKarnıbahar_Click(object sender, EventArgs e)
        {
            label4.Text = "CAULIFLOWER";
        }

        private void btnBrokoli_Click(object sender, EventArgs e)
        {
            label5.Text = "BROCCOLI";
        }

        private void btnLahana_Click_1(object sender, EventArgs e)
        {
            label6.Text = "CABBAGE";
        }

        private void btnMantar_Click_1(object sender, EventArgs e)
        {
            label7.Text = "MUSHROOM";
        }

        private void btnMarul_Click_1(object sender, EventArgs e)
        {
            label8.Text = "LETTUCE";
        }

        private void btnPancar_Click_1(object sender, EventArgs e)
        {
            label9.Text = "BEETROOT";
        }

        private void btnPatates_Click_1(object sender, EventArgs e)
        {
            label10.Text = "POTATO";
        }

        private void btnPatlican_Click_1(object sender, EventArgs e)
        {
            label11.Text = "AUBERGINE";
        }

        private void btnSalatalik_Click_1(object sender, EventArgs e)
        {
            label12.Text = "CUCUMBER";
        }

        private void btnSarimsak_Click_1(object sender, EventArgs e)
        {
            label13.Text = "GARLIC";
        }

        private void btnSogan_Click_1(object sender, EventArgs e)
        {
            label14.Text = "ONION";
        }

        private void btnZeytin_Click_1(object sender, EventArgs e)
        {
            label15.Text = "OLIVE";
        }
    }
}

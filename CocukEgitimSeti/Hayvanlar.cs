﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CocukEgitimSeti
{
    public partial class Hayvanlar : Form
    {
        public Hayvanlar()
        {
            InitializeComponent();
        }

        private void btnAri_Click(object sender, EventArgs e)
        {
            label1.Text = "BEE";
        }

        private void btnAkrep_Click(object sender, EventArgs e)
        {
            label2.Text = "SCORPION";
        }

        private void btnAslan_Click(object sender, EventArgs e)
        {
            label3.Text = "LION";
        }

        private void btnAt_Click(object sender, EventArgs e)
        {
            label4.Text = "HORSE";
        }

        private void btnAyi_Click(object sender, EventArgs e)
        {
            label5.Text = "BEAR";
        }

        private void btnBalik_Click(object sender, EventArgs e)
        {
            label6.Text = "FISH";
        }

        private void btnBalina_Click(object sender, EventArgs e)
        {
            label7.Text = "WHALE";
        }

        private void btnDeve_Click(object sender, EventArgs e)
        {
            label8.Text = "CAMEL";
        }

        private void btnEsek_Click(object sender, EventArgs e)
        {
            label9.Text = "ASS";
        }

        private void btnFare_Click(object sender, EventArgs e)
        {
            label10.Text = "MOUSE";
        }

        private void btnFil_Click(object sender, EventArgs e)
        {
            label11.Text = "ELEPHANT";
        }

        private void btnGeyik_Click(object sender, EventArgs e)
        {
            label12.Text = "DEER";
        }

        private void btnGoril_Click(object sender, EventArgs e)
        {
            label13.Text = "GORİLLA";
        }

        private void btnHoroz_Click(object sender, EventArgs e)
        {
            label14.Text = "COCK";
        }

        private void btnKaplumbaga_Click(object sender, EventArgs e)
        {
            label15.Text = "TURTLE";
        }

        private void btnKarinca_Click(object sender, EventArgs e)
        {
            label16.Text = "ANT";
        }

        private void btnKedi_Click(object sender, EventArgs e)
        {
            label17.Text = "CAT";
        }

        private void btnKelebek_Click(object sender, EventArgs e)
        {
            label18.Text = "BUTTERFLY";
        }

        private void btnKoyun_Click(object sender, EventArgs e)
        {
            label19.Text = "SHEEP";
        }

        private void btnKopek_Click(object sender, EventArgs e)
        {
            label20.Text = "DOG";
        }

        private void btnKurbaga_Click(object sender, EventArgs e)
        {
            label21.Text = "FROG";
        }

        private void btnKutup_Click(object sender, EventArgs e)
        {
            label22.Text = "POLAR BEER";
        }

        private void btnLeylek_Click(object sender, EventArgs e)
        {
            label23.Text = "STORK";
        }

        private void btnMaymun_Click(object sender, EventArgs e)
        {
            label24.Text = "MONKEY";
        }

        private void btnSalyangoz_Click(object sender, EventArgs e)
        {
            label25.Text = "SNAIL";
        }

        private void btnSerce_Click(object sender, EventArgs e)
        {
            label26.Text = "SPARROW";
        }

        private void btnSincap_Click(object sender, EventArgs e)
        {
            label27.Text = "SQUIRREL";
        }

        private void btnTavsan_Click(object sender, EventArgs e)
        {
            label28.Text = "RABBIT";
        }

        private void btnTavuk_Click(object sender, EventArgs e)
        {
            label29.Text = "CHICKEN";
        }

        private void btnTavusKus_Click(object sender, EventArgs e)
        {
            label30.Text = "PEACOCK";
        }

        private void btnTilki_Click(object sender, EventArgs e)
        {
            label31.Text = "FOX";
        }

        private void btnTimsah_Click(object sender, EventArgs e)
        {
            label32.Text = "CROCODILE";
        }

        private void btnUskumru_Click(object sender, EventArgs e)
        {
            label33.Text = "MACKEREL";
        }

        private void btnYarasa_Click(object sender, EventArgs e)
        {
            label34.Text = "BAT";
        }

        private void btnYengec_Click(object sender, EventArgs e)
        {
            label35.Text = "CRAB";
        }

        private void btnYilan_Click(object sender, EventArgs e)
        {
            label36.Text = "SNAKE";
        }

        private void btnYilanBalik_Click(object sender, EventArgs e)
        {
            label37.Text = "EEL";
        }

        private void btnZebra_Click(object sender, EventArgs e)
        {
            label38.Text = "ZEBRA";
        }

        private void btnZurafa_Click(object sender, EventArgs e)
        {
            label39.Text = "GRAFFE";
        }

        private void btnAnasayfa_Click(object sender, EventArgs e)
        {
            form_index index = new form_index();
            index.Show();
            this.Hide();
        }

        private void btnOgren_Click(object sender, EventArgs e)
        {
            Ogrenelim ogren = new Ogrenelim();
            ogren.Show();
            this.Hide();
        }
    }
}

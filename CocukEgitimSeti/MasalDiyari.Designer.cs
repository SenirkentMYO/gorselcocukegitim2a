﻿namespace CocukEgitimSeti
{
    partial class MasalDiyari
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_index = new System.Windows.Forms.Button();
            this.btn_hungry = new System.Windows.Forms.Button();
            this.lbl_Masal = new System.Windows.Forms.Label();
            this.btn_Cin = new System.Windows.Forms.Button();
            this.btn_Rap = new System.Windows.Forms.Button();
            this.btn_Friend = new System.Windows.Forms.Button();
            this.btn_Rabbit = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // btn_index
            // 
            this.btn_index.BackColor = System.Drawing.Color.Crimson;
            this.btn_index.Font = new System.Drawing.Font("Mia\'s Scribblings ~", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_index.Location = new System.Drawing.Point(713, 279);
            this.btn_index.Name = "btn_index";
            this.btn_index.Size = new System.Drawing.Size(87, 40);
            this.btn_index.TabIndex = 0;
            this.btn_index.Text = "ANASAYFA";
            this.btn_index.UseVisualStyleBackColor = false;
            this.btn_index.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn_hungry
            // 
            this.btn_hungry.BackColor = System.Drawing.Color.SteelBlue;
            this.btn_hungry.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_hungry.Location = new System.Drawing.Point(0, 60);
            this.btn_hungry.Name = "btn_hungry";
            this.btn_hungry.Size = new System.Drawing.Size(130, 30);
            this.btn_hungry.TabIndex = 1;
            this.btn_hungry.Text = "HUNGRY MOUSE";
            this.btn_hungry.UseVisualStyleBackColor = false;
            this.btn_hungry.Click += new System.EventHandler(this.btn_hungry_Click);
            // 
            // lbl_Masal
            // 
            this.lbl_Masal.AutoSize = true;
            this.lbl_Masal.BackColor = System.Drawing.Color.Crimson;
            this.lbl_Masal.Font = new System.Drawing.Font("Bradley Hand ITC", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Masal.ForeColor = System.Drawing.Color.White;
            this.lbl_Masal.Location = new System.Drawing.Point(200, 10);
            this.lbl_Masal.Name = "lbl_Masal";
            this.lbl_Masal.Size = new System.Drawing.Size(214, 30);
            this.lbl_Masal.TabIndex = 2;
            this.lbl_Masal.Text = "MASALLARIMIZ";
            // 
            // btn_Cin
            // 
            this.btn_Cin.BackColor = System.Drawing.Color.SteelBlue;
            this.btn_Cin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_Cin.Location = new System.Drawing.Point(0, 100);
            this.btn_Cin.Name = "btn_Cin";
            this.btn_Cin.Size = new System.Drawing.Size(110, 30);
            this.btn_Cin.TabIndex = 3;
            this.btn_Cin.Text = "CİNDERELLA";
            this.btn_Cin.UseVisualStyleBackColor = false;
            this.btn_Cin.Click += new System.EventHandler(this.btn_Cin_Click);
            // 
            // btn_Rap
            // 
            this.btn_Rap.BackColor = System.Drawing.Color.SteelBlue;
            this.btn_Rap.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_Rap.Location = new System.Drawing.Point(0, 180);
            this.btn_Rap.Name = "btn_Rap";
            this.btn_Rap.Size = new System.Drawing.Size(110, 30);
            this.btn_Rap.TabIndex = 4;
            this.btn_Rap.Text = "RAPUNZEL";
            this.btn_Rap.UseVisualStyleBackColor = false;
            this.btn_Rap.Click += new System.EventHandler(this.btn_Rap_Click);
            // 
            // btn_Friend
            // 
            this.btn_Friend.BackColor = System.Drawing.Color.SteelBlue;
            this.btn_Friend.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_Friend.Location = new System.Drawing.Point(0, 140);
            this.btn_Friend.Name = "btn_Friend";
            this.btn_Friend.Size = new System.Drawing.Size(110, 30);
            this.btn_Friend.TabIndex = 5;
            this.btn_Friend.Text = "A REAL FRIEND";
            this.btn_Friend.UseVisualStyleBackColor = false;
            this.btn_Friend.Click += new System.EventHandler(this.btn_Friend_Click);
            // 
            // btn_Rabbit
            // 
            this.btn_Rabbit.BackColor = System.Drawing.Color.SteelBlue;
            this.btn_Rabbit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_Rabbit.Location = new System.Drawing.Point(0, 220);
            this.btn_Rabbit.Name = "btn_Rabbit";
            this.btn_Rabbit.Size = new System.Drawing.Size(130, 30);
            this.btn_Rabbit.TabIndex = 6;
            this.btn_Rabbit.Text = "THE LOST RABBIT";
            this.btn_Rabbit.UseVisualStyleBackColor = false;
            this.btn_Rabbit.Click += new System.EventHandler(this.btn_Rabbit_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(160, 60);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(640, 190);
            this.richTextBox1.TabIndex = 7;
            this.richTextBox1.Text = "";
            // 
            // MasalDiyari
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(811, 325);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.btn_Rabbit);
            this.Controls.Add(this.btn_Friend);
            this.Controls.Add(this.btn_Rap);
            this.Controls.Add(this.btn_Cin);
            this.Controls.Add(this.lbl_Masal);
            this.Controls.Add(this.btn_hungry);
            this.Controls.Add(this.btn_index);
            this.Name = "MasalDiyari";
            this.Text = "Masal Diyarı";
            this.Load += new System.EventHandler(this.MasalDiyari_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_index;
        private System.Windows.Forms.Button btn_hungry;
        private System.Windows.Forms.Label lbl_Masal;
        private System.Windows.Forms.Button btn_Cin;
        private System.Windows.Forms.Button btn_Rap;
        private System.Windows.Forms.Button btn_Friend;
        private System.Windows.Forms.Button btn_Rabbit;
        private System.Windows.Forms.RichTextBox richTextBox1;
    }
}
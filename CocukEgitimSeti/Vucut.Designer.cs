﻿namespace CocukEgitimSeti
{
    partial class Vucut
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Vucut));
            this.btnAna = new System.Windows.Forms.Button();
            this.btnOgr = new System.Windows.Forms.Button();
            this.pB1 = new System.Windows.Forms.PictureBox();
            this.lbl1 = new System.Windows.Forms.Label();
            this.pB2 = new System.Windows.Forms.PictureBox();
            this.lbl2 = new System.Windows.Forms.Label();
            this.pB3 = new System.Windows.Forms.PictureBox();
            this.pB4 = new System.Windows.Forms.PictureBox();
            this.pB5 = new System.Windows.Forms.PictureBox();
            this.pB6 = new System.Windows.Forms.PictureBox();
            this.pB7 = new System.Windows.Forms.PictureBox();
            this.pB8 = new System.Windows.Forms.PictureBox();
            this.pB9 = new System.Windows.Forms.PictureBox();
            this.lbl3 = new System.Windows.Forms.Label();
            this.lbl4 = new System.Windows.Forms.Label();
            this.lbl5 = new System.Windows.Forms.Label();
            this.lbl6 = new System.Windows.Forms.Label();
            this.lbl7 = new System.Windows.Forms.Label();
            this.lbl8 = new System.Windows.Forms.Label();
            this.lbl9 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pB1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB9)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAna
            // 
            this.btnAna.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnAna.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnAna.Location = new System.Drawing.Point(578, 31);
            this.btnAna.Name = "btnAna";
            this.btnAna.Size = new System.Drawing.Size(148, 40);
            this.btnAna.TabIndex = 38;
            this.btnAna.Text = "ANASAYFA";
            this.btnAna.UseVisualStyleBackColor = false;
            this.btnAna.Click += new System.EventHandler(this.btnAna_Click);
            // 
            // btnOgr
            // 
            this.btnOgr.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnOgr.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnOgr.Location = new System.Drawing.Point(578, 88);
            this.btnOgr.Name = "btnOgr";
            this.btnOgr.Size = new System.Drawing.Size(148, 40);
            this.btnOgr.TabIndex = 39;
            this.btnOgr.Text = "ÖĞRENELİM";
            this.btnOgr.UseVisualStyleBackColor = false;
            this.btnOgr.Click += new System.EventHandler(this.btnOgr_Click);
            // 
            // pB1
            // 
            this.pB1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pB1.BackgroundImage")));
            this.pB1.Location = new System.Drawing.Point(30, 12);
            this.pB1.Name = "pB1";
            this.pB1.Size = new System.Drawing.Size(69, 60);
            this.pB1.TabIndex = 40;
            this.pB1.TabStop = false;
            this.pB1.Click += new System.EventHandler(this.pB1_Click);
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.BackColor = System.Drawing.Color.Transparent;
            this.lbl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl1.Location = new System.Drawing.Point(137, 31);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(0, 20);
            this.lbl1.TabIndex = 41;
            // 
            // pB2
            // 
            this.pB2.Image = ((System.Drawing.Image)(resources.GetObject("pB2.Image")));
            this.pB2.Location = new System.Drawing.Point(30, 101);
            this.pB2.Name = "pB2";
            this.pB2.Size = new System.Drawing.Size(69, 76);
            this.pB2.TabIndex = 42;
            this.pB2.TabStop = false;
            this.pB2.Click += new System.EventHandler(this.pB2_Click);
            // 
            // lbl2
            // 
            this.lbl2.AutoSize = true;
            this.lbl2.BackColor = System.Drawing.Color.Transparent;
            this.lbl2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl2.Location = new System.Drawing.Point(139, 136);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(0, 20);
            this.lbl2.TabIndex = 43;
            // 
            // pB3
            // 
            this.pB3.Image = ((System.Drawing.Image)(resources.GetObject("pB3.Image")));
            this.pB3.Location = new System.Drawing.Point(30, 216);
            this.pB3.Name = "pB3";
            this.pB3.Size = new System.Drawing.Size(85, 81);
            this.pB3.TabIndex = 44;
            this.pB3.TabStop = false;
            this.pB3.Click += new System.EventHandler(this.pB3_Click);
            // 
            // pB4
            // 
            this.pB4.Image = ((System.Drawing.Image)(resources.GetObject("pB4.Image")));
            this.pB4.Location = new System.Drawing.Point(30, 349);
            this.pB4.Name = "pB4";
            this.pB4.Size = new System.Drawing.Size(58, 88);
            this.pB4.TabIndex = 45;
            this.pB4.TabStop = false;
            this.pB4.Click += new System.EventHandler(this.pB4_Click);
            // 
            // pB5
            // 
            this.pB5.Image = ((System.Drawing.Image)(resources.GetObject("pB5.Image")));
            this.pB5.Location = new System.Drawing.Point(251, 12);
            this.pB5.Name = "pB5";
            this.pB5.Size = new System.Drawing.Size(100, 125);
            this.pB5.TabIndex = 46;
            this.pB5.TabStop = false;
            this.pB5.Click += new System.EventHandler(this.pB5_Click);
            // 
            // pB6
            // 
            this.pB6.Image = ((System.Drawing.Image)(resources.GetObject("pB6.Image")));
            this.pB6.Location = new System.Drawing.Point(251, 180);
            this.pB6.Name = "pB6";
            this.pB6.Size = new System.Drawing.Size(100, 117);
            this.pB6.TabIndex = 47;
            this.pB6.TabStop = false;
            this.pB6.Click += new System.EventHandler(this.pB6_Click);
            // 
            // pB7
            // 
            this.pB7.Image = ((System.Drawing.Image)(resources.GetObject("pB7.Image")));
            this.pB7.Location = new System.Drawing.Point(251, 361);
            this.pB7.Name = "pB7";
            this.pB7.Size = new System.Drawing.Size(100, 76);
            this.pB7.TabIndex = 48;
            this.pB7.TabStop = false;
            this.pB7.Click += new System.EventHandler(this.pB7_Click);
            // 
            // pB8
            // 
            this.pB8.Image = ((System.Drawing.Image)(resources.GetObject("pB8.Image")));
            this.pB8.Location = new System.Drawing.Point(498, 164);
            this.pB8.Name = "pB8";
            this.pB8.Size = new System.Drawing.Size(100, 133);
            this.pB8.TabIndex = 49;
            this.pB8.TabStop = false;
            this.pB8.Click += new System.EventHandler(this.pB8_Click);
            // 
            // pB9
            // 
            this.pB9.Image = ((System.Drawing.Image)(resources.GetObject("pB9.Image")));
            this.pB9.Location = new System.Drawing.Point(498, 341);
            this.pB9.Name = "pB9";
            this.pB9.Size = new System.Drawing.Size(100, 96);
            this.pB9.TabIndex = 50;
            this.pB9.TabStop = false;
            this.pB9.Click += new System.EventHandler(this.pB9_Click);
            // 
            // lbl3
            // 
            this.lbl3.AutoSize = true;
            this.lbl3.BackColor = System.Drawing.Color.Transparent;
            this.lbl3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl3.Location = new System.Drawing.Point(139, 250);
            this.lbl3.Name = "lbl3";
            this.lbl3.Size = new System.Drawing.Size(0, 20);
            this.lbl3.TabIndex = 51;
            // 
            // lbl4
            // 
            this.lbl4.AutoSize = true;
            this.lbl4.BackColor = System.Drawing.Color.Transparent;
            this.lbl4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl4.Location = new System.Drawing.Point(138, 388);
            this.lbl4.Name = "lbl4";
            this.lbl4.Size = new System.Drawing.Size(0, 20);
            this.lbl4.TabIndex = 52;
            // 
            // lbl5
            // 
            this.lbl5.AutoSize = true;
            this.lbl5.BackColor = System.Drawing.Color.Transparent;
            this.lbl5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl5.Location = new System.Drawing.Point(406, 90);
            this.lbl5.Name = "lbl5";
            this.lbl5.Size = new System.Drawing.Size(0, 20);
            this.lbl5.TabIndex = 53;
            // 
            // lbl6
            // 
            this.lbl6.AutoSize = true;
            this.lbl6.BackColor = System.Drawing.Color.Transparent;
            this.lbl6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl6.Location = new System.Drawing.Point(392, 241);
            this.lbl6.Name = "lbl6";
            this.lbl6.Size = new System.Drawing.Size(0, 20);
            this.lbl6.TabIndex = 54;
            // 
            // lbl7
            // 
            this.lbl7.AutoSize = true;
            this.lbl7.BackColor = System.Drawing.Color.Transparent;
            this.lbl7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl7.Location = new System.Drawing.Point(387, 395);
            this.lbl7.Name = "lbl7";
            this.lbl7.Size = new System.Drawing.Size(0, 20);
            this.lbl7.TabIndex = 55;
            // 
            // lbl8
            // 
            this.lbl8.AutoSize = true;
            this.lbl8.BackColor = System.Drawing.Color.Transparent;
            this.lbl8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl8.Location = new System.Drawing.Point(660, 244);
            this.lbl8.Name = "lbl8";
            this.lbl8.Size = new System.Drawing.Size(0, 20);
            this.lbl8.TabIndex = 56;
            // 
            // lbl9
            // 
            this.lbl9.AutoSize = true;
            this.lbl9.BackColor = System.Drawing.Color.Transparent;
            this.lbl9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl9.Location = new System.Drawing.Point(672, 391);
            this.lbl9.Name = "lbl9";
            this.lbl9.Size = new System.Drawing.Size(0, 20);
            this.lbl9.TabIndex = 57;
            // 
            // Vucut
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(770, 490);
            this.Controls.Add(this.lbl9);
            this.Controls.Add(this.lbl8);
            this.Controls.Add(this.lbl7);
            this.Controls.Add(this.lbl6);
            this.Controls.Add(this.lbl5);
            this.Controls.Add(this.lbl4);
            this.Controls.Add(this.lbl3);
            this.Controls.Add(this.pB9);
            this.Controls.Add(this.pB8);
            this.Controls.Add(this.pB7);
            this.Controls.Add(this.pB6);
            this.Controls.Add(this.pB5);
            this.Controls.Add(this.pB4);
            this.Controls.Add(this.pB3);
            this.Controls.Add(this.lbl2);
            this.Controls.Add(this.pB2);
            this.Controls.Add(this.lbl1);
            this.Controls.Add(this.pB1);
            this.Controls.Add(this.btnOgr);
            this.Controls.Add(this.btnAna);
            this.Name = "Vucut";
            this.Text = "Vucut";
            ((System.ComponentModel.ISupportInitialize)(this.pB1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pB9)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAna;
        private System.Windows.Forms.Button btnOgr;
        private System.Windows.Forms.PictureBox pB1;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.PictureBox pB2;
        private System.Windows.Forms.Label lbl2;
        private System.Windows.Forms.PictureBox pB3;
        private System.Windows.Forms.PictureBox pB4;
        private System.Windows.Forms.PictureBox pB5;
        private System.Windows.Forms.PictureBox pB6;
        private System.Windows.Forms.PictureBox pB7;
        private System.Windows.Forms.PictureBox pB8;
        private System.Windows.Forms.PictureBox pB9;
        private System.Windows.Forms.Label lbl3;
        private System.Windows.Forms.Label lbl4;
        private System.Windows.Forms.Label lbl5;
        private System.Windows.Forms.Label lbl6;
        private System.Windows.Forms.Label lbl7;
        private System.Windows.Forms.Label lbl8;
        private System.Windows.Forms.Label lbl9;
    }
}
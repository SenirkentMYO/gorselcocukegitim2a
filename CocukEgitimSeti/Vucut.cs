﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CocukEgitimSeti
{
    public partial class Vucut : Form
    {
        public Vucut()
        {
            InitializeComponent();
        }

        private void pB1_Click(object sender, EventArgs e)
        {
            lbl1.Text = "Mouth";
        }

        private void btnAna_Click(object sender, EventArgs e)
        {
            form_index ind = new form_index();
            ind.Show();
            this.Hide();
        }

        private void pB2_Click(object sender, EventArgs e)
        {
            lbl2.Text = "Nose";
        }

        private void pB3_Click(object sender, EventArgs e)
        {
            lbl3.Text = "Eye";
        }

        private void pB4_Click(object sender, EventArgs e)
        {
            lbl4.Text = "Ear";
        }

        private void pB5_Click(object sender, EventArgs e)
        {
            lbl5.Text = "Face";
        }

        private void pB6_Click(object sender, EventArgs e)
        {
            lbl6.Text = "Arm";
        }

        private void pB7_Click(object sender, EventArgs e)
        {
            lbl7.Text = "Hand";
        }

        private void pB8_Click(object sender, EventArgs e)
        {
            lbl8.Text = "Leg";
        }

        private void pB9_Click(object sender, EventArgs e)
        {
            lbl9.Text = "Foot";
        }

        private void btnOgr_Click(object sender, EventArgs e)
        {
            Ogrenelim ogr = new Ogrenelim();
            ogr.Show();
            this.Hide();
        }
    }
}

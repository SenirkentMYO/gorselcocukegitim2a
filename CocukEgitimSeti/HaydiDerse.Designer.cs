﻿namespace CocukEgitimSeti
{
    partial class frm_dersler
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_index = new System.Windows.Forms.Button();
            this.btZamir = new System.Windows.Forms.Button();
            this.btIsim = new System.Windows.Forms.Button();
            this.btDil = new System.Windows.Forms.Button();
            this.btSifat = new System.Windows.Forms.Button();
            this.btIyelik = new System.Windows.Forms.Button();
            this.btSoru = new System.Windows.Forms.Button();
            this.btYer = new System.Windows.Forms.Button();
            this.btDzensizFl = new System.Windows.Forms.Button();
            this.lbDersler = new System.Windows.Forms.Label();
            this.gvDersler = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.gvDersler)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_index
            // 
            this.btn_index.BackColor = System.Drawing.Color.Crimson;
            this.btn_index.Font = new System.Drawing.Font("Mia\'s Scribblings ~", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_index.ForeColor = System.Drawing.Color.Black;
            this.btn_index.Location = new System.Drawing.Point(776, 398);
            this.btn_index.Name = "btn_index";
            this.btn_index.Size = new System.Drawing.Size(100, 50);
            this.btn_index.TabIndex = 7;
            this.btn_index.Text = "ANASAYFA";
            this.btn_index.UseVisualStyleBackColor = false;
            this.btn_index.Click += new System.EventHandler(this.btn_index_Click);
            // 
            // btZamir
            // 
            this.btZamir.BackColor = System.Drawing.Color.IndianRed;
            this.btZamir.Font = new System.Drawing.Font("Bookman Old Style", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btZamir.Location = new System.Drawing.Point(0, 64);
            this.btZamir.Name = "btZamir";
            this.btZamir.Size = new System.Drawing.Size(166, 30);
            this.btZamir.TabIndex = 13;
            this.btZamir.Text = "ZAMİRLER";
            this.btZamir.UseVisualStyleBackColor = false;
            this.btZamir.Click += new System.EventHandler(this.btZamir_Click);
            // 
            // btIsim
            // 
            this.btIsim.BackColor = System.Drawing.Color.Coral;
            this.btIsim.Font = new System.Drawing.Font("Bookman Old Style", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btIsim.Location = new System.Drawing.Point(0, 99);
            this.btIsim.Name = "btIsim";
            this.btIsim.Size = new System.Drawing.Size(156, 30);
            this.btIsim.TabIndex = 14;
            this.btIsim.Text = "İSİMLER";
            this.btIsim.UseVisualStyleBackColor = false;
            this.btIsim.Click += new System.EventHandler(this.btIsim_Click);
            // 
            // btDil
            // 
            this.btDil.BackColor = System.Drawing.Color.RosyBrown;
            this.btDil.Font = new System.Drawing.Font("Bookman Old Style", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btDil.Location = new System.Drawing.Point(0, 134);
            this.btDil.Name = "btDil";
            this.btDil.Size = new System.Drawing.Size(146, 30);
            this.btDil.TabIndex = 15;
            this.btDil.Text = "DİLLER";
            this.btDil.UseVisualStyleBackColor = false;
            this.btDil.Click += new System.EventHandler(this.btDil_Click);
            // 
            // btSifat
            // 
            this.btSifat.BackColor = System.Drawing.Color.Salmon;
            this.btSifat.Font = new System.Drawing.Font("Bookman Old Style", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btSifat.Location = new System.Drawing.Point(0, 169);
            this.btSifat.Name = "btSifat";
            this.btSifat.Size = new System.Drawing.Size(136, 30);
            this.btSifat.TabIndex = 16;
            this.btSifat.Text = "SIFATLAR";
            this.btSifat.UseVisualStyleBackColor = false;
            this.btSifat.Click += new System.EventHandler(this.btSifat_Click);
            // 
            // btIyelik
            // 
            this.btIyelik.BackColor = System.Drawing.Color.Salmon;
            this.btIyelik.Font = new System.Drawing.Font("Bookman Old Style", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btIyelik.Location = new System.Drawing.Point(0, 204);
            this.btIyelik.Name = "btIyelik";
            this.btIyelik.Size = new System.Drawing.Size(136, 30);
            this.btIyelik.TabIndex = 17;
            this.btIyelik.Text = "İYELİK";
            this.btIyelik.UseVisualStyleBackColor = false;
            this.btIyelik.Click += new System.EventHandler(this.btIyelik_Click);
            // 
            // btSoru
            // 
            this.btSoru.BackColor = System.Drawing.Color.RosyBrown;
            this.btSoru.Font = new System.Drawing.Font("Bookman Old Style", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btSoru.Location = new System.Drawing.Point(0, 239);
            this.btSoru.Name = "btSoru";
            this.btSoru.Size = new System.Drawing.Size(146, 30);
            this.btSoru.TabIndex = 18;
            this.btSoru.Text = "SORULAR";
            this.btSoru.UseVisualStyleBackColor = false;
            this.btSoru.Click += new System.EventHandler(this.btSoru_Click);
            // 
            // btYer
            // 
            this.btYer.BackColor = System.Drawing.Color.Coral;
            this.btYer.Font = new System.Drawing.Font("Bookman Old Style", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btYer.Location = new System.Drawing.Point(0, 274);
            this.btYer.Name = "btYer";
            this.btYer.Size = new System.Drawing.Size(156, 30);
            this.btYer.TabIndex = 19;
            this.btYer.Text = "YER";
            this.btYer.UseVisualStyleBackColor = false;
            this.btYer.Click += new System.EventHandler(this.btYer_Click);
            // 
            // btDzensizFl
            // 
            this.btDzensizFl.BackColor = System.Drawing.Color.IndianRed;
            this.btDzensizFl.Font = new System.Drawing.Font("Bookman Old Style", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btDzensizFl.Location = new System.Drawing.Point(0, 309);
            this.btDzensizFl.Name = "btDzensizFl";
            this.btDzensizFl.Size = new System.Drawing.Size(166, 30);
            this.btDzensizFl.TabIndex = 20;
            this.btDzensizFl.Text = "DÜZENSİZ FİİLLER";
            this.btDzensizFl.UseVisualStyleBackColor = false;
            this.btDzensizFl.Click += new System.EventHandler(this.btDzensizFl_Click);
            // 
            // lbDersler
            // 
            this.lbDersler.AutoSize = true;
            this.lbDersler.BackColor = System.Drawing.Color.Crimson;
            this.lbDersler.Font = new System.Drawing.Font("Bookman Old Style", 21.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbDersler.Location = new System.Drawing.Point(296, 9);
            this.lbDersler.Name = "lbDersler";
            this.lbDersler.Size = new System.Drawing.Size(227, 34);
            this.lbDersler.TabIndex = 21;
            this.lbDersler.Text = "DERSLERİMİZ";
            // 
            // gvDersler
            // 
            this.gvDersler.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvDersler.Location = new System.Drawing.Point(292, 64);
            this.gvDersler.Name = "gvDersler";
            this.gvDersler.Size = new System.Drawing.Size(561, 275);
            this.gvDersler.TabIndex = 22;
            // 
            // frm_dersler
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(879, 451);
            this.Controls.Add(this.gvDersler);
            this.Controls.Add(this.lbDersler);
            this.Controls.Add(this.btDzensizFl);
            this.Controls.Add(this.btYer);
            this.Controls.Add(this.btSoru);
            this.Controls.Add(this.btIyelik);
            this.Controls.Add(this.btSifat);
            this.Controls.Add(this.btDil);
            this.Controls.Add(this.btIsim);
            this.Controls.Add(this.btZamir);
            this.Controls.Add(this.btn_index);
            this.Name = "frm_dersler";
            this.Text = "Dersler";
            this.Activated += new System.EventHandler(this.frm_dersler_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.gvDersler)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_index;
        private System.Windows.Forms.Button btZamir;
        private System.Windows.Forms.Button btIsim;
        private System.Windows.Forms.Button btDil;
        private System.Windows.Forms.Button btSifat;
        private System.Windows.Forms.Button btIyelik;
        private System.Windows.Forms.Button btSoru;
        private System.Windows.Forms.Button btYer;
        private System.Windows.Forms.Button btDzensizFl;
        private System.Windows.Forms.Label lbDersler;
        private System.Windows.Forms.DataGridView gvDersler;
    }
}
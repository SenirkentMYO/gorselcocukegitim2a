﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CocukEgitimSeti
{
    public partial class MasalDiyari : Form
    {
        public MasalDiyari()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            form_index index = new form_index();
            index.Show();
            this.Hide();
        }

        private void btn_hungry_Click(object sender, EventArgs e)
        {
            richTextBox1.Visible = true;
            richTextBox1.Text="Mouse, said the rat, if you want to climb out of the basket, you must wait till you have grown as thin as you were when you went in.Just then a rat came along, and he heard the mouse. How shall I climb out? said the mouse. oh, how shall I climb out? When the mouse tried to climb out of the basket, she could not. She was too fat to pass through the hole. Then she began to eat the corn. Being very hungry, she ate a great deal, and went on eating and eating. She had grown very fat before she felt that she had had enough. At last the mouse found a basket, full of corn. There was a small hole in the basket, and she crept in. She could just get through the hole.     A mouse was having a very bad time. She could find no food at all. She looked here and there, but there was no food, and she grew very thin.";

        }

        private void btn_Cin_Click(object sender, EventArgs e)
        {
            richTextBox1.Visible = true;
            richTextBox1.Text = "In former times, a rich man and his wife were the parents of a beautiful little daughter, but before she had arrived at womanhood, her dear mother fell sick and died to the grief of her husband and daughter. After a time, the little girl’s father married another lady. Now this lady was proud and haughty, and had two grown-up daughters as disagreeable as herself. She could not endure her step-daughter and she gave her the most degrading occupations, and compelled her to wash the dishes and clean the stairs. When the poor girl had finished her work, she used to sit in the chimney-corner amongst the cinders, which made her sisters give her the name of “Cinderella.” However, in her shabby clothes Cinderella was ten times handsomer than her sisters.";
        }

        private void btn_Friend_Click(object sender, EventArgs e)
        {
            richTextBox1.Visible = true;
            richTextBox1.Text = "A new summer holiday has just started and Elsa, a 10 year old girl, was wondering what it had in store for her this time. The sun was smiling at her, promising captivating moments this summer, the leaves were whispering secrets which wind carried along, so that she could hear them… She was looking at the clouds wondering where they led to, wishing she could step into their way, following them. If only I had wings to fly and lose myself among the birds and the clouds…, she thought.";
        }

        private void btn_Rabbit_Click(object sender, EventArgs e)
        {
            richTextBox1.Visible = true;
            richTextBox1.Text = "Bunny was a little rabbit, the youngest of a large family. His home was in an old wood, where the trees were very high, and wild-flowers grew in great abundance. His mother had given him to understand that he must not stray away from her, lest he should get lost, and not be able to find her.";
        }

        private void btn_Rap_Click(object sender, EventArgs e)
        {
            richTextBox1.Visible = true;
            richTextBox1.Text = "Once upon a time there lived a man and his wife who were very unhappy because they had no children.These good people had a little window at the back of their house, which looked into the most lovely garden, full of all manner of beautiful flowers and vegetables; but the garden was surrounded by a high wall, and no one dared to enter it, for it belonged to a witch of great power, who was feared by the whole world. One day the woman stood at the window overlooking the garden, and saw there a bed full of the finest rampion: the leaves looked so fresh and green that she longed to eat them. The desire grew day by day, and just because she knew she couldn't possibly get any, she pined away and became quite pale and wretched. Then her husband grew alarmed and said:                            `What ails you, dear wife?'                                                                                                                                                                                  `Oh,' she answered, `if I don't get some rampion to eat out of the garden behind the house, I know I shall die.'";
           
        }

        private void MasalDiyari_Load(object sender, EventArgs e)
        {
            richTextBox1.Visible = false;
        }

    }
}

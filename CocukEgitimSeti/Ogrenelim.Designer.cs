﻿namespace CocukEgitimSeti
{
    partial class Ogrenelim
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.btn_akrb = new System.Windows.Forms.Button();
            this.btn_mslk = new System.Windows.Forms.Button();
            this.btn_ulke = new System.Windows.Forms.Button();
            this.btn_harf = new System.Windows.Forms.Button();
            this.btn_sayi = new System.Windows.Forms.Button();
            this.btn_esya = new System.Windows.Forms.Button();
            this.btn_vct = new System.Windows.Forms.Button();
            this.btn_renk = new System.Windows.Forms.Button();
            this.btn_skil = new System.Windows.Forms.Button();
            this.btn_hyvn = new System.Windows.Forms.Button();
            this.btn_myv = new System.Windows.Forms.Button();
            this.btn_sbz = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button1.Location = new System.Drawing.Point(264, 153);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(148, 40);
            this.button1.TabIndex = 0;
            this.button1.Text = "ANASAYFA";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn_akrb
            // 
            this.btn_akrb.BackColor = System.Drawing.Color.Transparent;
            this.btn_akrb.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.Akrb;
            this.btn_akrb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_akrb.ForeColor = System.Drawing.Color.Black;
            this.btn_akrb.Location = new System.Drawing.Point(278, 12);
            this.btn_akrb.Name = "btn_akrb";
            this.btn_akrb.Size = new System.Drawing.Size(108, 25);
            this.btn_akrb.TabIndex = 3;
            this.btn_akrb.UseVisualStyleBackColor = false;
            this.btn_akrb.Click += new System.EventHandler(this.btn_akrb_Click);
            // 
            // btn_mslk
            // 
            this.btn_mslk.BackColor = System.Drawing.Color.Transparent;
            this.btn_mslk.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.Mslk;
            this.btn_mslk.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_mslk.ForeColor = System.Drawing.Color.Black;
            this.btn_mslk.Location = new System.Drawing.Point(117, 57);
            this.btn_mslk.Name = "btn_mslk";
            this.btn_mslk.Size = new System.Drawing.Size(108, 25);
            this.btn_mslk.TabIndex = 4;
            this.btn_mslk.UseVisualStyleBackColor = false;
            this.btn_mslk.Click += new System.EventHandler(this.btn_mslk_Click);
            // 
            // btn_ulke
            // 
            this.btn_ulke.BackColor = System.Drawing.Color.Transparent;
            this.btn_ulke.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.Ulke;
            this.btn_ulke.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_ulke.ForeColor = System.Drawing.Color.Black;
            this.btn_ulke.Location = new System.Drawing.Point(452, 57);
            this.btn_ulke.Name = "btn_ulke";
            this.btn_ulke.Size = new System.Drawing.Size(108, 25);
            this.btn_ulke.TabIndex = 5;
            this.btn_ulke.UseVisualStyleBackColor = false;
            this.btn_ulke.Click += new System.EventHandler(this.btn_ulke_Click);
            // 
            // btn_harf
            // 
            this.btn_harf.BackColor = System.Drawing.Color.Transparent;
            this.btn_harf.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.Harf1;
            this.btn_harf.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_harf.ForeColor = System.Drawing.Color.Black;
            this.btn_harf.Location = new System.Drawing.Point(54, 161);
            this.btn_harf.Name = "btn_harf";
            this.btn_harf.Size = new System.Drawing.Size(108, 25);
            this.btn_harf.TabIndex = 6;
            this.btn_harf.UseVisualStyleBackColor = false;
            this.btn_harf.Click += new System.EventHandler(this.btn_harf_Click);
            // 
            // btn_sayi
            // 
            this.btn_sayi.BackColor = System.Drawing.Color.Transparent;
            this.btn_sayi.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.Sayi;
            this.btn_sayi.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_sayi.ForeColor = System.Drawing.Color.Black;
            this.btn_sayi.Location = new System.Drawing.Point(89, 218);
            this.btn_sayi.Name = "btn_sayi";
            this.btn_sayi.Size = new System.Drawing.Size(108, 25);
            this.btn_sayi.TabIndex = 7;
            this.btn_sayi.UseVisualStyleBackColor = false;
            this.btn_sayi.Click += new System.EventHandler(this.btn_sayi_Click);
            // 
            // btn_esya
            // 
            this.btn_esya.BackColor = System.Drawing.Color.Transparent;
            this.btn_esya.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.Esya;
            this.btn_esya.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_esya.ForeColor = System.Drawing.Color.Black;
            this.btn_esya.Location = new System.Drawing.Point(483, 218);
            this.btn_esya.Name = "btn_esya";
            this.btn_esya.Size = new System.Drawing.Size(108, 25);
            this.btn_esya.TabIndex = 8;
            this.btn_esya.UseVisualStyleBackColor = false;
            this.btn_esya.Click += new System.EventHandler(this.btn_esya_Click);
            // 
            // btn_vct
            // 
            this.btn_vct.BackColor = System.Drawing.Color.Transparent;
            this.btn_vct.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.Vct;
            this.btn_vct.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_vct.ForeColor = System.Drawing.Color.Black;
            this.btn_vct.Location = new System.Drawing.Point(278, 312);
            this.btn_vct.Name = "btn_vct";
            this.btn_vct.Size = new System.Drawing.Size(108, 25);
            this.btn_vct.TabIndex = 9;
            this.btn_vct.UseVisualStyleBackColor = false;
            this.btn_vct.Click += new System.EventHandler(this.btn_vct_Click);
            // 
            // btn_renk
            // 
            this.btn_renk.BackColor = System.Drawing.Color.Transparent;
            this.btn_renk.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.Renk;
            this.btn_renk.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_renk.ForeColor = System.Drawing.Color.Black;
            this.btn_renk.Location = new System.Drawing.Point(89, 107);
            this.btn_renk.Name = "btn_renk";
            this.btn_renk.Size = new System.Drawing.Size(108, 25);
            this.btn_renk.TabIndex = 10;
            this.btn_renk.UseVisualStyleBackColor = false;
            this.btn_renk.Click += new System.EventHandler(this.btn_renk_Click);
            // 
            // btn_skil
            // 
            this.btn_skil.BackColor = System.Drawing.Color.Transparent;
            this.btn_skil.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.Skil;
            this.btn_skil.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_skil.ForeColor = System.Drawing.Color.Black;
            this.btn_skil.Location = new System.Drawing.Point(483, 107);
            this.btn_skil.Name = "btn_skil";
            this.btn_skil.Size = new System.Drawing.Size(108, 25);
            this.btn_skil.TabIndex = 11;
            this.btn_skil.UseVisualStyleBackColor = false;
            this.btn_skil.Click += new System.EventHandler(this.btn_skil_Click);
            // 
            // btn_hyvn
            // 
            this.btn_hyvn.BackColor = System.Drawing.Color.Transparent;
            this.btn_hyvn.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.Hyvn;
            this.btn_hyvn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_hyvn.ForeColor = System.Drawing.Color.Black;
            this.btn_hyvn.Location = new System.Drawing.Point(523, 161);
            this.btn_hyvn.Name = "btn_hyvn";
            this.btn_hyvn.Size = new System.Drawing.Size(108, 25);
            this.btn_hyvn.TabIndex = 12;
            this.btn_hyvn.UseVisualStyleBackColor = false;
            this.btn_hyvn.Click += new System.EventHandler(this.btn_hyvn_Click);
            // 
            // btn_myv
            // 
            this.btn_myv.BackColor = System.Drawing.Color.Transparent;
            this.btn_myv.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.Myv;
            this.btn_myv.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_myv.ForeColor = System.Drawing.Color.Black;
            this.btn_myv.Location = new System.Drawing.Point(452, 272);
            this.btn_myv.Name = "btn_myv";
            this.btn_myv.Size = new System.Drawing.Size(108, 25);
            this.btn_myv.TabIndex = 13;
            this.btn_myv.UseVisualStyleBackColor = false;
            this.btn_myv.Click += new System.EventHandler(this.btn_myv_Click);
            // 
            // btn_sbz
            // 
            this.btn_sbz.BackColor = System.Drawing.Color.Transparent;
            this.btn_sbz.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.Sbz;
            this.btn_sbz.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_sbz.ForeColor = System.Drawing.Color.Black;
            this.btn_sbz.Location = new System.Drawing.Point(117, 272);
            this.btn_sbz.Name = "btn_sbz";
            this.btn_sbz.Size = new System.Drawing.Size(108, 25);
            this.btn_sbz.TabIndex = 14;
            this.btn_sbz.UseVisualStyleBackColor = false;
            this.btn_sbz.Click += new System.EventHandler(this.btn_sbz_Click);
            // 
            // Ogrenelim
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.back1;
            this.ClientSize = new System.Drawing.Size(701, 482);
            this.Controls.Add(this.btn_sbz);
            this.Controls.Add(this.btn_myv);
            this.Controls.Add(this.btn_hyvn);
            this.Controls.Add(this.btn_skil);
            this.Controls.Add(this.btn_renk);
            this.Controls.Add(this.btn_vct);
            this.Controls.Add(this.btn_esya);
            this.Controls.Add(this.btn_sayi);
            this.Controls.Add(this.btn_harf);
            this.Controls.Add(this.btn_ulke);
            this.Controls.Add(this.btn_mslk);
            this.Controls.Add(this.btn_akrb);
            this.Controls.Add(this.button1);
            this.Name = "Ogrenelim";
            this.Text = "Öğrenelim";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btn_akrb;
        private System.Windows.Forms.Button btn_mslk;
        private System.Windows.Forms.Button btn_ulke;
        private System.Windows.Forms.Button btn_harf;
        private System.Windows.Forms.Button btn_sayi;
        private System.Windows.Forms.Button btn_esya;
        private System.Windows.Forms.Button btn_vct;
        private System.Windows.Forms.Button btn_renk;
        private System.Windows.Forms.Button btn_skil;
        private System.Windows.Forms.Button btn_hyvn;
        private System.Windows.Forms.Button btn_myv;
        private System.Windows.Forms.Button btn_sbz;
    }
}
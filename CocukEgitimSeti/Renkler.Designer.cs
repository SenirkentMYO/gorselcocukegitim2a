﻿namespace CocukEgitimSeti
{
    partial class Renkler
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAna = new System.Windows.Forms.Button();
            this.btnOgr = new System.Windows.Forms.Button();
            this.pb6 = new System.Windows.Forms.PictureBox();
            this.pb4 = new System.Windows.Forms.PictureBox();
            this.pb5 = new System.Windows.Forms.PictureBox();
            this.pb2 = new System.Windows.Forms.PictureBox();
            this.pb3 = new System.Windows.Forms.PictureBox();
            this.pb1 = new System.Windows.Forms.PictureBox();
            this.bttn1 = new System.Windows.Forms.Button();
            this.bttn2 = new System.Windows.Forms.Button();
            this.bttn3 = new System.Windows.Forms.Button();
            this.bttn4 = new System.Windows.Forms.Button();
            this.bttn5 = new System.Windows.Forms.Button();
            this.bttn6 = new System.Windows.Forms.Button();
            this.bttn7 = new System.Windows.Forms.Button();
            this.bttn8 = new System.Windows.Forms.Button();
            this.bttn9 = new System.Windows.Forms.Button();
            this.bttn10 = new System.Windows.Forms.Button();
            this.lbl1 = new System.Windows.Forms.Label();
            this.lbl2 = new System.Windows.Forms.Label();
            this.lbl3 = new System.Windows.Forms.Label();
            this.lbl4 = new System.Windows.Forms.Label();
            this.lbl5 = new System.Windows.Forms.Label();
            this.lbl6 = new System.Windows.Forms.Label();
            this.lbl7 = new System.Windows.Forms.Label();
            this.lbl8 = new System.Windows.Forms.Label();
            this.lbl9 = new System.Windows.Forms.Label();
            this.lbl10 = new System.Windows.Forms.Label();
            this.lbl11 = new System.Windows.Forms.Label();
            this.lbl12 = new System.Windows.Forms.Label();
            this.lbl13 = new System.Windows.Forms.Label();
            this.lbl14 = new System.Windows.Forms.Label();
            this.lbl15 = new System.Windows.Forms.Label();
            this.lbl16 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pb6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAna
            // 
            this.btnAna.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnAna.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnAna.Location = new System.Drawing.Point(12, 521);
            this.btnAna.Name = "btnAna";
            this.btnAna.Size = new System.Drawing.Size(148, 40);
            this.btnAna.TabIndex = 36;
            this.btnAna.Text = "ANASAYFA";
            this.btnAna.UseVisualStyleBackColor = false;
            this.btnAna.Click += new System.EventHandler(this.btnAna_Click);
            // 
            // btnOgr
            // 
            this.btnOgr.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnOgr.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnOgr.Location = new System.Drawing.Point(12, 580);
            this.btnOgr.Name = "btnOgr";
            this.btnOgr.Size = new System.Drawing.Size(148, 40);
            this.btnOgr.TabIndex = 37;
            this.btnOgr.Text = "ÖĞRENELİM";
            this.btnOgr.UseVisualStyleBackColor = false;
            this.btnOgr.Click += new System.EventHandler(this.btnOgr_Click);
            // 
            // pb6
            // 
            this.pb6.BackColor = System.Drawing.Color.Transparent;
            this.pb6.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.mor;
            this.pb6.Location = new System.Drawing.Point(484, 219);
            this.pb6.Name = "pb6";
            this.pb6.Size = new System.Drawing.Size(76, 146);
            this.pb6.TabIndex = 40;
            this.pb6.TabStop = false;
            this.pb6.Click += new System.EventHandler(this.pb6_Click);
            // 
            // pb4
            // 
            this.pb4.BackColor = System.Drawing.Color.Transparent;
            this.pb4.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.ysil;
            this.pb4.Location = new System.Drawing.Point(307, 128);
            this.pb4.Name = "pb4";
            this.pb4.Size = new System.Drawing.Size(140, 71);
            this.pb4.TabIndex = 41;
            this.pb4.TabStop = false;
            this.pb4.Click += new System.EventHandler(this.pb4_Click);
            // 
            // pb5
            // 
            this.pb5.BackColor = System.Drawing.Color.Transparent;
            this.pb5.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.mavi;
            this.pb5.Location = new System.Drawing.Point(332, 218);
            this.pb5.Name = "pb5";
            this.pb5.Size = new System.Drawing.Size(127, 122);
            this.pb5.TabIndex = 42;
            this.pb5.TabStop = false;
            this.pb5.Click += new System.EventHandler(this.pb5_Click);
            // 
            // pb2
            // 
            this.pb2.BackColor = System.Drawing.Color.Transparent;
            this.pb2.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.trn;
            this.pb2.Location = new System.Drawing.Point(513, 56);
            this.pb2.Name = "pb2";
            this.pb2.Size = new System.Drawing.Size(119, 117);
            this.pb2.TabIndex = 43;
            this.pb2.TabStop = false;
            this.pb2.Click += new System.EventHandler(this.pb2_Click);
            // 
            // pb3
            // 
            this.pb3.BackColor = System.Drawing.Color.Transparent;
            this.pb3.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.sari;
            this.pb3.Location = new System.Drawing.Point(406, 21);
            this.pb3.Name = "pb3";
            this.pb3.Size = new System.Drawing.Size(79, 145);
            this.pb3.TabIndex = 44;
            this.pb3.TabStop = false;
            this.pb3.Click += new System.EventHandler(this.pb3_Click);
            // 
            // pb1
            // 
            this.pb1.BackColor = System.Drawing.Color.Transparent;
            this.pb1.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.krmz;
            this.pb1.Location = new System.Drawing.Point(518, 195);
            this.pb1.Name = "pb1";
            this.pb1.Size = new System.Drawing.Size(140, 70);
            this.pb1.TabIndex = 45;
            this.pb1.TabStop = false;
            this.pb1.Click += new System.EventHandler(this.pb1_Click);
            // 
            // bttn1
            // 
            this.bttn1.BackColor = System.Drawing.Color.Black;
            this.bttn1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn1.ForeColor = System.Drawing.Color.Black;
            this.bttn1.Location = new System.Drawing.Point(12, 12);
            this.bttn1.Name = "bttn1";
            this.bttn1.Size = new System.Drawing.Size(75, 25);
            this.bttn1.TabIndex = 46;
            this.bttn1.UseVisualStyleBackColor = false;
            this.bttn1.Click += new System.EventHandler(this.bttn1_Click);
            // 
            // bttn2
            // 
            this.bttn2.BackColor = System.Drawing.Color.White;
            this.bttn2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn2.ForeColor = System.Drawing.Color.Black;
            this.bttn2.Location = new System.Drawing.Point(12, 51);
            this.bttn2.Name = "bttn2";
            this.bttn2.Size = new System.Drawing.Size(75, 25);
            this.bttn2.TabIndex = 47;
            this.bttn2.UseVisualStyleBackColor = false;
            this.bttn2.Click += new System.EventHandler(this.bttn2_Click);
            // 
            // bttn3
            // 
            this.bttn3.BackColor = System.Drawing.Color.Red;
            this.bttn3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn3.ForeColor = System.Drawing.Color.Black;
            this.bttn3.Location = new System.Drawing.Point(12, 94);
            this.bttn3.Name = "bttn3";
            this.bttn3.Size = new System.Drawing.Size(75, 25);
            this.bttn3.TabIndex = 48;
            this.bttn3.UseVisualStyleBackColor = false;
            this.bttn3.Click += new System.EventHandler(this.bttn3_Click);
            // 
            // bttn4
            // 
            this.bttn4.BackColor = System.Drawing.Color.OrangeRed;
            this.bttn4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn4.ForeColor = System.Drawing.Color.Black;
            this.bttn4.Location = new System.Drawing.Point(12, 141);
            this.bttn4.Name = "bttn4";
            this.bttn4.Size = new System.Drawing.Size(75, 25);
            this.bttn4.TabIndex = 49;
            this.bttn4.UseVisualStyleBackColor = false;
            this.bttn4.Click += new System.EventHandler(this.bttn4_Click);
            // 
            // bttn5
            // 
            this.bttn5.BackColor = System.Drawing.Color.Yellow;
            this.bttn5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn5.ForeColor = System.Drawing.Color.Black;
            this.bttn5.Location = new System.Drawing.Point(12, 195);
            this.bttn5.Name = "bttn5";
            this.bttn5.Size = new System.Drawing.Size(75, 25);
            this.bttn5.TabIndex = 50;
            this.bttn5.UseVisualStyleBackColor = false;
            this.bttn5.Click += new System.EventHandler(this.bttn5_Click);
            // 
            // bttn6
            // 
            this.bttn6.BackColor = System.Drawing.Color.Green;
            this.bttn6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn6.ForeColor = System.Drawing.Color.Black;
            this.bttn6.Location = new System.Drawing.Point(12, 249);
            this.bttn6.Name = "bttn6";
            this.bttn6.Size = new System.Drawing.Size(75, 25);
            this.bttn6.TabIndex = 51;
            this.bttn6.UseVisualStyleBackColor = false;
            this.bttn6.Click += new System.EventHandler(this.bttn6_Click);
            // 
            // bttn7
            // 
            this.bttn7.BackColor = System.Drawing.Color.DarkBlue;
            this.bttn7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn7.ForeColor = System.Drawing.Color.Black;
            this.bttn7.Location = new System.Drawing.Point(12, 299);
            this.bttn7.Name = "bttn7";
            this.bttn7.Size = new System.Drawing.Size(75, 25);
            this.bttn7.TabIndex = 52;
            this.bttn7.UseVisualStyleBackColor = false;
            this.bttn7.Click += new System.EventHandler(this.bttn7_Click);
            // 
            // bttn8
            // 
            this.bttn8.BackColor = System.Drawing.Color.Purple;
            this.bttn8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn8.ForeColor = System.Drawing.Color.Black;
            this.bttn8.Location = new System.Drawing.Point(12, 359);
            this.bttn8.Name = "bttn8";
            this.bttn8.Size = new System.Drawing.Size(75, 25);
            this.bttn8.TabIndex = 53;
            this.bttn8.UseVisualStyleBackColor = false;
            this.bttn8.Click += new System.EventHandler(this.bttn8_Click);
            // 
            // bttn9
            // 
            this.bttn9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.bttn9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn9.ForeColor = System.Drawing.Color.Black;
            this.bttn9.Location = new System.Drawing.Point(12, 412);
            this.bttn9.Name = "bttn9";
            this.bttn9.Size = new System.Drawing.Size(75, 25);
            this.bttn9.TabIndex = 54;
            this.bttn9.UseVisualStyleBackColor = false;
            this.bttn9.Click += new System.EventHandler(this.bttn9_Click);
            // 
            // bttn10
            // 
            this.bttn10.BackColor = System.Drawing.Color.Maroon;
            this.bttn10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bttn10.ForeColor = System.Drawing.Color.Black;
            this.bttn10.Location = new System.Drawing.Point(12, 463);
            this.bttn10.Name = "bttn10";
            this.bttn10.Size = new System.Drawing.Size(75, 25);
            this.bttn10.TabIndex = 55;
            this.bttn10.UseVisualStyleBackColor = false;
            this.bttn10.Click += new System.EventHandler(this.bttn10_Click);
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.BackColor = System.Drawing.Color.Transparent;
            this.lbl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl1.Location = new System.Drawing.Point(103, 19);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(13, 18);
            this.lbl1.TabIndex = 56;
            this.lbl1.Text = " ";
            // 
            // lbl2
            // 
            this.lbl2.AutoSize = true;
            this.lbl2.BackColor = System.Drawing.Color.Transparent;
            this.lbl2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl2.Location = new System.Drawing.Point(103, 54);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(13, 18);
            this.lbl2.TabIndex = 57;
            this.lbl2.Text = " ";
            // 
            // lbl3
            // 
            this.lbl3.AutoSize = true;
            this.lbl3.BackColor = System.Drawing.Color.Transparent;
            this.lbl3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl3.Location = new System.Drawing.Point(103, 97);
            this.lbl3.Name = "lbl3";
            this.lbl3.Size = new System.Drawing.Size(13, 18);
            this.lbl3.TabIndex = 58;
            this.lbl3.Text = " ";
            // 
            // lbl4
            // 
            this.lbl4.AutoSize = true;
            this.lbl4.BackColor = System.Drawing.Color.Transparent;
            this.lbl4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl4.Location = new System.Drawing.Point(103, 144);
            this.lbl4.Name = "lbl4";
            this.lbl4.Size = new System.Drawing.Size(13, 18);
            this.lbl4.TabIndex = 59;
            this.lbl4.Text = " ";
            // 
            // lbl5
            // 
            this.lbl5.AutoSize = true;
            this.lbl5.BackColor = System.Drawing.Color.Transparent;
            this.lbl5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl5.Location = new System.Drawing.Point(103, 198);
            this.lbl5.Name = "lbl5";
            this.lbl5.Size = new System.Drawing.Size(13, 18);
            this.lbl5.TabIndex = 60;
            this.lbl5.Text = " ";
            // 
            // lbl6
            // 
            this.lbl6.AutoSize = true;
            this.lbl6.BackColor = System.Drawing.Color.Transparent;
            this.lbl6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl6.Location = new System.Drawing.Point(103, 252);
            this.lbl6.Name = "lbl6";
            this.lbl6.Size = new System.Drawing.Size(13, 18);
            this.lbl6.TabIndex = 61;
            this.lbl6.Text = " ";
            // 
            // lbl7
            // 
            this.lbl7.AutoSize = true;
            this.lbl7.BackColor = System.Drawing.Color.Transparent;
            this.lbl7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl7.Location = new System.Drawing.Point(103, 302);
            this.lbl7.Name = "lbl7";
            this.lbl7.Size = new System.Drawing.Size(13, 18);
            this.lbl7.TabIndex = 62;
            this.lbl7.Text = " ";
            // 
            // lbl8
            // 
            this.lbl8.AutoSize = true;
            this.lbl8.BackColor = System.Drawing.Color.Transparent;
            this.lbl8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl8.Location = new System.Drawing.Point(103, 362);
            this.lbl8.Name = "lbl8";
            this.lbl8.Size = new System.Drawing.Size(13, 18);
            this.lbl8.TabIndex = 63;
            this.lbl8.Text = " ";
            // 
            // lbl9
            // 
            this.lbl9.AutoSize = true;
            this.lbl9.BackColor = System.Drawing.Color.Transparent;
            this.lbl9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl9.Location = new System.Drawing.Point(103, 415);
            this.lbl9.Name = "lbl9";
            this.lbl9.Size = new System.Drawing.Size(13, 18);
            this.lbl9.TabIndex = 64;
            this.lbl9.Text = " ";
            // 
            // lbl10
            // 
            this.lbl10.AutoSize = true;
            this.lbl10.BackColor = System.Drawing.Color.Transparent;
            this.lbl10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl10.Location = new System.Drawing.Point(103, 466);
            this.lbl10.Name = "lbl10";
            this.lbl10.Size = new System.Drawing.Size(13, 18);
            this.lbl10.TabIndex = 65;
            this.lbl10.Text = " ";
            // 
            // lbl11
            // 
            this.lbl11.AutoSize = true;
            this.lbl11.BackColor = System.Drawing.Color.Transparent;
            this.lbl11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl11.Location = new System.Drawing.Point(702, 54);
            this.lbl11.Name = "lbl11";
            this.lbl11.Size = new System.Drawing.Size(0, 18);
            this.lbl11.TabIndex = 66;
            // 
            // lbl12
            // 
            this.lbl12.AutoSize = true;
            this.lbl12.BackColor = System.Drawing.Color.Transparent;
            this.lbl12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl12.Location = new System.Drawing.Point(702, 15);
            this.lbl12.Name = "lbl12";
            this.lbl12.Size = new System.Drawing.Size(0, 18);
            this.lbl12.TabIndex = 67;
            // 
            // lbl13
            // 
            this.lbl13.AutoSize = true;
            this.lbl13.BackColor = System.Drawing.Color.Transparent;
            this.lbl13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl13.Location = new System.Drawing.Point(702, 97);
            this.lbl13.Name = "lbl13";
            this.lbl13.Size = new System.Drawing.Size(0, 18);
            this.lbl13.TabIndex = 68;
            // 
            // lbl14
            // 
            this.lbl14.AutoSize = true;
            this.lbl14.BackColor = System.Drawing.Color.Transparent;
            this.lbl14.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl14.Location = new System.Drawing.Point(702, 144);
            this.lbl14.Name = "lbl14";
            this.lbl14.Size = new System.Drawing.Size(0, 18);
            this.lbl14.TabIndex = 69;
            // 
            // lbl15
            // 
            this.lbl15.AutoSize = true;
            this.lbl15.BackColor = System.Drawing.Color.Transparent;
            this.lbl15.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl15.Location = new System.Drawing.Point(702, 198);
            this.lbl15.Name = "lbl15";
            this.lbl15.Size = new System.Drawing.Size(0, 18);
            this.lbl15.TabIndex = 70;
            // 
            // lbl16
            // 
            this.lbl16.AutoSize = true;
            this.lbl16.BackColor = System.Drawing.Color.Transparent;
            this.lbl16.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbl16.Location = new System.Drawing.Point(702, 252);
            this.lbl16.Name = "lbl16";
            this.lbl16.Size = new System.Drawing.Size(0, 18);
            this.lbl16.TabIndex = 71;
            // 
            // Renkler
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CocukEgitimSeti.Properties.Resources.back;
            this.ClientSize = new System.Drawing.Size(952, 697);
            this.Controls.Add(this.lbl16);
            this.Controls.Add(this.lbl15);
            this.Controls.Add(this.lbl14);
            this.Controls.Add(this.lbl13);
            this.Controls.Add(this.lbl12);
            this.Controls.Add(this.lbl11);
            this.Controls.Add(this.lbl10);
            this.Controls.Add(this.lbl9);
            this.Controls.Add(this.lbl8);
            this.Controls.Add(this.lbl7);
            this.Controls.Add(this.lbl6);
            this.Controls.Add(this.lbl5);
            this.Controls.Add(this.lbl4);
            this.Controls.Add(this.lbl3);
            this.Controls.Add(this.lbl2);
            this.Controls.Add(this.lbl1);
            this.Controls.Add(this.bttn10);
            this.Controls.Add(this.bttn9);
            this.Controls.Add(this.bttn8);
            this.Controls.Add(this.bttn7);
            this.Controls.Add(this.bttn6);
            this.Controls.Add(this.bttn5);
            this.Controls.Add(this.bttn4);
            this.Controls.Add(this.bttn3);
            this.Controls.Add(this.bttn2);
            this.Controls.Add(this.bttn1);
            this.Controls.Add(this.pb1);
            this.Controls.Add(this.pb3);
            this.Controls.Add(this.pb2);
            this.Controls.Add(this.pb5);
            this.Controls.Add(this.pb4);
            this.Controls.Add(this.pb6);
            this.Controls.Add(this.btnOgr);
            this.Controls.Add(this.btnAna);
            this.Name = "Renkler";
            this.Text = "Renkler";
            this.Load += new System.EventHandler(this.Renkler_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pb6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

     
        private System.Windows.Forms.Button btnAna;
        private System.Windows.Forms.Button btnOgr;
        private System.Windows.Forms.PictureBox pb6;
        private System.Windows.Forms.PictureBox pb4;
        private System.Windows.Forms.PictureBox pb5;
        private System.Windows.Forms.PictureBox pb2;
        private System.Windows.Forms.PictureBox pb3;
        private System.Windows.Forms.PictureBox pb1;
        private System.Windows.Forms.Button bttn1;
        private System.Windows.Forms.Button bttn2;
        private System.Windows.Forms.Button bttn3;
        private System.Windows.Forms.Button bttn4;
        private System.Windows.Forms.Button bttn5;
        private System.Windows.Forms.Button bttn6;
        private System.Windows.Forms.Button bttn7;
        private System.Windows.Forms.Button bttn8;
        private System.Windows.Forms.Button bttn9;
        private System.Windows.Forms.Button bttn10;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Label lbl2;
        private System.Windows.Forms.Label lbl3;
        private System.Windows.Forms.Label lbl4;
        private System.Windows.Forms.Label lbl5;
        private System.Windows.Forms.Label lbl6;
        private System.Windows.Forms.Label lbl7;
        private System.Windows.Forms.Label lbl8;
        private System.Windows.Forms.Label lbl9;
        private System.Windows.Forms.Label lbl10;
        private System.Windows.Forms.Label lbl11;
        private System.Windows.Forms.Label lbl12;
        private System.Windows.Forms.Label lbl13;
        private System.Windows.Forms.Label lbl14;
        private System.Windows.Forms.Label lbl15;
        private System.Windows.Forms.Label lbl16;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CocukEgitimSeti
{
    public partial class Sekiller : Form
    {
        public Sekiller()
        {
            InitializeComponent();
        }

        private void pB1_Click(object sender, EventArgs e)
        {
            lbl1.Text = "Altıgen / Hexagon";
        }

        private void pB2_Click(object sender, EventArgs e)
        {
            lbl2.Text = "Besgen / Pentagon";
        }

        private void pB3_Click(object sender, EventArgs e)
        {
            lbl3.Text = "Daire / Circle";
        }

        private void pB4_Click(object sender, EventArgs e)
        {
            lbl4.Text = "Dikdörtgen / Rectangle";
        }

        private void pB5_Click(object sender, EventArgs e)
        {
            lbl5.Text = "Sekizgen / Octagon";
        }

        private void pB6_Click(object sender, EventArgs e)
        {
            lbl6.Text = "Kare / Square";
        }

        private void pB7_Click(object sender, EventArgs e)
        {
            lbl7.Text = "Elips / Oval";
        }

        private void pB8_Click(object sender, EventArgs e)
        {
            lbl8.Text = "Üçgen / Triangle";
        }

        private void pB9_Click(object sender, EventArgs e)
        {
            lbl9.Text = "Silindir / Cylinder";
        }

        private void pB10_Click(object sender, EventArgs e)
        {
            lbl10.Text = "Koni / Cone";
        }

        private void pB11_Click(object sender, EventArgs e)
        {
            lbl11.Text = "Dikdörtgen Prizma / Rectangular Prism";
        }

        private void pB12_Click(object sender, EventArgs e)
        {
            lbl12.Text = "Küp / Cube";
        }

        private void pB13_Click(object sender, EventArgs e)
        {
            lbl13.Text = "Küre / Sphere";
        }

        private void pB14_Click(object sender, EventArgs e)
        {
            lbl14.Text = "Piramit / Pyramid";
        }

        private void btnAna_Click(object sender, EventArgs e)
        {
            form_index ind = new form_index();
            ind.Show();
            this.Hide();
        }

        private void btnOgr_Click(object sender, EventArgs e)
        {
            Ogrenelim ogr = new Ogrenelim();
            ogr.Show();
            this.Hide();
        }
    }
}

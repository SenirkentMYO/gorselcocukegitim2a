﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CocukEgitimSeti
{
    public partial class Harfler : Form
    {
        public Harfler()
        {
            InitializeComponent();
        }

        private void Harfler_Activated(object sender, EventArgs e)
        {
           
        }

        private void btnOgren_Click(object sender, EventArgs e)
        {
            Ogrenelim ogren = new Ogrenelim();
            ogren.Show();
            this.Hide();

        }

        private void btnAnas_Click(object sender, EventArgs e)
        {
            form_index anas = new form_index();
            anas.Show();
            this.Hide();
        }

        private void btnAna_Click(object sender, EventArgs e)
        {
            form_index ind = new form_index();
            ind.Show();
            this.Hide();
        }

        private void btnOgr_Click(object sender, EventArgs e)
        {
            Ogrenelim ogr = new Ogrenelim();
            ogr.Show();
            this.Hide();
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            lbl1.Text = "EY";
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            lbl2.Text = "Bİ";
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            lbl3.Text = "Sİ";
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            lbl4.Text = "Dİ";
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            lbl5.Text = "İ";
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            lbl6.Text = "EF";
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            lbl7.Text = "Cİ";
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            lbl8.Text = "EYC";
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            lbl9.Text = "AY";
        }

        private void btn10_Click(object sender, EventArgs e)
        {
            lbl10.Text = "JİY";
        }

        private void btn11_Click(object sender, EventArgs e)
        {
            lbl11.Text = "Kİ";
        }

        private void btn12_Click(object sender, EventArgs e)
        {
            lbl12.Text = "EL";
        }

        private void btn13_Click(object sender, EventArgs e)
        {
            lbl13.Text = "EM";
        }

        private void btn14_Click(object sender, EventArgs e)
        {
            lbl14.Text = "EN";
        }

        private void btn15_Click(object sender, EventArgs e)
        {
            lbl15.Text = "O";
        }

        private void btn16_Click(object sender, EventArgs e)
        {
            lbl16.Text = "Pİ";
        }

        private void btn17_Click(object sender, EventArgs e)
        {
            lbl17.Text = "KU";
        }

        private void btn18_Click(object sender, EventArgs e)
        {
            lbl18.Text = "AR";
        }

        private void btn19_Click(object sender, EventArgs e)
        {
            lbl19.Text = "ES";
        }

        private void btn20_Click(object sender, EventArgs e)
        {
            lbl20.Text = "Tİ";
        }

        private void btn21_Click(object sender, EventArgs e)
        {
            lbl21.Text = "YU";
        }

        private void btn22_Click(object sender, EventArgs e)
        {
            lbl22.Text = "Vİ";
        }

        private void btn23_Click(object sender, EventArgs e)
        {
            lbl23.Text = "DABİL YU";
        }

        private void btn24_Click(object sender, EventArgs e)
        {
            lbl24.Text = "EKS";
        }

        private void btn25_Click(object sender, EventArgs e)
        {
            lbl25.Text = "VAY";
        }

        private void btn26_Click(object sender, EventArgs e)
        {
            lbl26.Text = "ZED";
        }
    }
}
